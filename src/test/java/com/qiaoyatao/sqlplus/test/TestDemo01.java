package com.qiaoyatao.sqlplus.test;
import com.qiaoyatao.sqlplus.helper.SqlHelper;
import com.qiaoyatao.sqlplus.core.format.SqlFormat;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.core.page.PageInfo;
import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.test.model.Employee;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo01 {


    @Test
    public  void  testAdd(){
        long start1 = Runtime.getRuntime().freeMemory();
        long start = System.currentTimeMillis();
        List<SysAccount> list = new ArrayList<>();
        for (int i = 1; i <=1000 ; i++) {
            SysAccount account = new SysAccount();
            account.setUserName("乔小乔");
            account.setPhone("15510304125");
            account.setRemarks("这是我备注");
            account.setPassword("admin");
            account.setSorting(i);
            account.setCreatUid(account.getId());
            account.setCreatTime(new Date());
            account.setCreatUname("乔小乔");
            list.add(account);
            account.insert();
//            int insert = new SqlPlus2().insert(account);
//            SysAccount account1 = new SysAccount().selectByid(i);
        }
        long end = System.currentTimeMillis();
        long end2 = Runtime.getRuntime().freeMemory();
        System.out.println("StringBuffer 占用了内存：" + (end2 - start1));
        System.out.println(end-start);
//        SysAccount.insertList(list);
    }

    @Test
    public  void  testSelectByid(){
        SysAccount account = new SysAccount();
        account.setPassword("admin");
        //查询多条
        List<SysAccount> list = account.selectList();
        for (SysAccount ac : list) {
            //id查询单条
            SysAccount byid = ac.selectByid();
            System.out.println(byid);
        }
        //id查询单条
        SysAccount account1 = account.selectByid(3);
         account= account.selectOne();
        Employee employee = new Employee();
        employee.setId(3);
        Employee employee1 = employee.selectByid();
    }

    @Test
    public  void  testUpdate(){
        SysAccount account = new SysAccount();
        account.setId(1);
        //id更新
        account.setPassword("admin");
        account.setModifyUid(account.getId());
        account.setModifyTime(new Date());
        account.setModifyUname("乔小乔");
        //sqlHelper.JdbcUpdate(account.updateById());
        account.setUserName("小王扣");
        account.updateById(1);
        //把admin全部更改为admin1
        SysAccount account1 = new SysAccount();
        account1.setPassword("admin1");
        account1.setUserName("乔小乔");
        //更新条件
        SysAccount account2 = new SysAccount();
        account2.setPassword("admin1");
        account2.setUserName("乔小乔");
        account1.update(account2);
    }


    @Test
    public  void  testdeleteById(){
        new SysAccount().deleteById(5);
        new SysAccount(4).deleteById();
        SysAccount account = new SysAccount();
        account.setPassword("admin1");
        account.delete();//and条件删除
    }

    @Test
    public void  testCount(){
        SysAccount account = new SysAccount();
        account.setPassword("");
        account.setCreatTime(null);
        PageInfo<SysAccount> info1 = account.selectPage(new Page(2,2));
//        System.out.println(info);
        PageInfo<SysAccount>  info =new SysAccount().selectPage(new Page(2,2),account);
    }

    @Test
    public void  test01(){
        QueryModel QueryModel = new QueryModel();
        QueryModel.eq("user_name","乔小乔2");
        QueryModel.eq("password","admin");
        HashMap<String, Object> map = new HashMap<>();
        map.put("phone","15510304120");
        map.put("creat_uid",1);
        QueryModel.allEq(map);
//        QueryModel.or("creat_uname","乔小乔");
        List<SysAccount> list = new SysAccount().selectList(QueryModel);
//        int i=new SysAccount().delete(QueryModel);
        SysAccount account = new SysAccount();
        account.setUserName("王梦妍");
        int update = account.update(QueryModel);

    }

    @Test
    public void  testLike(){
        //模糊查询
        QueryModel QueryModel =new QueryModel();
        QueryModel.likeEq("user_name","乔小乔");
        QueryModel.likeOr("user_name","乔小乔");
//        List<SysAccount> list = new SysAccount().selectList(QueryModel);
        new SysAccount().delete(QueryModel);
    }

    @Test
    public void  testGt(){
        //模糊查询
        QueryModel QueryModel =new QueryModel();
        QueryModel.likeEq("user_name","乔小乔");
//        QueryModel.gtAnd("sorting",5);
//        QueryModel.gtOr("sorting",5);
        QueryModel.ltAnd("sorting",5);
        List<SysAccount> list = new SysAccount().selectList(QueryModel);

    }

    @Test
    public void  testCreate(){
        SysAccount account = new SysAccount();
        account.create();
    }

    @Test
    public void  testjudge(){
        SysAccount account = new SysAccount();
        QueryModel QueryModel = new QueryModel();
        Object [] params={1,"admin",0};
//        QueryModel.snippet("id =# AND password like # and is_del=#",1);
        QueryModel.snippet("",1);
//        List<SysAccount> list = account.selectList(QueryModel);
//        account=account.selectOne(QueryModel);
//        account.delete(QueryModel);
        account.setUserName("王梦妍");
        account.setModifyTime(new Date());
        account.setModifyUid(1);
        account.setModifyUname(account.getUserName());
        account.update(QueryModel);
    }

    @Test
    public void  testQL(){
        SysAccount account = new SysAccount();
        account.setId(1);
        account.setPassword("admin");
        account.setIsDel(0);
        QueryModel QueryModel = new QueryModel();
//        QueryModel.full("SELECT id,password,user_name,phone,sorting,remarks FROM sys_account WHERE id =# or password like # and is_del=#",account);
//        List<SysAccount> list = account.fullList(QueryModel);
        QueryModel QueryModel1 = new QueryModel();
        Object [] params={77,"admin",0};
        QueryModel1.full("SELECT * FROM sys_account WHERE id =# or password like # and is_del=# LIMIT 0, 1000",params);
        List<SysAccount> list1 = account.fullList(QueryModel1);
    }

    @Test
    public void  testjt(){
        SysAccount account = new SysAccount();
        QueryModel QueryModel = new QueryModel();
        account.setPassword("admin");
        account.setIsDel(0);
        QueryModel.snippet("and password = # and is_del=#",account);
        List<SysAccount> list = new SysAccount().selectList(QueryModel);
        for (SysAccount c : list) {
            System.out.println(c);
        }
    }


    @Autowired
    private SqlHelper sqlHelper;
    @Test
    public void  testdel(){
        SysAccount account = new SysAccount();
        account.setPhone("155103041201");
//        int delete = account.delete();
//        Long aLong = account.selectCount();
        String insert = SqlFormat.insert(account);
        sqlHelper.insert(insert);

    }

}
