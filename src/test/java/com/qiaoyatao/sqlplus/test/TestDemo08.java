/*
package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.helper.SqlSession3;
import com.qiaoyatao.sqlplus.core.format.Desc;
import com.qiaoyatao.sqlplus.core.format.ShowCreate;
import com.qiaoyatao.sqlplus.core.format.SqlFormat;
import com.qiaoyatao.sqlplus.core.SqlPlus2;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.core.page.PageInfo;
import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import com.qiaoyatao.sqlplus.system.zxing.QrCode;
import com.qiaoyatao.sqlplus.system.zxing.ZxingCF;
import com.qiaoyatao.test.model.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo08 {


    //    微信生成二维码
    @Test
    public void test001() throws Exception {
        String path = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "static").getPath();
        ZxingCF cf = new ZxingCF();
//        cf.setContent("http://47.105.186.204/disc/#/login");//这里设置自定义网站url
        cf.setContent("Zhang Shujun, are you stupid?");//这里设置自定义网站url
        cf.setLogoUrl(path + "\\1.jpg");
        cf.setBackgroundUrl(path + "\\wx.jpg");
        cf.setDestPath(path + "\\" + StrUtil.formatDate(new Date()));
        cf.setBackground(true);
        String encode = QrCode.encode(cf);
        System.out.println(encode);
        String decode = QrCode.decode(encode);
        System.out.println(decode);
    }

//    支付宝生成二维码


    @Test
    public void test002() throws Exception {
        String path = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "static").getPath();
        ZxingCF cf = new ZxingCF();
//        cf.setContent("http://47.105.186.204/disc/#/login");//这里设置自定义网站url
        cf.setContent("Zhang Shujun, are you stupid?");//这里设置自定义网站url
        cf.setLogoUrl(path + "\\1.jpg");
        cf.setBackgroundUrl(path + "\\ali.jpg");
        cf.setDestPath(path + "\\" + StrUtil.formatDate(new Date()));
        cf.setBackground(true);
        cf.setBY(515);
        cf.setBX(225);
        cf.setQrcode_size(460);
        cf.setFont(true);
        cf.setFontText("乔小乔(*小抠)");
        String encode = QrCode.encode(cf);
        System.out.println(encode);
        String decode = QrCode.decode(encode);
        System.out.println(decode);
    }

    @Test
    public void testExcel2() throws IOException {
        //创建工作薄对象
        HSSFWorkbook workbook = new HSSFWorkbook();//这里也可以设置sheet的Name
        //创建工作表对象
        HSSFSheet sheet = workbook.createSheet();
        //创建工作表的行
        HSSFRow row = sheet.createRow(0);//设置第一行，从零开始
        row.createCell(0).setCellValue("设备采购导出");//
        sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 5));
        //文档输出
        workbook.setSheetName(0, "设备采购导出");//设置sheet的Name
        FileOutputStream out = new FileOutputStream("d:/ex/ex001.xls");
        workbook.write(out);
        out.close();
    }


    @Test
    public void test003() throws IOException {
        String[] arr = {"id", "订单号", "下单时间", "个数", "单价", "订单金额"};
        List<String[]> list = new ArrayList<>();
        list.add(arr);
        list.add(arr);
        list.add(arr);
        list.add(arr);
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("设备采购导出");
        HSSFRow row0 = sheet.createRow(0);
        row0.setHeightInPoints(30);
        HSSFCell cell = row0.createCell(0);
        cell.setCellValue("设备采购导出");
        HSSFCellStyle style = workbook.createCellStyle();
//        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);//水平居中
//        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);//垂直居中
        style.setWrapText(true);//自动换行
        cell.setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));
        HSSFRow row = sheet.createRow(1);
        for (int i = 0; i < arr.length; i++) {
            row.createCell(i).setCellValue(Array.get(arr, i).toString());
        }
        row.setHeightInPoints(20); // 设置行的高度
        AtomicReference<Integer> index = new AtomicReference<>(2);
        list.forEach(e -> {
            HSSFRow row1 = sheet.createRow(index.get());
            for (int i = 0; i < e.length; i++) {
                row1.createCell(i).setCellValue(Array.get(arr, i).toString() == "#" ? null : Array.get(arr, i).toString());
            }
            index.getAndSet(index.get() + 1);
        });
        workbook.setActiveSheet(0);
        OutputStream outputStream = new FileOutputStream("d:/ex/" + System.currentTimeMillis() + ".xls");
        workbook.write(outputStream);
        outputStream.close();
    }

    @Autowired
    private SysAccountMapper sysAccountMapper;

    @Test
    public void test004() {
        SysAccount account = new SysAccount();
        account.setUserName("老王");
        account.setPassword("admin");
        account.setCreatTime(new Date());
        new SysAccount().copyBean(account).insert();
        account.insert();
//        CrudUtil.update(account);
    }


    @Test
    public void test005() {
        SysAccount account = new SysAccount();
        account.setUserName("老王");
        SysAccount param = new SysAccount();
        param.setUserName("乔小乔1");
        sysAccountMapper.updateByParam(account, param);
        int i = sysAccountMapper.deleteByParam(account);
        List<SysAccount> list = sysAccountMapper.selectList(account);
        SysAccount selectOne = sysAccountMapper.selectOne(param);
    }

    @Test
    public void test006() {
        for (int i = 2; i < 1000; i++) {
            SysAccount byid = new SysAccount().selectByid(i);
            if (byid == null) {
                continue;
            }
            byid.deleteById();
        }

//        new SysAccount().truncate();
        Desc desc = new SysAccount().desc();
        ShowCreate create = new SysAccount().showCreate();
//        int i= new SysAccount().dropTable();

        System.out.println(create.getCreateTable());
        System.out.println(create.getTable());
        ShowCreate showCreate = SysAccount.showCreate(SysAccount.class);
//        List<SysAccount> list = new SysAccount().selectList();
        System.out.println(String.format("Hi,%s", "王力"));

        UserVO vo = new UserVO();
        vo.setOffset(0);
        vo.setLimit(20);
//        Page page = new Page(vo);

        ACVO acvo = new ACVO();
        acvo.setUserName("乔小乔");
        acvo.setPhone("111111");
        acvo.setId(1);
        SysAccount account = new SysAccount();
        account.setUserName("乔小乔");
        account.setPassword("122333");

//        BeanUtil.copyAsName(acvo,account);

        BeanUtil.copyAsName(acvo, account, true);
        String s = BeanUtil.toParam(SysAccount.class);
        System.out.println(s);
    }

    @Autowired
    private SysUserMapper sysUserMapper;

    @Test
    public void test007() {
        ACVO acvo = new ACVO();
        acvo.setUserName("乔小乔");
        acvo.setPhone("111111");
        acvo.setDeptId(1);
        int insert = SysAccount.insert(SysAccount.class, acvo);
        SysUser user = new SysUser();
        user.setPhone("111");
        user.setUemail("333333333");
        int insert1 = sysUserMapper.Insert(user);
        SysUser user1 = sysUserMapper.queryById(1, SysUser.class);

        List<SysAccount> list = new SysAccount().selectList();

        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= 100000; i++) {
            sb.append("admin,");
        }
        String s = SqlUtil.substr(sb);
        QueryModel model = new QueryModel();
        model.inValue("password", s);
        List<SysAccount> list1 = new SysAccount().selectList(model);

        long l = System.currentTimeMillis();
        List<SysAccount> list2 = sysAccountMapper.queryModelList(model, SysAccount.class);
        long end = System.currentTimeMillis();
        System.out.println(end - l);
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Test
    public void test008() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        Map<String, Object> one = SqlSession3.selectOne("SELECT * FROM `sys_account` LIMIT 0, 1");
        Map<String, Object> one1 = SqlSession3.selectOne("SELECT * FROM `sys_account` LIMIT 0, 1");

        SysAccount account = SqlSession3.selectOne(SqlFormat.selectByid(1, new SysAccount()), SysAccount.class);

        List<SysAccount> list = new SysAccount().selectList();

        List<SysAccount> list1 = SqlSession3.selectList(SqlFormat.selectList(new SysAccount()), SysAccount.class);

        Long aLong = SqlSession3.count(SqlFormat.selectCount(new SysAccount(), new SysAccount()));


        long start = System.currentTimeMillis();
        for (int i = 1; i <= 10000; i++) {
            SysAccount account2 = new SysAccount();
            account.setUserName("乔小乔");
            account.setPhone("15510304125");
            account.setRemarks("这是我备注");
            account.setPassword("admin");
            account.setSorting(i);
            account.setCreatUid(account.getId());
            account.setCreatTime(new Date());
            account.setCreatUname("乔小乔");

            account2.setId(i);
//            int insert = SqlSession3.insert(SqlFormat.insert(account));
            SysAccount one2 = SqlSession3.selectOne(SqlFormat.selectByid(account2), SysAccount.class);
        }
        long end = System.currentTimeMillis();


        long start1 = System.currentTimeMillis();
        for (int i = 1; i <= 10000; i++) {
            SysAccount account3 = new SysAccount();
            account.setUserName("乔小乔");
            account.setPhone("15510304125");
            account.setRemarks("这是我备注");
            account.setPassword("admin");
            account.setSorting(i);
            account.setCreatUid(account.getId());
            account.setCreatTime(new Date());
            account.setCreatUname("乔小乔");

            account.setId(i);
            SysAccount account1 = new SysAccount().selectByid(i);
//            int insert = SqlSession3.insert(SqlFormat.insert(account));
        }

        long end2 = System.currentTimeMillis();

        System.out.println((end - start));//14.369 秒

        System.out.println((end2 - start1));//26.571 秒
    }


    @Test
    @Transactional(propagation = Propagation.SUPPORTS) //不支持事务
    public void test009() {
        int i = new SysAccount().deleteById(8);
        SysAccount account = new SysAccount().selectByid(8);
    }

    @Test
    public void test010() {

        SqlPlus2 plus2 = new SqlPlus2();
        SysAccount account = plus2.selectByid(SysAccount.class, 12);
        SysAccount account1 = plus2.selectByid(new SysAccount(10), SysAccount.class);
        ACVO acvo = plus2.selectByid(SysAccount.class, 9, ACVO.class);

        Employee e = plus2.selectByid(Employee.class, 1, Employee.class);
        e.setId(null);
        int insert1 = plus2.insert(e);
        int insert = plus2.insert(Employee.class, e);
        int i = plus2.insertOrUpdate(e);
        List<SysAccount> list = plus2.selectList(new SysAccount());
        QueryModel model = new QueryModel();
        SysAccount account2 = plus2.selectOne(SysAccount.class, model);
        List<SysAccount> list1 = plus2.selectList(SysAccount.class, model);
        List<SysAccount> list2 = plus2.selectList(SysAccount.class, new HashMap<>());
        PageInfo<SysAccount> info = plus2.selectPage(new Page(), account);
        PageInfo<SysAccount> info1 = plus2.selectPage(SysAccount.class, new Page(), model);
        PageInfo<SysAccount> page = plus2.selectPage(new Page(), account, SysAccount.class);
        int i1 = plus2.deleteById(SysAccount.class, 1);
        int i2 = plus2.deleteById(account);
        int delete = plus2.delete(account);
        int delete1 = plus2.delete(SysAccount.class, account);
        int delete2 = plus2.delete(SysAccount.class, new HashMap<>());
        int i3 = plus2.updateById(account);
        int i4 = plus2.updateById(account, 1);
        int update = plus2.update(account, account);
        int update1 = plus2.update(account, model);
        int update2 = plus2.update(account, new HashMap<>());
        Long count = plus2.count(SysAccount.class, model);
        int i5 = plus2.deleteById(SysAccount.class, 1);
        plus2.valid(account);
        String valid = plus2.valid(account, true);
        plus2.validShow(account, true);
        int insert2 = plus2.insert(SysAccount.class, BeanUtil.beanToMap(account));
    }

    @Autowired
    private SqlPlus2 sqlPlus;

    @Test
    public void test012() {
        SqlPlus2 plus = new SqlPlus2();
        SysAccount account = plus.selectByid(SysAccount.class, 12);
        Map<String, Object> map = plus.toSymbolCase(account);
        Set<String> set = map.keySet();
        for (String key : set) {
            System.out.println(key + "--" + map.get(key));
        }
        System.out.println(plus.toParam(account));
        System.out.println(plus.toParam(SysAccount.class));
        System.out.println(sqlPlus.toParam(SysAccount.class));
    }

    @Test
    public void test013() {
        SqlPlus2 plus = new SqlPlus2();
        List<SysAccount> list = plus.selectList(new SysAccount());
        PageInfo<SysAccount> info = plus.selectPage(new Page(), new SysAccount());
        int i = plus.truncate(SysAccount.class);
    }
}
*/
