package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.core.format.ORDER_BY;
import com.qiaoyatao.sqlplus.test.model.ACVO;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo07 {


    @Test
    public void test02(){
        SysAccount account = new SysAccount().selectByid(1);
        account.setId(null);
        account.insertOrUpdate(); //id为空新增，不为空更新
        ACVO acvo = new ACVO();
        acvo.setUserName("王小小扣");
        acvo.setPhone("155552525555");
        acvo.setPassword("85858855");
        acvo.setId(2);
        int i = new SysAccount().insertOrUpdate(acvo);//id为空新增，不为空更新
    }
    @Test
    public void test03(){
        SysAccount account = new SysAccount();
        account.setUserName("乔小乔");
//        account.setCreatTime(new Date());
        SysAccount account1 = account.selectOne(ORDER_BY.desc("creat_time"));
    }



}
