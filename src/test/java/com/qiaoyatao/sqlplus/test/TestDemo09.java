package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.core.format.SqlFormat;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.helper.SqlHelper;
import com.qiaoyatao.sqlplus.helper.SqlSessionDynamic;
import com.qiaoyatao.sqlplus.system.http.RemoteServer;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import com.qiaoyatao.sqlplus.test.model.TUser;
import com.qiaoyatao.sqlplus.test.model.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo09 {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Test
    public void test0() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        SqlMapper sqlMapper = new SqlMapper(sqlSession);
        String sql = "select *  FROM sys_account WHERE id = 1";
        System.out.println(sql);

        List<Map<String, Object>> list = sqlMapper.selectList(sql );
        sqlSession.close();
    }

    @Test
    public void test1() {
        User user = new User();
        user.setName("秦朝阳")
                .setAge(1)
                .setEmail("25212222")
                .setCount(1);
        System.out.println(user.toString());
        User user2 = new User();
        BeanUtils.copyProperties(user, user2);
        System.out.println(user2.toString());
    }

    @Test
    public void test2() {
        SqlSessionDynamic dynamic = new SqlSessionDynamic(sqlSessionFactory);
        SysAccount account = new SysAccount();
        account.setUserName("乔小乔");
        account.setPhone("15510304125");
        account.setRemarks("这是我备注");
        account.setPassword("admin");
        account.setSorting(1);
        account.setCreatUid(account.getId());
        account.setCreatTime(new Date());
        account.setCreatUname("乔小乔");
        int insert = dynamic.insert(SqlFormat.insert(account));
        int delete = dynamic.delete(SqlFormat.deleteById(2, new SysAccount()));
    }

    @Test
    public void test3() throws UnknownHostException {
        InetAddress addr = InetAddress.getLocalHost();
        String hostAddress = addr.getHostAddress();
        String hostName = addr.getHostName();
        byte[] address = addr.getAddress();
        System.out.println("IP地址：" + addr.getHostAddress() + "，主机名：" + addr.getHostName());
    }

    @Test
    public void test4() {
        List<TUser> list = new ArrayList<TUser>();
        for (int i = 0; i < 10; i++) {
            TUser user = new TUser()
                    .setId(Long.valueOf(i))
                    .setAge(20 + i)
                    .setJobNumber("2020" + i)
                    .setEntryDate(new Date())
                    .setSex(i % 2 == 0 ? "男" : "女")
                    .setFamilyMemberQuantity(new BigDecimal(i))
                    .setName("老王" + i);
            list.add(user);
        }
        //1、通过groupingBy可以分组指定字段
        Map<String, List<TUser>> groupBySex = list.stream().collect(Collectors.groupingBy(TUser::getSex));
        Map<String, List<TUser>> map = list.stream().collect(Collectors.groupingBy((p -> p.getSex())));
        //通过filter方法可以过滤某些条件
        Set<TUser> users = list.stream().filter(a -> !a.getJobNumber().equals("20200")).collect(Collectors.toSet());
        List<TUser> users1 = list.stream().filter(a -> !a.getJobNumber().equals("20200")).collect(Collectors.toList());

        users1.sort(Comparator.comparing(TUser::getId).reversed());//排序
        int sum = list.stream().mapToInt(TUser::getAge).sum();
        int max = list.stream().mapToInt(TUser::getAge).max().getAsInt();
        int min = list.stream().mapToInt(TUser::getAge).min().getAsInt();
        //BigDecimal求和
        BigDecimal reduce = list.stream().map(TUser::getFamilyMemberQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal subtract = list.stream().map(TUser::getFamilyMemberQuantity).reduce(BigDecimal.ZERO, BigDecimal::subtract);
        BigDecimal multiply = list.stream().map(TUser::getFamilyMemberQuantity).reduce(BigDecimal.ZERO, BigDecimal::multiply);
        //判断字段是否为空
        List<TUser> users2 = list.stream().filter(x -> x.getSex() != "男").collect(Collectors.toList());
        List<TUser> users3 = list.stream().filter(x -> x.getSex() == "男").collect(Collectors.toList());
        //判断是否对象空
        List<TUser> tUsers = list.stream().filter(Objects::isNull).collect(Collectors.toList());
        List<TUser> tUsers1 = list.stream().filter(Objects::nonNull).collect(Collectors.toList());
        //4.最值
        Date date = list.stream().map(TUser::getEntryDate).min(Date::compareTo).get();
        Date maxDate = list.stream().map(TUser::getEntryDate).max(Date::compareTo).get();
        //List 转map
        Map<Long, TUser> userMap = list.stream().collect(Collectors.toMap(TUser::getId, a -> a, (k1, k2) -> k1));
        //先排序在转map
        List<TUser> users4 = list.stream().sorted(Comparator.comparing(TUser::getId).reversed()).collect(Collectors.toList());
        Map<String, TUser> userMap1 = users4.stream().collect(Collectors.toMap(TUser::getName, a -> a, (k1, k2) -> k1));
        //可通过Sort对单字段多字段排序 reversed 倒序
        //单字段排序，根据id排序
        list.sort(Comparator.comparing(TUser::getId).reversed());
        //多字段排序，根据id，年龄排序
        list.sort(Comparator.comparing(TUser::getId).thenComparing(TUser::getAge));
        //可通过distinct方法进行去重
        List<TUser> collect = list.stream().distinct().collect(Collectors.toList());
        ////获取list对象的某个字段组装成新list
        List<Long> idList = list.stream().map(a -> a.getId()).collect(Collectors.toList());
        List<String> nameList = list.stream().map(a -> a.getName()).collect(Collectors.toList());
        nameList.sort(Comparator.reverseOrder());//排序
        List<TUser> users5 = list.stream().peek(k -> {
            if (k.getId() % 2 != 0) {
                k.setName(null);
            }
        }).filter(k -> k.getId() % 2 != 0).collect(Collectors.toList());
        //9.批量设置list列表字段为同一个值
        list.stream().forEach(a -> a.setName("老王"));
        //第一种Lambda表达式
        list.forEach(s -> System.out.println(s));
        list.forEach(System.out::println);
    }

    @Test
    public void test5() {
        List<TUser> list = new ArrayList<TUser>();
        for (int i = 0; i < 10; i++) {
            TUser user = new TUser()
                    .setId(Long.valueOf(i))
                    .setAge(20 + i)
                    .setJobNumber("2020" + i)
                    .setEntryDate(new Date())
                    .setSex(i % 2 == 0 ? "男" : "女")
                    .setFamilyMemberQuantity(new BigDecimal(i))
                    .setName("老王" + i);
            list.add(user);
        }
        list.sort(Comparator.comparing(TUser::getId).reversed());
        int pageNumber = 3;
        int pageSize = 4;
        List<TUser> collect = list.stream().filter(k -> k.getId() >= 1 && k.getId() <= 5).collect(Collectors.toList());
        //分页
        List<TUser> users = list.stream().skip((pageNumber - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
        List<TUser> tUsers = list.subList(1, 10);
        TUser set = list.set(2, list.get(0));
        List<Integer> list1 = new ArrayList<>();
        List<Long> longs = list.stream().map(k -> k.getId()).collect(Collectors.toList());
        List<String> list2 = list.stream().map(k -> k.getName()).collect(Collectors.toList());
        longs.sort(Comparator.comparing(Long::longValue).reversed());
        list2.sort(Comparator.comparing(String::toString).reversed());
        list2.stream().peek(k->{
            System.out.println(k.toString());
        }).collect(Collectors.toList());
        new Page();
    }

    @Test
    public void test6() {
        SysAccount account = new SysAccount().selectByid(1);
        account.setId(null);
//        int insert = account.insert();
        SysAccount one = SqlHelper.selectOne(SqlFormat.selectByid(1, new SysAccount()), SysAccount.class);
        int i = SqlHelper.insert(SqlFormat.insert(account));
        List<SysAccount> list = SqlHelper.selectList(SqlFormat.selectList(new SysAccount()), SysAccount.class);

    }

    @Autowired
    private RemoteServer remoteServer;
    @Test
    public void test7() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("pageNum",2);
        map.put("pageSize",22);
        String s = remoteServer.httpGet("http://localhost/api/test2", map);
        System.out.println(s);
    }
}
