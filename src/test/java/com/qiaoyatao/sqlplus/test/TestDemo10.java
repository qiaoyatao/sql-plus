package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.gen.builder.SpUtil;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.net.URLCodec;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo10 {

    @Test
    public void test0() {
        System.out.println(String.format("Hi,%s", "小超"));
    }

    //    @Rollback(false)
    //    @Transactional
    @Test
    public void test1() {
    }

    @Test
    public void testLambda() {
        Map<String, String> map = new HashMap<>();
        map.put("a", "a");
        map.put("b", "b");
        map.put("c", "c");
        map.put("d", "d");
        // 使用Lambda进行遍历:
        map.forEach(
                (String key, Object value) -> {
                    System.out.println(key + "--" + value);
                    System.out.println(key instanceof String);
                    System.out.println(value instanceof String);
                });
        map.forEach(
                (key, value) -> {
                    System.out.println(key + "----" + value);
                });
    }

    @Test
    public void testLambdaList() {
        List<String> list = new ArrayList<String>();
        list.add("a");
        list.add("bb");
        list.add("ccc");
        list.add("dddd");
        System.out.println("listLambda遍历:");
        list.forEach(
                (String v) -> {
                    System.out.println(v);
                });
        list.forEach(System.out::println);
        // 使用普通的方式创建
        Runnable able =
                new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("普通方式创建!");
                    }
                };
        able.run();
        // 使用拉姆达方式创建
        Runnable r2 = () -> System.out.println("拉姆达方式创建!");
        r2.run();
    }

    @Test
    public void testStream() {
        List<String> list = Arrays.asList("张三", "李四", "王五", "xuwujing");
        int i = Arrays.binarySearch(list.toArray(), "张三");
        Stream<Object> stream = Arrays.stream(list.toArray());
        List<Object> newList = stream.filter(str -> !"张三".equals(str)).collect(Collectors.toList());

        newList.forEach(
                (Object key) -> {
                    System.out.println(key);
                });

        System.out.println("------------------------------");
        Stream stream1 = Stream.of("a", "b", "c");
        String[] strArray = new String[]{"a", "b", "c"};
        stream1 = Stream.of(strArray);
        List<String> list1 = Arrays.asList(strArray);
        stream1 = list1.stream();
    }

    @Test
    public void testStreamOf() {
        Stream<String> stream2 = Stream.of("a", "b", "c");
        //        Object[] array = stream2.toArray();
        //        List<String> list = stream2.collect(Collectors.toList());
        //        List<String> list2 = stream2.collect(Collectors.toCollection(ArrayList::new));

        //        Set<String> set = stream2.collect(Collectors.toSet());
        //        Stack<String> stack = stream2.collect(Collectors.toCollection(Stack::new));
        String str = stream2.collect(Collectors.joining(",")).toString();
        IntStream stream = IntStream.builder().add(20).add(30).add(20).build();
        //        OptionalInt min = stream.min();
        //        OptionalInt max = stream.max();
        //        long count = stream.count();
    }

    @Test
    public void testMap() {
        List<String> list = Arrays.asList("zhangSan", "liSi", "wangWu");
        System.out.println("转换之前的数据:" + list);
        List<String> list1 = list.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println("转换之后的数据:" + list1);
        List<String> list31 = Arrays.asList("1", "2", "3");
        List<Integer> list2 = list31.stream().map(Integer::valueOf).collect(Collectors.toList());
        System.out.println("转换之后的数据:" + list2);
    }

    @Test
    public void test7() throws Exception {

//www.cnblogs.com/myitnews/p/12287044.html *********** Base64编码和解码 *********** 核心类
//    org.apache.commons.codec.binary.Base64 核心方法 encodeToString 编码 decode 解码

/*
        Base64 base64 = new Base64();
        String str = "AAaaa我";
        String result = base64.encodeToString(str.getBytes("UTF-8")); // 编码
        System.out.println(result);
        byte[] decode = base64.decode(result.getBytes()); // 解码
        System.out.println(new String(decode));

*
         * ***********Hex编码和解码 ***********核心类 org.apache.commons.codec.binary.Hex 核心方法
         *encodeHexString, encodeHex 编码 decodeHex 解码

        String str_1 = "test";

        String hexString = Hex.encodeHexString(str_1.getBytes("UTF-8")); // 直接一步到位
        System.out.println(hexString);
        char[] encodeHex = Hex.encodeHex(str_1.getBytes(), true); // 先转换成char数组，第二个参数意思是是否全部转换成小写
        System.out.println(new String(encodeHex));

        byte[] decodeHex = Hex.decodeHex(encodeHex); // char数组型的
        System.out.println(new String(decodeHex));
        byte[] decodeHex2 = Hex.decodeHex(hexString.toCharArray()); // 字符串类型的，该方法要求传入的是char[]
        System.out.println(new String(decodeHex2));


         * ***********MD5加密 ***********核心类 org.apache.commons.codec.digest.DigestUtils 核心方法 md5Hex 编码

        String str_2 = "test";
        String md5 = DigestUtils.md5Hex(str_2.getBytes("UTF-8"));
        System.out.println(md5);


*
         * ***********SHA加密 ***********核心类 org.apache.commons.codec.digest.DigestUtils 核心方法 sha1Hex 编码

        String str_3 = "test中国";
        String sha1Hex = DigestUtils.sha1Hex(str_3.getBytes("UTF-8"));
        System.out.println(sha1Hex);


*
         * ***********URLCodec ***********核心类 org.apache.commons.codec.net.URLCodec 核心方法 encode 编码
         *decode 解码

        String url = "http://baidu.com?name=你好";
        URLCodec codec = new URLCodec();
        String encode = codec.encode(url);
        System.out.println(encode);
        String decodes = codec.decode(encode);
        System.out.println(decodes);

<dependency >
            <groupId > commons - io </groupId >
            <artifactId > commons - io </artifactId >
            <version > 2.5 </version >
        </dependency >
        <!--编码解码-- >
        <dependency >
            <groupId > commons - codec </groupId >
            <artifactId > commons - codec </artifactId >
            <version > 1.15 </version >
        </dependency >*/

    }

    @Test
    public void test8() {
     /*   UserDTO userDTO = new UserDTO();
        userDTO.setUserName("string");
//        userDTO.setUName("string");
        userDTO.setPhone("1");
        StrUtil.clear(userDTO);

        System.out.println(userDTO);

        System.out.println(String.class.getSimpleName());
        String aCase = SpUtil.toUpperCase("sys_account");*/

    }

    @Test
    public void test9() {
        Field[] fields = SqlUtil.superField(SysAccount.class);
        String append = StrUtil.append("1--", 33, "88---", "---555", "--");
        System.out.println(append);
    }

    /*public static UserDTO setUser(UserDTO dto) {
        if (dto == null) {
            return null;
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setUserName(dto.getUserName());
        userDTO.setUName(dto.getUName());
        userDTO.setPhone(dto.getPhone());
        userDTO.setOffset(dto.getOffset());
        userDTO.setLimit(dto.getLimit());
        return userDTO;
    }*/


}
