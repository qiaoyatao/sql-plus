package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.annotation.bean.AsName;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.lang.reflect.Field;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo12 {

    @Test
    public void test0() throws IllegalAccessException {
        SysAccount account = new SysAccount();
        account.setId(1);
//        account.setUserName("11");
        Class model = account.getClass();
        Field[] fields = model.getDeclaredFields();
        int num=0;
        int isck=0;
        for (Field field : fields) {
            if (field.isAnnotationPresent(AsName.class)){
                isck++;
                field.setAccessible(true);
                Object value = field.get(account);
                if (value==null || value.toString().length()==0 || value.equals(0)){
                    num++;
                }
            }
        }
        System.out.println(num==isck?true:false);
    }

    @Test
    public void test1() {
      String str= "地下非机械非人防车位:0039（车位）,,高层:0018,0056,,小高层:0040,0031,,";
        System.out.println(filterComma(str));
    }

    public String filterComma(String str) {
       return Stream.of(str).flatMap(s -> Stream.of(s.split(",")))
                .filter(d -> d.length() > 0).collect(Collectors.joining(","));
    }

    @Test
    public void test2() {
        
    }

    @Test
    public void test3() {

    }

    @Test
    public void test4() {

    }

    @Test
    public void test5() {

    }

    @Test
    public void test6() {

    }

    @Test
    public void test7() {

    }

    @Test
    public void test8() {

    }

    @Test
    public void test9() {

    }

    @Test
    public void test10() {

    }
    @Test
    public void test11() {

    }
    @Test
    public void test12() {

    }
    @Test
    public void test13() {

    }
    @Test
    public void test14() {

    }
    @Test
    public void test15() {

    }
    @Test
    public void test16() {

    }



}
