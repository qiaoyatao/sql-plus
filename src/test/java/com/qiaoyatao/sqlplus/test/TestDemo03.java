package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.gen.builder.DataSource;
import com.qiaoyatao.sqlplus.gen.GlobalConfig;
import com.qiaoyatao.sqlplus.gen.PackageConfig;
import com.qiaoyatao.sqlplus.gen.builder.SpField;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import com.qiaoyatao.sqlplus.system.core.EmailUtl;
import com.qiaoyatao.sqlplus.system.core.ZipUtil;
import com.qiaoyatao.sqlplus.system.http.RemoteServer;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.core.page.PageInfo;
import com.qiaoyatao.sqlplus.core.format.ORDER_BY;
import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.system.valid.Validation;
import com.qiaoyatao.sqlplus.test.model.ACEVO;
import com.qiaoyatao.sqlplus.test.model.ACVO;
import com.qiaoyatao.sqlplus.test.model.Employee;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo03 {

    @Test
    public void test001() {
        List<SysAccount> list = new SysAccount().selectList();
        List<ACVO> list1 = new SysAccount().selectList(ACVO.class);
        ACVO r1 = new SysAccount().selectByid(1, ACVO.class);
        ACVO r = new SysAccount().selectByid(ACVO.class);
        ACVO r2 = new SysAccount().selectOne(ACVO.class);
        PageInfo<ACVO> info = new SysAccount().selectPage(new Page(), new SysAccount(), ACVO.class);
        PageInfo<SysAccount> info1 = new SysAccount().selectPage(new Page(), new SysAccount());
        SysAccount ac = new SysAccount();
        ACVO acvo = new ACVO();
        acvo.setPassword("dedede");
        List<ACVO> list2 = ac.selectList(acvo, ACVO.class);
        ACVO acvo1 = ac.selectOne(acvo, ACVO.class);
        acvo1.setUserName("1");
        Validation.valid(acvo1, null);
        int delete = ac.delete(acvo);
    }

    @Test
    public void test002() {
        //修改状态
        new SysAccount().status(1);
        new SysAccount().del(1);
        new SysAccount(1).status();
        new SysAccount(1).del();

    }


    @Test
    public void test003() {
        SysAccount account = new SysAccount();
        account.setPassword("admin");
        List<ACVO> accounts = account.selectList(ACVO.class, ORDER_BY.asc("user_name"));
        List<ACVO> list = account.selectList(ACVO.class);
        List<SysAccount> list1 = account.selectList();

        PageInfo<ACVO> info = account.selectPage(new Page(), ACVO.class);
        ACVO acvo = account.selectByid(10, ACVO.class);

        PageInfo<SysAccount> info1 = account.selectPage(new Page(), ORDER_BY.desc("user_name"));
        PageInfo<ACVO> info2 = account.selectPage(new Page(), ACVO.class, ORDER_BY.desc("user_name"));

        PageInfo<SysAccount> info3 = account.selectPage(new Page());
        PageInfo info4 = new SysAccount().selectPage(new Page(), acvo, ACVO.class);
        PageInfo<SysAccount> info5 = new SysAccount().selectPage(new Page());
        PageInfo<ACVO> info6 = new SysAccount().selectPage(new Page(), account, ACVO.class, ORDER_BY.asc("user_name"));
    }

    @Test
    public void test004() {
        ACVO acvo = new ACVO();
        acvo.setPassword("admin1");
        ACEVO acevo = new SysAccount().selectOne(acvo, ACEVO.class);
        SysAccount account = new SysAccount();
        account.setUserName("乔小乔");
        account.delete();
    }

    @Test
    public void test005() throws Exception {
        String[] list = {"TUser", "Student", "SysCrNote", "Airport"};
        for (String model : list) {
            String path = new StringBuffer()
                    .append("./src/core/java/model/")
                    .append(model)
                    .append(".java")
                    .toString();
            File checkFile = new File(path);
            checkFile.delete();
            checkFile.createNewFile();// 创建目标文件
            FileWriter writer = new FileWriter(checkFile, true);
            writer.append("package model;");
            writer.append("@Data");
            writer.append(" ");
            writer.append("public class ");
            writer.append(model);
            writer.append("  extends Model<");
            writer.append(model + ">");
            writer.append("{}");
            writer.flush();
        }

    }

    @Test
    public void test006() throws Exception {
        String[] list = {"TUser", "Student", "SysCrNote", "Airport"};
        for (String model : list) {
            String path = new StringBuffer()
                    .append("./src/core/java/model/")
                    .append(model)
                    .append(".java")
                    .toString();
            File checkFile = new File(path);
            checkFile.delete();
            checkFile.createNewFile();// 创建目标文件
            PrintWriter writer = new PrintWriter(path);
            writer.append("package model;");
            writer.println("@Data");
            writer.append("public class ");
            writer.append(model);
            writer.append("  extends Model<");
            writer.append(model + ">");
            writer.println("{");
            writer.println();
            writer.print("    ");
            writer.println("private Integer id;");
            writer.println("}");
            writer.flush();

        }
    }

    @Autowired
    private SqlSessionFactory sqlSession;

    @Test
    public void test007() throws Exception {
        Connection session = sqlSession.openSession().getConnection();
        List<String> tableNames = new ArrayList<>();
        ResultSet rs = null;
        DatabaseMetaData db = session.getMetaData();
        //从元数据中获取到所有的表名
        rs = db.getTables("sql_plus", null, null, new String[]{"TABLE"});
        while (rs.next()) {
            tableNames.add(rs.getString(3));
        }
        for (String tableName : tableNames) {
            PreparedStatement ps = session.prepareStatement("show full columns from " + tableName);
            System.out.println("表 " + tableName);
            ResultSet c = ps.executeQuery();
            while (c.next()) {
                System.out.println("字段名：" + c.getString(1) + "------"
                        + "类型：" + c.getString(2) + "------"
                        + "备注: " + c.getString(9)
                );
                System.out.println("开始--");
                for (int i = 1; i <=9; i++) {
                    System.out.println(i+"---"+c.getString(i));
                }
                System.out.println("结束--");
            }
        }
    }


    @Test
    public void test008() throws Exception {
        Connection session = sqlSession.openSession().getConnection();
        List<String> tableNames = new ArrayList<>();
        ResultSet rs = null;
        DatabaseMetaData db = session.getMetaData();
        //从元数据中获取到所有的表名
        rs = db.getTables("tmc_info", null, null, new String[]{"TABLE"});
        while (rs.next()) {
            tableNames.add(rs.getString(3));
        }
        for (String tableName : tableNames) {
            create(tableName, session);
        }
    }


    public void create(String table, Connection session) throws IOException {
        String table1 = toUpperCase(table);
        String path = new StringBuffer()
                .append("./src/core/java/model/")
                .append(table1)
                .append(".java")
                .toString();
        File checkFile = new File(path);
        checkFile.delete();
        checkFile.createNewFile();// 创建目标文件
        PrintWriter writer = new PrintWriter(path);
        writer.append("package model;");
        writer.println("@Data  ");
        writer.append("public class ");
        writer.append(table1);
        writer.append("  extends Model<");
        writer.append(table1 + ">");
        writer.println("{");
        writer.println();
        List<SpField> list = setField(table, session);
        for (SpField tField : list) {
            if (tField.getComment().length() != 0) {
                writer.println("    //" + tField.getComment());
            }
            writer.println(tField.getField());
            writer.println();
        }
        writer.println("}");
        writer.flush();
    }

    public List<SpField> setField(String table, Connection session) {
        List<SpField> list = new ArrayList<>();
        try {
            PreparedStatement ps = session.prepareStatement("show full columns from " + table);
            ResultSet c = ps.executeQuery();
            while (c.next()) {
                String field = new StringBuffer()
                        .append("    ")
                        .append("private ")
                        .append(toType(c.getString(2)))
                        .append(StrUtil.toCamelCase(c.getString(1)))
                        .append(";").toString();
                list.add(new SpField().setField(field).setColumnType(c.getString(9)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static String toUpperCase(String table) {
        table = StrUtil.toCamelCase(table);
        return new StringBuffer()
                .append(table.substring(0, 1).toUpperCase())
                .append(table.substring(1))
                .toString();
    }

    private static String toType(String type) {
        if (type.contains("int") || type.contains("tinyint")) {
            return " Integer ";
        } else if (type.contains("bigint")) {
            return " Long ";
        } else if (type.contains("varchar") || type.contains("text") || type.contains("char")) {
            return " String ";
        } else if (type.contains("datetime")) {
            return " Date ";
        } else {
            return " String ";
        }
    }


    @Test
    public void testGenerator() throws Exception {
        DataSource source = new DataSource();
        source.setUrl("jdbc:mysql://localhost:3306/tmc_info?useUnicode=true&characterEncoding=utf8&serverTimezone=CTT");
        source.setUsername("root");
        source.setPassword("root");
        source.setDriverName("com.mysql.cj.jdbc.Driver");
        GlobalConfig global = new GlobalConfig();
        global.setConnection(source.connection());
        global.setAuthor("乔小乔");
        global.setOutputDir("./src/test/java/");//文件输出路径
        global.setDatabase("tmc_info");//具体数据库
        global.setCrud(true);
        global.setVo(true);
        global.setDel("is_del");
        global.setStatus("is_use");

//        global.setPreAuthorize(true);
//        global.setCrudInclude(new String[]{"sys_airport"});
//        global.setCrudExclude(new String[]{"sys_airport_city"});
//        global.setTableField(true);
//        global.setSwagger2(true);//swagger实体类注解
//        排除字段
        global.setExcludeField(new String[]{"creat_uid", "creat_uname", "modify_uid", "modify_uname", "modify_time", "creat_time"
                , "is_del", "is_use", "sorting", "remarks"});

        //global.setExclude(new String[]{"sys_airport","sys_airport_city"}); 排除表
        //需要生成表（默认生成所有表）
        global.setInclude(new String[]{"sys_cr_invoice"});
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.qiaoyatao.gen");//根路径
//        gen.model();//生成实体类
//        gen.controller();//生成controller
//        gen.service();//生成service接口及实现类
//        gen.mapper();//生成Mapper接口
//        gen.xml();//生成Mapper所对应xml文件
        source.close();

    }

    @Test
    public void test009() {
        Employee sc = new Employee();
        sc.setId(20);
        sc.setName("乔小乔");
        sc.setCreateTime(new Date());
        ACEVO acevo = new ACEVO();
        acevo.setId(1);
        acevo.setAge("20");
        acevo.setAddress("北京市昌平区");
        acevo.setName("乔小乔");
        sc.copyBean(acevo);
//        sc.insert();
        sc.copyTo(acevo);
        ACEVO acevo1 = sc.copyTo(ACEVO.class);
        System.out.println(sc);
    }


    @Test
    public void test010() {
        QueryModel queryModel = new QueryModel();
        queryModel.likeEq("user_name", "乔小乔");
        PageInfo<ACEVO> info = new SysAccount().
                selectPage(new Page(2, 10), queryModel, ACEVO.class);
        System.out.println(info);
    }

    @Test
    public void test0012() {
        Employee employee = new Employee();
        employee.setId(1);
        List<Object> list = new ArrayList<>();
        list.add(employee);
        Employee employee1 = new Employee();
        employee1.setName("你好");
        list.add(employee1);
        String valid = Validation.valid(employee, true, true);
        System.out.println(valid);
    }


    @Test
    public void test0013() {
        DataSource source = new DataSource();
            source.setUrl("jdbc:mysql://localhost:3306/tmc_info?useUnicode=true&characterEncoding=utf8&serverTimezone=CTT");
        source.setUsername("root");
        source.setPassword("root");
        source.setDriverName("com.mysql.cj.jdbc.Driver");
        Connection conn = source.connection();
        String sql = "select * from sys_cr_note";
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery(sql);
            ResultSetMetaData data = rs.getMetaData();
            for (int i = 1; i <= data.getColumnCount(); i++) {
// 获得所有列的数目及实际列数
                int columnCount = data.getColumnCount();
// 获得指定列的列名
                String columnName = data.getColumnName(i);
// 获得指定列的列值
                int columnType = data.getColumnType(i);
// 获得指定列的数据类型名
                String columnTypeName = data.getColumnTypeName(i);
// 所在的Catalog名字
                String catalogName = data.getCatalogName(i);
// 对应数据类型的类
                String columnClassName = data.getColumnClassName(i);
// 在数据库中类型的最大字符个数
                int columnDisplaySize = data.getColumnDisplaySize(i);
// 默认的列的标题
                String columnLabel = data.getColumnLabel(i);
// 获得列的模式
                String schemaName = data.getSchemaName(i);
// 某列类型的精确度(类型的长度)
                int precision = data.getPrecision(i);
// 小数点后的位数
                int scale = data.getScale(i);
// 获取某列对应的表名
                String tableName = data.getTableName(i);
// 是否自动递增
                boolean isAutoInctement = data.isAutoIncrement(i);
// 在数据库中是否为货币型
                boolean isCurrency = data.isCurrency(i);
// 是否为空
                int isNullable = data.isNullable(i);
// 是否为只读
                boolean isReadOnly = data.isReadOnly(i);
// 能否出现在where中
                boolean isSearchable = data.isSearchable(i);
                System.out.println(columnCount);
                System.out.println("获得列" + i + "的字段名称:" + columnName);
                System.out.println("获得列" + i + "的类型,返回SqlType中的编号:" + columnType);
                System.out.println("获得列" + i + "的数据类型名:" + columnTypeName);
                System.out.println("获得列" + i + "所在的Catalog名字:" + catalogName);
                System.out.println("获得列" + i + "对应数据类型的类:" + columnClassName);
                System.out.println("获得列" + i + "在数据库中类型的最大字符个数:" + columnDisplaySize);
                System.out.println("获得列" + i + "的默认的列的标题:" + columnLabel);
                System.out.println("获得列" + i + "的模式:" + schemaName);
                System.out.println("获得列" + i + "类型的精确度(类型的长度):" + precision);
                System.out.println("获得列" + i + "小数点后的位数:" + scale);
                System.out.println("获得列" + i + "对应的表名:" + tableName);
                System.out.println("获得列" + i + "是否自动递增:" + isAutoInctement);
                System.out.println("获得列" + i + "在数据库中是否为货币型:" + isCurrency);
                System.out.println("获得列" + i + "是否为空:" + isNullable);
                System.out.println("获得列" + i + "是否为只读:" + isReadOnly);
                System.out.println("获得列" + i + "能否出现在where中:" + isSearchable);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test0014() {

    }


    @Test
    public void lineCaptchaTest2() {

    }

    @Test
    public void test0015() {
        String html = "君不见，黄河之水天上来，奔流到海不复回。君不见，高堂明镜悲白发，朝如青丝暮成雪。";
        String[] toList = {"2521542324@qq.com"};
//        String[] ccList = new String[]{"2521542324@qq.com","975574462@qq.com"};
        EmailUtl.sendQQ(html, toList, null);
    }

    @Autowired
    private RemoteServer remoteServer;

    @Test
    public void test0016() {
        String s = remoteServer.httpGet("http://localhost:80/test");
        Map<String, Object> map = new HashMap<>();
        map.put("pageNum", 1);
        map.put("pageSize", 20);
        String sss = remoteServer.httpGet("http://localhost:80/test2", map);
        Page page = new Page();
        page.setOffset(100);
        page.setLimit(1);
        String jsonPost = remoteServer.httpPost("http://localhost:80/test1", page);
        String jsonHeader = remoteServer.httpPost("http://localhost:80/test1", map, page);
        String jsonHFrom = remoteServer.httpPost("http://localhost:80/test1", map);
        String jsonGET = remoteServer.httpGet("http://localhost:80/test2", map);
        System.out.println(jsonHeader);
        System.out.println(jsonPost);
        System.out.println(jsonHFrom);
    }

    @Test
    public void test03() throws Exception {
//        ZipUtil.toZip("D:\\baidupan", "d:/upload.zip", true);
        File file1 = new File("D:/upload");
        ZipUtil.deletedDir(file1);

//        ZipUtil.unzip("d:/upload.zip", "d:/upload/");
    }


    @Test
    public void test0017() {
        QueryModel queryModel = new QueryModel();
        queryModel.eq("name", "乔小乔").eq("address", "北京市昌平区")
                .likeEq("name", "ddd")
                .gtAnd("aa", 10)
                .or("dd", 1000);
        String sql = queryModel.sql();
    }

}
