package com.qiaoyatao.sqlplus.test.model;

import com.qiaoyatao.sqlplus.annotation.model.ID;
import com.qiaoyatao.sqlplus.annotation.model.TableField;
import com.qiaoyatao.sqlplus.annotation.model.TableName;
import com.qiaoyatao.sqlplus.core.Model;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName(value = "sys_item",comment = "项目表")
public class SysItem extends Model<SysItem> {

//    @ID(value = "item_id",type = IdType.INPUT,comment="项目id")
    @ID(comment="项目id")
    private int id;

    @TableField(comment = "整数类型")
    private Integer num1;

    @TableField(comment = "字符串")
    private String name;

    @TableField(comment = "短整型")
    private double money;

    @TableField(comment = "Double类型")
    private Double money1;

    @TableField(comment = "Boolean类型")
    private Boolean bs;

    @TableField(comment = "boolean类型")
    private boolean bt;

    @TableField(comment = "long类型")
    private long los;

    @TableField(comment = "Long类型")
    private Long kk;

    @TableField(comment = "float类型")
    private float gg;

    @TableField(comment = "Float类型")
    private Float gr;

    @TableField(comment = "BigDecimal类型")
    private BigDecimal numde;

    @TableField(comment = "char类型")
    private char dede;

    @TableField(comment = "short类型")
    private short ssde;

    @TableField(comment = "Short类型")
    private Short dswe;

    @TableField(comment = "byte类型")
    private byte bde;

    @TableField(comment = "Byte类型")
    private Byte wrrt;

    @TableField(comment = "日期类型")
    private Date crateTime;

}
