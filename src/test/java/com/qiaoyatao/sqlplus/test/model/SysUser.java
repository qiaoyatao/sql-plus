package com.qiaoyatao.sqlplus.test.model;

import com.qiaoyatao.sqlplus.annotation.bean.Delete;
import com.qiaoyatao.sqlplus.annotation.bean.Status;
import com.qiaoyatao.sqlplus.core.Model;
import lombok.Data;
import java.util.Date;

/**
 * @author: 王小扣
 * @date: 2020-02-21 17:03:50
 * 描述: 用户表
 */
@Data
public class SysUser extends Model<SysUser> {

    //id
    private Integer id;

    //用户名
    private String userName;

    //姓名
    private String uName;

    //手机号
    private String phone;

    //密码
    private String password;

    //密码
    private String uemail;

    //性别
    private Integer region;

    private Date userDate;

    //1-是超级管理员0-不是
    private Integer isAdmin;

    //公司id
    private Integer companyId;

    //创建人id
    private Integer creatUid;

    //创建人
    private String creatUname;

    //创建时间
    private Date creatTime;

    //修改人id
    private Integer modifyUid;

    //修改人
    private String modifyUname;

    //修改时间
    private Date modifyTime;

    //0-未删除，1-删除
    @Delete
    private Integer isDel;

    //0-禁用1-启用
    @Status
    private Integer isUse;

}
