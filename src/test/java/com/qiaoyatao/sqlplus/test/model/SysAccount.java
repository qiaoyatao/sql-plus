package com.qiaoyatao.sqlplus.test.model;
import com.qiaoyatao.sqlplus.annotation.model.TableField;
import com.qiaoyatao.sqlplus.annotation.model.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * <p>
 * 平台账户表
 * </p>
 * @author qiaoyatao
 * @since 2019-09-27
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@TableName(value = "sys_account")
public class SysAccount extends BaseModel<SysAccount> implements Serializable {

    private static final long serialVersionUID = 1L;

//    @ID(value = "id",comment = "主键id")
    private Integer id;

    @TableField(value = "password",exist = true,comment = "密码")
    private String password;

    @TableField(like = true)
    private String userName;

    @TableField(value = "phone",comment = "手机号")
    private String phone;

    public  SysAccount(Integer id){
        this.id=id;
    }

}
