package com.qiaoyatao.sqlplus.test.model;
import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.core.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
public class Employee extends Model<Employee> {

    private  Integer id;

    @NotBlank(message = "名称不为空！")
    private  String name;

    @NotBlank(message = "年龄不为空！")
    private  String age;

    private  String gender;

    private  Long deptId;

    private  String address;

    @JsonFormat(pattern = Symbol.PATTERN, timezone = Symbol.TIMEZONE)
    private Date createTime;
}
