package com.qiaoyatao.sqlplus.test.model;

import com.qiaoyatao.sqlplus.core.page.Page;
import lombok.Data;

@Data
public class UserVO extends Page {

    private String userName;

    private String gender;

    private String mailbox;

    private String aa;

    private String cc;

    private String bb;
}
