package com.qiaoyatao.sqlplus.test.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qiaoyatao.sqlplus.annotation.model.TableField;
import com.qiaoyatao.sqlplus.constants.Symbol;
import lombok.Data;
import java.util.Date;

@Data
public class ACEVO {

    private  Integer id;

    private  String name;

    private  String age;

    private  String gender;

    private  Long deptId;

    private  String address;

    @JsonFormat(pattern = Symbol.PATTERN, timezone = Symbol.TIMEZONE)
    private Date createTime;

    @TableField(value = "password",exist = true,comment = "密码")
    private String password;

    @TableField(value = "user_name",comment = "用户名")
    private String userName;

    @TableField(value = "phone",comment = "手机号")
    private String phone;

}
