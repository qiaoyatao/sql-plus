package com.qiaoyatao.sqlplus.test.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qiaoyatao.sqlplus.annotation.model.TableField;
import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.core.Model;
import lombok.Data;
import java.util.Date;

@Data
public class BaseModel<T extends Model> extends Model<T> {
    /**
     * 创建人id
     */
    @TableField(value = "creat_uid", comment = "创建人id")
    private Integer creatUid;
    /**
     * 创建人
     */
    @TableField(value = "creat_uname", comment = "创建人姓名")
    private String creatUname;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = Symbol.PATTERN, timezone = Symbol.TIMEZONE)
    @TableField(comment = "创建时间",like = true)
    private Date creatTime;
    /**
     * 修改人id
     */
    @TableField(comment = "修改人id")
    private Integer modifyUid;
    /**
     * 修改人
     */
    @TableField(comment = "修改人")
    private String modifyUname;
    /**
     * 修改时间
     */
    @JsonFormat(pattern = Symbol.PATTERN, timezone = Symbol.TIMEZONE)
    @TableField(comment = "修改时间")
    private Date modifyTime;
    /**
     * 0-未删除，1-删除
     */
//    @Delete
    @TableField(comment = "0-未删除，1-删除")
    private Integer isDel;
    /**
     * 0-禁用1-启用
     */
//    @Status
    @TableField(comment = "0-禁用1-启用")
    private Integer isUse;
    /**
     * 排序
     */
    @TableField(comment = "排序")
    private Integer sorting;
    /**
     * 备注
     */
    @TableField(comment = "备注")
    private String remarks;
}
