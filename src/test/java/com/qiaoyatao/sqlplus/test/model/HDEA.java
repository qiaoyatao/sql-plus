package com.qiaoyatao.sqlplus.test.model;

import com.qiaoyatao.sqlplus.core.Model;
import lombok.Data;

@Data
public class HDEA extends Model<HDEA> {

    private byte b;

    private short s;

    private int i;

    private long l;

    private float f;

    private double d;

    private char c;

    private boolean bo;

}
