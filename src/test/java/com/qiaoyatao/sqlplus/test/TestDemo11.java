package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import com.qiaoyatao.sqlplus.test.model.Person;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import com.qiaoyatao.sqlplus.test.model.SysUser;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("all")
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo11 {

    @Autowired
    private SqlSessionFactory sessionFactory;

    @Test
    public void test0() throws Exception {
        //https://blog.csdn.net/kangzhuang521/article/details/101014656
        Connection conn = sessionFactory.openSession().getConnection();
        // 创建ScriptRunner，用于执行SQL脚本
        ScriptRunner runner = new ScriptRunner(conn);
        runner.setErrorLogWriter(null);
        runner.setLogWriter(null);
        // 执行SQL脚本
        runner.runScript(Resources.getResourceAsReader("db/sys_account.sql"));
        // 关闭连接
        conn.close();
    }

    @Test
    public void test1() {
        String date = StrUtil.formatDate(new Date());
        String toParam = BeanUtil.toParam(SysAccount.class);
        String param = BeanUtil.toParam(SysUser.class);
//        List<SysAccount> list = new SysAccount().selectList();
//        list.stream().forEach(System.out::println);
        Map<String, Object> aCase = BeanUtil.toSymbolCase(new SysAccount());
        String append = StrUtil.append("1", "ee", "rrr00");

    }

    @Test
    public void test2() throws ParseException {
        String beginTime = new String("2017-06-09 10:22:22");
        String endTime = new String("2017-05-08 11:22:22");

//        1  直接用Date自带方法before()和after()比较

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date sd1 = df.parse(beginTime);
        Date sd2 = df.parse(endTime);

        System.out.println(sd1.before(sd2));//sd1> sd2 为true反之为false

        System.out.println(sd1.after(sd2));//sd1< sd2 为true反之为false
    }

    @Test
    public void test3() {
        List<Person> list = new ArrayList<>();
        list.add(new Person("jack", 20));
        list.add(new Person("mike", 25));
        list.add(new Person("tom", 30));
        List<Person> list20 = list.stream().filter(person -> person.getAge() == 20).collect(Collectors.toList());
        List<Person> collect = list.stream().sorted((p, p2) -> p.getAge() - p2.getAge()).collect(Collectors.toList());

        List<Person> list1 = list.stream().sorted(Comparator.comparingInt(Person::getAge)).collect(Collectors.toList());
        List<Person> list2 = list.stream().limit(2).collect(Collectors.toList());
        Integer reduce = list.stream().map(Person::getAge).reduce(0, (a, b) -> a + b);

        Integer max = list.stream().map(Person::getAge).reduce(0, Integer::max);
        int min = list.stream().mapToInt(Person::getAge).reduce(Integer::min).getAsInt();
        Double dMin = list.stream().mapToDouble(Person::getAge).reduce(Double::min).getAsDouble();
        double reduce1 = list.stream().mapToDouble(Person::getAge).reduce(0, Double::sum);

    }

    @Test
    public void test4() {
        /*int a = 1;
        //aStr为"1"
        String aStr = Convert.toStr(a);
        long[] b = {1, 2, 3, 4, 5};
        //bStr为："[1, 2, 3, 4, 5]"
        String bStr = Convert.toStr(b);

        String[] b2 = {"1", "2", "3", "4"};
        //结果为Integer数组
        Integer[] intArray = Convert.toIntArray(b2);
        long[] c = {1, 2, 3, 4, 5};
        //结果为Integer数组
        Integer[] intArray2 = Convert.toIntArray(c);
        //转换为日期对象：
        String a1 = "2017-05-06";
        Date value = Convert.toDate(a1);

        Object[] a2 = {"a", "你", "好", "", 1};
        List<?> list = Convert.convert(List.class, a2);
        //从4.1.11开始可以这么用
        List<?> list2 = Convert.toList(a2);

        Object[] a3 = {"a", "你", "好", "", 1};
        List<String> list3 = Convert.convert(new TypeReference<List<String>>() {
        }, a3);

        String a4 = "123456789";

        //结果为："１２３４５６７８９"
        String sbc = Convert.toSBC(a4);

        String a5 = "１２３４５６７８９";

        //结果为"123456789"
        String dbc = Convert.toDBC(a5);


        String a6 = "我是一个小小的可爱的字符串";

        //结果："e68891e698afe4b880e4b8aae5b08fe5b08fe79a84e58fafe788b1e79a84e5ad97e7aca6e4b8b2"
        String hex = Convert.toHex(a6, CharsetUtil.CHARSET_UTF_8);
        //结果为："我是一个小小的可爱的字符串"
        String raw = Convert.hexStrToStr(hex, CharsetUtil.CHARSET_UTF_8);

        String raw2 = Convert.hexToStr(hex, CharsetUtil.CHARSET_UTF_8);

        String a7 = "我是一个小小的可爱的字符串";

        //结果为："\\u6211\\u662f\\u4e00\\u4e2a\\u5c0f\\u5c0f\\u7684\\u53ef\\u7231\\u7684\\u5b57\\u7b26\\u4e32"
        String unicode = Convert.strToUnicode(a7);

        //结果为："我是一个小小的可爱的字符串"
        String raw3 = Convert.unicodeToStr(unicode);


        String a8 = "我不是乱码";
        //转换后result为乱码
        String result = Convert.convertCharset(a8, CharsetUtil.UTF_8, CharsetUtil.ISO_8859_1);
        String raw4 = Convert.convertCharset(result, CharsetUtil.ISO_8859_1, "UTF-8");

        long a9 = 4535345;
        //结果为：75
        long minutes = Convert.convertTime(a9, TimeUnit.MILLISECONDS, TimeUnit.MINUTES);
        //金额大小写转换
        double a10 = 67556.32;
        //结果为："陆万柒仟伍佰伍拾陆元叁角贰分"
        String digitUppercase = Convert.digitToChinese(a10);
        double a11 = 67556.32;
        //结果为："陆万柒仟伍佰伍拾陆元叁角贰分"
        String digitUppercase1 = Convert.digitToChinese(a11);
        //去包装
        Class<?> wrapClass = Integer.class;
        //结果为：int.class
        Class<?> unWraped = Convert.unWrap(wrapClass);
        //包装
        Class<?> primitiveClass = long.class;
        //结果为：Long.class
        Class<?> wraped = Convert.wrap(primitiveClass);

        ConverterRegistry converterRegistry = ConverterRegistry.getInstance();
        String result2 = converterRegistry.convert(String.class, 3423);
        Integer convert = converterRegistry.convert(Integer.class, "111");
        long convert2 = converterRegistry.convert(long.class, "111");*/


    }

    @Test
    public void test5() {
      /*  ConverterRegistry converterRegistry = ConverterRegistry.getInstance();
        //此处做为示例自定义String转换，因为Hutool中已经提供String转换，请尽量不要替换
        //替换可能引发关联转换异常（例如覆盖String转换会影响全局）
        converterRegistry.putCustom(String.class, CustomConverter.class);
        int a = 454553;
        String result = converterRegistry.convert(String.class, a);
        Assert.assertEquals("Custom: 454553", result);*/


    }

    @Test
    public void test6() {
       /* //当前时间
        Date date = DateUtil.date();
        //当前时间
        Date date2 = DateUtil.date(Calendar.getInstance());
        //当前时间
        Date date3 = DateUtil.date(System.currentTimeMillis());
        //当前时间字符串，格式：yyyy-MM-dd HH:mm:ss
        String now = DateUtil.now();
        //当前日期字符串，格式：yyyy-MM-dd
        String today = DateUtil.today();

        String dateStr = "2017-03-01";
        Date date1 = DateUtil.parse(dateStr);

        String dateStr1 = "2017-03-01";
        Date date4 = DateUtil.parse(dateStr1, "yyyy-MM-dd");

        String dateStr2 = "2017-03-01";
        Date date5 = DateUtil.parse(dateStr2);
        //结果 2017/03/01
        String format = DateUtil.format(date5, "yyyy/MM/dd");

        //常用格式的格式化，结果：2017-03-01
        String formatDate = DateUtil.formatDate(date5);

        //结果：2017-03-01 00:00:00
        String formatDateTime = DateUtil.formatDateTime(date5);

        //结果：00:00:00
        String formatTime = DateUtil.formatTime(date5);

        //获取Date对象的某个部分
        Date date6 = DateUtil.date();
        //获得年的部分
        int year = DateUtil.year(date6);
        //获得月份，从0开始计数
        int month1 = DateUtil.month(date6);
        //获得月份枚举
        Month month = DateUtil.monthEnum(date6);
*/

    }

    @Test
    public void test7() {
       /* String dateStr = "2017-03-01 22:33:23";
        Date date = DateUtil.parse(dateStr);

//一天的开始，结果：2017-03-01 00:00:00
        Date beginOfDay = DateUtil.beginOfDay(date);

//一天的结束，结果：2017-03-01 23:59:59
        Date endOfDay = DateUtil.endOfDay(date);

        String dateStr2 = "2017-03-01 22:33:23";
        Date date1 = DateUtil.parse(dateStr2);

//结果：2017-03-03 22:33:23
        Date newDate = DateUtil.offset(date1, DateField.DAY_OF_MONTH, 2);

//常用偏移，结果：2017-03-04 22:33:23
        DateTime newDate2 = DateUtil.offsetDay(date1, 3);

//常用偏移，结果：2017-03-01 19:33:23
        DateTime newDate3 = DateUtil.offsetHour(date1, -3);

        //昨天
        DateTime yesterday = DateUtil.yesterday();
//明天
        DateTime tomorrow = DateUtil.tomorrow();
//上周
        DateTime dateTime = DateUtil.lastWeek();
//下周
        DateTime dateTime1 = DateUtil.nextWeek();
//上个月
        DateTime dateTime2 = DateUtil.lastMonth();
//下个月
        DateTime dateTime3 = DateUtil.nextMonth();

        String dateStr1 = "2017-03-01 22:33:23";
        Date date2 = DateUtil.parse(dateStr1);

        String dateStr3 = "2017-04-01 23:33:23";
        Date date4 = DateUtil.parse(dateStr3);

        //相差一个月，31天
        long betweenDay = DateUtil.between(date1, date2, DateUnit.DAY);
        //Level.MINUTE表示精确到分
        String formatBetween = DateUtil.formatBetween(betweenDay, BetweenFormatter.Level.MINUTE);
        //输出：31天1小时
        Console.log(formatBetween);

        TimeInterval timer = DateUtil.timer();

//---------------------------------
//-------这是执行过程
//---------------------------------

        timer.interval();//花费毫秒数
        timer.intervalRestart();//返回花费时间，并重置开始时间
        timer.intervalMinute();//花费分钟数*/


    }

    @Test
    public void test8() {

    }

    @Test
    public void test9() {

    }

    @Test
    public void test10() {

    }

    @Test
    public void test11() {

    }

    @Test
    public void test12() {

    }

    @Test
    public void test13() {

    }

    @Test
    public void test14() {

    }

    @Test
    public void test15() {

    }

    @Test
    public void test16() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse("2022-01-31 14:05:00");
        Integer difMonth = getDifMonth(new Date(), date);

    }

    public static Integer getDifMonth(Date startDate, Date endDate) {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.setTime(startDate);
        end.setTime(endDate);
        int result = start.get(Calendar.MONTH) - (end.get(Calendar.MONTH) - 1);
        int month = (start.get(Calendar.YEAR) - end.get(Calendar.YEAR)) * 12;
        return Math.abs(month + result);
    }


 /*   public static class CustomConverter implements Converter<String> {
        @Override
        public String convert(Object value, String defaultValue) throws IllegalArgumentException {
            return "Custom: " + value.toString();
        }
    }*/


}
