package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.core.page.PageInfo;
import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.test.model.Employee;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo02 {

    @Test
    public void  testCreate() throws Exception {
        Employee employee = new Employee();
        employee.create();
    }

    @Test
    public void  testinsert() throws Exception {
        ArrayList<Employee> list = new ArrayList<Employee>();
        for (int i = 1; i <=10 ; i++) {
            Employee employee = new Employee();
            employee.setName("乔小乔");
            employee.setAddress("北京市昌平区生命科学园");
            employee.setAge("10");
            employee.setDeptId(1l);
            employee.setCreateTime(new Date());
            list.add(employee);
        }
        Employee.insertList(list);//2、批量新增

        SysAccount account = new SysAccount();
        account.setPhone("15510304125");
        account.setUserName("乔小乔");
        Employee employee = new Employee();
        employee.setId(1);
        employee.setName("乔小乔");
        employee.setAddress("北京市昌平区生命科学园");
        employee.setAge("27");
        employee.setDeptId(1l);
        employee.setCreateTime(new Date());
        Employee.insertList(account,employee);//不同类型插入
    }

    @Test
    public void testSelectByid(){
        //1、参数id
        Employee one = new Employee().selectByid(1);
        //2、实体字段id查询
        Employee employee = new Employee();
        employee.setId(2);
        Employee newEmployee = employee.selectByid();
    }

    @Test
    public void testDeleteByid(){
        //1、参数id删除
        int i = new Employee().deleteById(1);
        //2、实体字段id删除
        Employee employee = new Employee();
        employee.setId(2);
        employee.deleteById();
    }

    @Test
    public void testUpdateByid(){
        //1、参数id更新
        Employee employee = new Employee();
        employee.setCreateTime(new Date());
        employee.setName("王梦妍");
        employee.setAge("20");
        employee.setGender("女");
        employee.setCreateTime(new Date());
        employee.setAddress("哈尔滨市");
        employee.updateById(1);
        //2、实体字段id更新
        Employee e = new Employee();
        e.setId(1);
        e.setName("乔小乔");
        e.setAddress("北京市昌平区生命科学园");
        e.updateById();
    }

    @Test
    public void testSelect(){
        //1、参数id更新
        Employee employee1 = new Employee();
//        employee1.setName("乔小乔");
//        employee1.setAddress("北京市昌平区生命科学园");
        List<Employee> list = employee1.selectList();
        Long i = employee1.count();
        //2、实体字段id删除
        Employee employee = new Employee();
        employee.setId(1);
        employee.setName("乔小乔");
        PageInfo<Employee> info = employee.selectPage(new Page(1, 10));

        PageInfo<Employee> info1 = employee.selectPage(null);

        QueryModel QueryModel = new QueryModel();
        QueryModel.eq("id",1);
        QueryModel.or("name","乔小乔4");
        List<Employee> list1 = employee.selectList(QueryModel);

        Employee one = employee.selectOne(QueryModel);
        QueryModel.snippet("`id` = # OR `name` =#",4);
        List<Employee> list2 = one.selectList(QueryModel);

    }

    @Test
    public void  test00(){

        QueryModel QueryModel = new QueryModel();
        Object [] params={1,"乔小乔"};
        String sql=" `id` =# AND `name` =#";
        //1、数组类型
        QueryModel.snippet(sql,params);
        Employee e1 = new Employee().selectOne(QueryModel);
        //2、可变类型
        QueryModel.snippet(sql,1,"乔小乔");
        Employee e2 = new Employee().selectOne(QueryModel);
        //3、对象类型
        Employee e = new Employee();
        e.setId(1);
        e.setName("老王");
        QueryModel.snippet(sql,e);
        //1、查询一条
        Employee e3 = new Employee().selectOne(QueryModel);
        //2、查询列表
        List<Employee> list = new Employee().selectList(QueryModel);
        //3、条件更新

        int update = new Employee().selectByid(1).update(QueryModel);
        //4、条件删除
        int delete = new Employee().delete(QueryModel);
    }

    @Test
    public void  test01(){
        QueryModel queryModel = new QueryModel(); //*号默认会替换成字段，可以查询具体字段，切记不可as
        String sql="SELECT * from `employee` WHERE `id` = # AND `name` =#";
        //1、数组类型
        Object [] params={1,"乔小乔"};
        queryModel.full(sql,params);
  //2、可变类型
        //3、对象类型
        Employee e = new Employee();
        e.setId(1);
        e.setName("老王");

        //1、查询一条
        Employee employee = new Employee().selectOne(queryModel);
        //2、查询列表
        List<Employee> list = new Employee().selectList(queryModel);
        //插入
        QueryModel qm = new QueryModel();
        qm.full("INSERT INTO `employee` (`age`,`name`)  VALUES (#, #)",params);
        int update = new Employee().update(qm);

    }
}
