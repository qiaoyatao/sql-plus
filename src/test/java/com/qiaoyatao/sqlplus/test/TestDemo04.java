package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.gen.GenFile;
import com.qiaoyatao.sqlplus.gen.GlobalConfig;
import com.qiaoyatao.sqlplus.gen.PackageConfig;
import com.qiaoyatao.sqlplus.gen.SpGenerator;
import com.qiaoyatao.sqlplus.gen.builder.DataSource;
import com.qiaoyatao.sqlplus.gen.builder.SpUtil;
import com.qiaoyatao.sqlplus.gen.builder.SpField;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo04 {

    @Test
    public void test01() {
//        QueryModel model = new QueryModel();
//        Integer [] in={1,2,3,8,10,55,99};
//        Date[] in={new Date(),new Date(),new Date(),new Date(),new Date()};
//        model.notIn("id",in);
//        model.notInValue("id", "55,21,3,8,10,55,99");
//        model.between("id",1,2);
//        List<SysAccount> list = new SysAccount().selectList(model);
            SysAccount account = new SysAccount();
            account.setUserName("乔小乔1");
            account.setPassword("admin");
//            PageInfo<SysAccount> list = new SysAccount().selectPage(new Page(),account);
            int delete = new SysAccount().delete(account);

        }

    @Test
    public void test002(){
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        // 载入（获取）模板对象
        Template t = ve.getTemplate("templates/hellocontroller.vm");
        VelocityContext ctx = new VelocityContext();
        // 域对象加入参数值
        ctx.put("name", "乔小乔");
        ctx.put("date", (new Date()).toString());
        // list集合
        List temp = new ArrayList();
        temp.add("1");
        temp.add("2");
        ctx.put("list", temp);

        StringWriter sw = new StringWriter();
        t.merge(ctx, sw);
        System.out.println(sw.toString());

    }

    @Test
    public void test003() throws Exception {
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        // 载入（获取）模板对象
        Template t = ve.getTemplate("templates/doc.vm");
        VelocityContext ctx = new VelocityContext();
        // 域对象加入参数值
        ctx.put("name", "HelloController");
        ctx.put("queryByList", "queryByList");
        ctx.put("delete", "delete");
        /*// list集合
        List temp = new ArrayList();
        temp.add("1");
        temp.add("2");
        ctx.put("list", temp);*/
        StringWriter sw = new StringWriter();
        t.merge(ctx, sw);
        System.out.println(sw.toString());
        String path="d://test/bb.doc";
        SpUtil.createFile(new File(path));
        PrintWriter print = new PrintWriter(path);
        print.println(sw.toString());
        print.close();
    }


    @Test
    public void test004(){
        DataSource source = new DataSource();
        source.setUrl("jdbc:mysql://localhost:3305/sys_goods?useUnicode=true&characterEncoding=utf8&serverTimezone=CTT");
        source.setUsername("root");
        source.setPassword("root");
        source.setDriverName("com.mysql.cj.jdbc.Driver");
        GlobalConfig global = new GlobalConfig();
        global.setConnection(source.connection());
        global.setAuthor("乔亚涛");
        global.setOutputDir("./src/test/java/");//文件输出路径
        global.setDatabase("sys_goods");//具体数据库
//        global.setInclude(new String[]{"storage_collar"});
//        global.setBaseResultMap(false);
//        global.setBaseColumnList(false);
        global.setSwagger2(true);
//        global.setTableField(true);
        global.setCrudXml(true);
//        global.setDel("del");
//        global.setStatus("is_use");
        global.setCrud(true);
        global.setSupperMapper(true);
        global.setBaseMapper(true);
        global.setApi(true);
        global.setApiCH(true);
        global.setJpa(true);
        global.setModel(true);
        global.setPreAuthorize(true);
        global.setConfig(true);
        global.setLog(true);
        global.setTest(true);
        global.setResult(true);//包装返回信息
        global.setValidate(true);//校验
        PackageConfig pk = new PackageConfig();
        pk.setParent("com.goods");//根路径
        //1、模板生成代码生成器
        global.setPk(pk);
        SpGenerator.execute(global);
//        SpGenerator.createDto(global,true);
//        SpGenerator.createVo(global,true);
//        SpGenerator.createTest(global,true);
        //2、输出流代码生成器
        pk.setParent("com.qiaoyatao.spFile");//根路径
        global.setPk(pk);
        GenFile.execute(global);

    }


    @Test
    public void  test006() throws IOException {
        DataSource source = new DataSource();
        source.setUrl("jdbc:mysql://localhost:3306/test004?useUnicode=true&characterEncoding=utf8&serverTimezone=CTT");
        source.setUsername("root");
        source.setPassword("root");
        source.setDriverName("com.mysql.cj.jdbc.Driver");
        GlobalConfig global = new GlobalConfig();
        global.setConnection(source.connection());
        global.setAuthor("乔小乔");
        global.setOutputDir("./src/test/java/");//文件输出路径
        global.setDatabase("test004");//具体数据库
        /*global.setExclude(
                new String[]{"t_device","t_device_code","t_device_code_log",
                        "t_device_log","t_device_name","t_device_name_log","t_equipment_loan","t_equipment_loan_log"
                        ,"t_equipment_purchase",
                        "t_equipment_purchase_log","t_goods","t_goods_log","t_special_declaration"
                        ,"t_special_declaration_log",
                }
                );*/
        List<String> list = SpUtil.getTable(global);
        list.forEach(table->{
            SpField spField = SpUtil.primaryKey(table, global);
            System.out.println(table+"---"+spField);

        });

        createEX(list,global);
    }

    private static void  createEX(List<String> list,GlobalConfig global) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("设备采购导出");
        HSSFRow row0 = sheet.createRow(0);
        row0.setHeightInPoints(30);
        HSSFCell cell = row0.createCell(0);
        cell.setCellValue("数据库模型");
        HSSFCellStyle style = workbook.createCellStyle();
//        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);//水平居中
//        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);//垂直居中
        style.setWrapText(true);//自动换行
        cell.setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));
        HSSFRow row = sheet.createRow(1);
        AtomicReference<Integer> index= new AtomicReference<>(2);
        AtomicInteger num= new AtomicInteger(1);
        list.forEach(table->{
            List<SpField> fields = SpUtil.getField(table, global);
            HSSFRow row1 = sheet.createRow(index.get());
            String remarks = SpUtil.remarks(table, global);
            row1.createCell(0).setCellValue(num+","+remarks);
            row1.createCell(1).setCellValue(table);
            index.getAndSet(index.get() + 1);
            for (int i = 0; i < fields.size(); i++) {
                HSSFRow row2 = sheet.createRow(index.get());
                SpField tField = fields.get(i);
                row2.createCell(0).setCellValue(tField.getField());
                row2.createCell(1).setCellValue(tField.getColumnType());
                row2.createCell(2).setCellValue(tField.getComment());
                row2.createCell(3).setCellValue(tField.getColumnClass());
                index.getAndSet(index.get() + 1);
            }
            index.getAndSet(index.get() + 1);
            num.getAndIncrement();
        });
        row.setHeightInPoints(20); // 设置行的高度
        workbook.setActiveSheet(0);
        OutputStream outputStream = new FileOutputStream("d:/" + System.currentTimeMillis() + ".xls");
        workbook.write(outputStream);
        outputStream.close();
    }

}
