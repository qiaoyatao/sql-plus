package com.qiaoyatao.sqlplus.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qiaoyatao.sqlplus.test.model.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.*;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebAppConfiguration
@SpringBootTest
public class TestMock {

    @Autowired
    private WebApplicationContext web;

    private MockMvc mockMvc;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(web).build();
    }

    @Test
    public void test0() throws Exception {
        MvcResult authResult = mockMvc.perform(get("/api/test2")//使用get方式来调用接口。
                .contentType(MediaType.APPLICATION_XHTML_XML)//请求参数的类型
                .param("offset", "1")
                .param("limit", "101")//请求的参数（可多个）
        ).andExpect(status().isOk())
                .andReturn();
        //获取数据
        log.info(authResult.getResponse().getContentAsString());
    }

    @Test
    public void testAdd() throws Exception {
        SysUser user = new SysUser().selectByid(3);
        user.setId(null);
        senPost("/api/sys_user/add", user);
    }

    @Test
    public void testUpdate() throws Exception {
        SysUser user = new SysUser().selectByid(3);
        user.setUemail("2222222222");
        senPost("/api/sys_user/update", user);
    }

    @Test
    public void testQueryById() throws Exception {
        senPathVariable("/api/sys_user/queryById/{id}", 4);
    }

    @Test
    public void testDelete() throws Exception {
        senPathVariable("/api/sys_user/delete/{id}", 31);
    }

    @Test
    public void testQueryList() throws Exception {
        SysUser user = new SysUser();
        user.setUName("老王");
        senPost("/api/sys_user/queryList", user);
    }

    @Test
    public void testQueryPage() throws Exception {
        /*UserDTO user = new UserDTO();
        user.setUName("老王");
        user.setOffset(2);
        user.setLimit(5);
        senPost("/api/sys_user/queryPage", user);*/
    }

    @Test
    public void test2() throws Exception {
        MvcResult authResult = mockMvc.perform(post("/api/sys_user/param1")//使用get方式来调用接口。
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)//请求参数的类型
                .param("offset", "1")
                .param("limit", "101")//请求的参数（可多个）
                .param("userName", "老王")//请求的参数（可多个）
                .param("uName", "老王1")//请求的参数（可多个）
                .param("phone", "15510304125")//请求的参数（可多个）
        ).andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        //获取数据
        log.info("数据--" + authResult.getResponse().getContentAsString());
    }

    @Test
    public void test3() throws Exception {
        String result = mockMvc.perform(multipart("/api/sys_user/param2")
                .file(new MockMultipartFile("file", "test.txt",
                        "multipart/form-data",
                        "hello upload".getBytes("UTF-8"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        Assert.assertEquals("file", result);
    }

    private String senPathVariable(String path, Object param) throws Exception {
        String result = mockMvc.perform(get(path, param)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info(result);
        return result;
    }

    private String senPost(String path, Object param) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String result = mockMvc.perform(post(path)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(param)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info(result);
        return result;
    }

    //https://blog.csdn.net/wang_muhuo/article/details/84655577
    //https://www.cnblogs.com/xuyatao/p/8337087.html
    //https://www.cnblogs.com/zengls/p/11316454.html
//    https://www.jianshu.com/p/6a21edd66f4a
    //https://blog.csdn.net/zai_xia/article/details/83790706
}
