package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.helper.SqlSession2;
import com.qiaoyatao.sqlplus.core.format.SqlFormat;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.core.page.PageInfo;
import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.core.XmlMapper;
import com.qiaoyatao.sqlplus.test.model.ACEVO;
import com.qiaoyatao.sqlplus.test.model.ACVO;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import com.qiaoyatao.sqlplus.test.model.SysItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo06 {

    @Test
    public void test01(){
        ACVO acvo = new ACVO();
        acvo.setPassword("乔小乔");
        acvo.setUserName("乔小乔");
        acvo.setPhone("15510304125");
        int i = new SysAccount().updateById(37, acvo);
        ACVO where = new ACVO();
        where.setPassword("乔小乔");
        int update = new SysAccount().update(acvo, where);
        new SysItem().create();
    }

    @Test
    public void test02(){
//        new SysItem().create();
//        new HDEA().create();
        SysAccount account = new SysAccount();
        String s = SqlFormat.selectByid(6, account);
        System.out.println(s);
//        SysAccount account1 = SqlHelper.selectOne(s, SysAccount.class);
        Map<String, Object> map = SqlSession2.selectOne(s);

        String s1 = SqlFormat.selectList(account);

        List<Map<String, Object>> list = SqlSession2.selectList(s1);

        List<SysAccount> list1 = account.selectList(map);

        Map<String, Object> mapByid = account.selectMapByid(6);

        account.setId(11);
        Map<String, Object> map1 = account.selectMapOne();
        account.setId(null);
        account.setUserName("王梦妍");
        List<Map<String, Object>> maps = account.selectMapList();
        ACEVO acevo = new ACEVO();
        acevo.setPassword("admin1");
        List<Map<String, Object>> list2 = account.selectByList(acevo);

        Map<String, Object> one = account.selectMapOne(acevo);

        List<Map<String, Object>> maps1 = account.selectMapList(map);


    }

    @Test
    public void test03(){
        SysAccount account = new SysAccount();
        account.setUserName("王梦妍");
        Map<String, Object> pageInfo = account.selectMapPage(new Page());
        Integer pageNum = (Integer) pageInfo.get("pageNum");
        Integer pageSize = (Integer) pageInfo.get("pageSize");
        Long total = (Long) pageInfo.get("total");
        Integer pages = (Integer) pageInfo.get("pages");
        List<Map<String, Object>> list= (List<Map<String, Object>>) pageInfo.get("list");
        list.forEach(d->{
            System.out.println(d.get("userName"));
            System.out.println(d.toString());
        });
        Map<String, Object> page = new SysAccount().selectMapPage(new Page(), account);
        QueryModel model = new QueryModel();
        model.eq("user_name","王梦妍");
        Map<String, Object> page1 = new SysAccount().selectMapPage(new Page(), model);
        Map<String, Object> param = new HashMap<>();
        param.put("user_name","王梦妍");
        Map<String, Object> page2 = new SysAccount().selectMapPage(new Page(), param);

        System.out.println(page2.toString());

        PageInfo info = BeanUtil.toBean(page2, PageInfo.class);
        info.getList().forEach(d->{
            SysAccount bean = BeanUtil.toBean((Map<String, Object>) d, SysAccount.class);
            System.out.println(bean.toString());
        });
    }

    @Test
    public void test04(){
        LinkedHashMap<String, Object> param = new LinkedHashMap<>();
        param.put("user_name","王梦妍");
        param.put("or creat_uname","老王");
        param.put("or password","admin123");
        param.put("and phone","忘归来");
        param.put("and creat_time","2020-04-14 21:25:01");
        param.put("or modify_uid",null);

        System.out.println(param.toString());

        SysAccount account = new SysAccount();
        List<SysAccount> list = account.selectLinkedList(param);
        List<ACVO> list2 = account.selectLinkedList(param,ACVO.class);

        SysAccount one = account.selectLinkedOne(param);

        ACVO one2 = account.selectLinkedOne(param,ACVO.class);

        LinkedHashMap<String, Object> param1 = new LinkedHashMap<>();
//        param1.put("id",39);

        int i = account.deleteLinked(param1);
//        param1.put(" password","admin123");
        one.setId(null);
        int i1 = one.updateLinked(param1);
    }

//    @Autowired
//    private AccountMapper accountMapper;

    @Test
    public void test05(){
        ACVO acvo = new ACVO();
        acvo.setUserName("王梦妍");
//        SysAccount account = accountMapper.queryByParam(SysAccount.class, acvo);
//        System.out.println(account.getUserName());
//        String phone = account.getPhone();
//        System.out.println(phone);
        acvo.setId(6);
        SysAccount account1 = new SysAccount().selectByid(acvo.getId());

        account1.setId(null);

        SysAccount account2 = new SysAccount().selectOne();
        account1.update(account2);


        QueryModel model = new QueryModel();
        model.eq("id",3);
        int update = account1.update(model);

//        int remove = accountMapper.remove(account2);
        LinkedHashMap<String, Object> param1 = new LinkedHashMap<>();
//        accountMapper.modifyMap(account1,param1);

    }

    @Test
    public void test06(){
        int insert = new SysAccount().insert();
        Map<String, Object> param = new HashMap<>();
        int insert1 = new SysAccount().insert(param);
        ACVO acvo = new ACVO();
        int insert2 = new SysAccount().insert(acvo);

        int i = new SysAccount().deleteById();

        int i2 = new SysAccount().deleteById(null);

        ACVO o = new SysAccount().selectByid(ACVO.class);

        SysAccount o1 = new SysAccount().selectByid(0);

        SysAccount account = new SysAccount().selectByid();

        int i1 = new SysAccount().updateById();

        int i3 = new SysAccount().updateById(0);

        int i4 = new SysAccount().updateById(0,acvo);

        Map<String, Object> result = new SysAccount().selectMapByid(0);

        Map<String, Object> result2 = new SysAccount().selectMapByid(6);

        int delete = new SysAccount().delete();

        int delete2 = new SysAccount().delete(acvo);

        QueryModel model = new QueryModel();
        SysAccount one = new SysAccount().selectOne(model);
        Map<String, Object> one1 = new SysAccount().selectMapOne(model);
        Map<String, Object> one2 = new SysAccount().selectMapOne();
        List<Map<String, Object>> list = new SysAccount().selectMapList();

//        int remove = accountMapper.removeById(null,SysAccount.class);
        PageInfo<SysAccount> info = new SysAccount().selectPage(new Page(), SysAccount.class);
        PageInfo<ACVO> info1 = new SysAccount().selectPage(new Page(), ACVO.class);

        PageInfo info2 = new SysAccount().selectPage(new Page(), new SysAccount());

        List<SysAccount> list1 = new SysAccount().selectList();

        List<ACVO> list2 = new SysAccount().selectList(ACVO.class);
    }

    @Autowired
    private XmlMapper mapper;
    @Test
    public void test07(){
        mapper.deleteByid("sys_account",113);
        Map<String, Object> account = mapper.selectById("sys_account", 40);
        account.remove("creat_time");
        List<Map<String, Object>> list = mapper.selectList("sys_account", account);
        account.remove("id");
        int i= mapper.insert("sys_account", account);
        SysAccount toBean = SysAccount.toBean(account, SysAccount.class);
        List<SysAccount> list1= SysAccount.toBeanList(null,SysAccount.class);
        SysAccount account1 = new SysAccount();
        account1.setPassword("乔小乔");
        List<SysAccount> list2= SysAccount.toBeanList(list,account1);
        Object o = SysAccount.toBean(account, account1);
    }

    @Test
    public void test08(){
        Map<String, Object> param = new HashMap<>();
        param.put("user_name","王梦妍");
        param.put("password",null);
        int id = mapper.updateById("sys_account", 37, param);
        List<Map<String, Object>> list = mapper.selectList("sys_account", param);
        param.put("user_name","老王");
        mapper.delete("sys_account", param);
        List<SysAccount> list1= SysAccount.toBeanList(list,SysAccount.class);
        Map<String, Object> model = new HashMap<>();
        model.put("password","admin123");
        int update = mapper.update("sys_account", model, param);
    }

    @Test
    public void test09(){
        SysAccount account = new SysAccount().selectByid(35);
        account.setId(null);
//        int i= dao.insert("sys_account", account.toMysql());
//        int i2= dao.insert("sys_account", account.toMysql(account));

        String sysAccount = StrUtil.toSymbolCase("SysAccount");
        System.out.println(sysAccount);
        String aCase = StrUtil.toCamelCase(sysAccount);
        System.out.println(aCase);

    }


//    @Autowired
//    private SysAccountMapper sysAccountMapper;
    @Test
    public void test10(){

        SysAccount account = new SysAccount().selectByid(4);
        account.setId(null);
        account.insert();
//        int insert = accountMapper.Insert(account);
//        account.deleteById(3);
        long startTime = System.currentTimeMillis();
//        SysAccount account1 = sysAccountMapper.selectById(5);
        long endTime = System.currentTimeMillis();
        System.out.println(Symbol.run_time + (endTime - startTime) + "ms");
//        List<SysAccount> list = new SysAccount().selectList();
    }

    @Test
    public void test11(){
        ACVO acvo = new ACVO();
        acvo.setPassword("admin1254");
        acvo.setUserName("1111");
        acvo.setPhone("15510304125");
        acvo.setDeptId(1);
//        int i = sysAccountMapper.insertParam(SysAccount.class, acvo);
    }

}
