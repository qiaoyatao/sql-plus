package com.qiaoyatao.sqlplus.test;

import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.core.page.PageInfo;
import com.qiaoyatao.sqlplus.core.format.ORDER_BY;
import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.test.model.ACEVO;
import com.qiaoyatao.sqlplus.test.model.ACVO;
import com.qiaoyatao.sqlplus.test.model.Employee;
import com.qiaoyatao.sqlplus.test.model.SysAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo05 {

//    @Autowired
//    private AccountMapper accountMapper;

    @Test
    public void  test01(){
       /* SysAccount account = accountMapper.queryById(72, SysAccount.class);
        SysAccount account1 = new SysAccount();
        account1.setPassword("admin");
        List<SysAccount> list = accountMapper.queryList(account1);
        System.out.println(account);
        account.setUserName("乔小乔");
        account.setId(null);
        int i = accountMapper.Insert(account);
        int id = accountMapper.removeById(4, SysAccount.class);
        account.setUserName("令狐藏魂");
        accountMapper.modifyById(5,account);

        Integer [] ids={24};
        accountMapper.removeByIds(SysAccount.class,ids);

        Integer [] ids1={25};
        account.deleteByIds(ids1);
        SysAccount account2 = accountMapper.queryById(26, SysAccount.class);

        SysAccount account3 = new SysAccount();
        account3.setPassword("admin");
        Page page = new Page();
        List<SysAccount> list1 = accountMapper.queryLimit(page, account3);
        long count = accountMapper.queryCount(account3);
        PageInfo<SysAccount> pageInfo = SqlUtil.queryPage(list1, count, page);*/
    }

    @Test
    public void  test02(){
       /* SysAccount account = new SysAccount();
        account.setUserName("令狐藏魂");
        account.setId(34);
        accountMapper.remove(account);
        accountMapper.removeId(account);
        SysAccount account1 = new SysAccount();
        account1.setPhone("15510304125");
        account1.setPassword("admin123");
        account1.setId(6);
        accountMapper.modifyId(account1);

        QueryModel queryModel = new QueryModel();
        queryModel.eq("password","admin1");
        List<SysAccount> list = accountMapper.queryModelList(queryModel, SysAccount.class);
        queryModel.eq("id",32);
        accountMapper.removeQueryModel(queryModel,SysAccount.class);

        accountMapper.modifyQueryModel(queryModel,account1);*/


    }

    @Test
    public void test03(){
        ACVO acvo = new ACVO();
        acvo.setPassword("admin1");
//        List<ACEVO> list =new SysAccount().innerList(Employee.class, acvo, "id", "dept_id", ACEVO.class);
//        ACEVO one =new SysAccount().innerOne(Employee.class, acvo, "id", "dept_id", ACEVO.class);

        Map<String, Object> param = new HashMap<>();
        param.put("id",null);
        param.put("user_name","忘归来");
        SysAccount ac= new SysAccount().selectOne(param);
        ACEVO ACEVO= new SysAccount().selectOne(param,ACEVO.class);

        List<SysAccount> list = new SysAccount().selectList(param);
        List<ACEVO> list1 = new SysAccount().selectList(param, ACEVO.class);

        ac.setCreatUid(1);
        ac.setCreatTime(new Date());
        ac.setModifyUname("老王");
        ac.setModifyTime(new Date());
        int update = ac.update(param);
        int delete = ac.delete(param);
    }

    @Test
    public void test04(){
        Map<String, Object> param = new HashMap<>();
        param.put("password",null);
        param.put("phone","忘归来");
        param.put("userName","余欢水");
        param.put("creatUid",1);
        param.put("creat_time",new Date());
        param.put("creat_uname","老王");
        param.put("isDel",1);
        param.put("isUse",1);
        param.put("sorting",1);
        param.put("remarks","备注");
        new SysAccount().insert(param);
        SysAccount.insert(SysAccount.class,param);
//        accountMapper.insertMap(SysAccount.class,param);
//        param.put("password",1);
//        accountMapper.removeMap(SysAccount.class,param);

        SysAccount account = new SysAccount();
        account.setPassword("admin123");
//        accountMapper.modifyMap(account,param);
    }

    @Test
    public void test05(){
        /*SysAccount account = new SysAccount();
        account.setPassword("admin123");
        SysAccount account1 = accountMapper.queryByModel(account);
        ACVO acvo = new ACVO();
        acvo.setUserName("秦定芳");
        SysAccount account2 = accountMapper.queryByParam(SysAccount.class, acvo);

        account.setPassword(null);
        List<SysAccount> list = accountMapper.queryByModelList(SysAccount.class);

        acvo.setUserName(null);
        acvo.setPassword("admin1");
        List<SysAccount> list1 = accountMapper.queryByParamList(SysAccount.class, acvo);
        account1.setId(null);
        acvo.setDeptId(1);
        int insert = new SysAccount().insert(acvo);
        String s = new SysAccount().toParam(acvo);
        System.out.println(s);
        accountMapper.insertParam(SysAccount.class,acvo);*/

    }


    @Test
    public void test06(){
        QueryModel queryModel = new QueryModel();
        queryModel.eq("password","admin1");
//                  .eq("user_name","秦定芳");
//        SysAccount account = accountMapper.queryModel(queryModel, SysAccount.class);

//        List<SysAccount> list = accountMapper.queryModelList(queryModel, SysAccount.class);

//        List<ACVO> list1 = BeanUtil.copyToList(list, ACVO.class);
        ACVO acvo = new ACVO();
        acvo.setDeptId(100);
//        List<ACVO> list2 = BeanUtil.copyToList(list, acvo);

//        List<SysAccount> list3 = SysAccount.copyToList(list2, SysAccount.class);

//        List<ACVO> list4 = SysAccount.copyToList(list2, acvo);

        String s = new SysAccount().toParam();
        System.out.println(s);
        System.out.println(SysAccount.toParam(ACVO.class));

//        Map<String, Object> map = account.beanToMap();

//        Map<String, Object> toMap = SysAccount.beanToMap(account);

//        SysAccount account1 = SysAccount.toBean(map, SysAccount.class);

        SysAccount account2 = new SysAccount();
        account2.setId(10210);
//        SysAccount o = (SysAccount)SysAccount.toBean(toMap, account2);
    }

    @Test
    public void test07(){
        Employee employee=new Employee();
        employee.setName("老王");
        employee.setDeptId(1L);
//        employee.setAge("20");
        employee.valid();
        String s = employee.valid(true);
        employee.validShow(false);

    }

    @Test
    public void test08(){
        ACVO acvo = new ACVO();
        acvo.setUserName("余欢水");
//        List<SysAccount> list = accountMapper.queryByParamLimit(SysAccount.class, null, acvo, ORDER_BY.asc("password"));
//        long count = accountMapper.queryByParamCount(SysAccount.class, acvo);
        QueryModel queryModel = new QueryModel();
        queryModel.likeEq("password","admin");
//        List<SysAccount> list1 = accountMapper.queryModelLimit(SysAccount.class,null, queryModel,ORDER_BY.asc("password"));
//        long count1 = accountMapper.queryModelCount(SysAccount.class, queryModel);
        Map<String, Object> param = new HashMap<>();
        param.put("password","admin1");
        PageInfo<SysAccount> pageInfo = new SysAccount().selectPage(null, param);
        PageInfo<ACVO> pageInfo1 = new SysAccount().selectPage(null, param,ACVO.class);

//        long count2 = accountMapper.queryMapCount(SysAccount.class, param);
//        List<SysAccount> list2 = accountMapper.queryMapLimit(SysAccount.class, null, param);
//        List<SysAccount> list3 = accountMapper.queryMapList(SysAccount.class, param);

        SysAccount account = new SysAccount().selectOne(queryModel);

//        SysAccount account1 = accountMapper.queryMapOne(SysAccount.class, param);

//        List<ACVO> list4 = SysAccount.copyToList(list1, ACVO.class);
        acvo.setUserName(null);
//        List<ACVO> list5 = SysAccount.copyToList(list1, acvo);
    }

    @Test
    public void test009(){
        SysAccount account = new SysAccount(42).selectByid();
        String root = System.getProperty("user.dir")+ File.separator+"src\\main\\resources\\static";
        System.out.println(root);

    }




}
