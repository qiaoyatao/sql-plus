/**
 * $remark 管理
 */
<template>
  <div>
    <!-- 面包屑导航 -->
    <el-breadcrumb separator-class="el-icon-arrow-right">
      <el-breadcrumb-item :to="{ path: '/' }">首页</el-breadcrumb-item>
      <el-breadcrumb-item>管理</el-breadcrumb-item>
    </el-breadcrumb>
    <!-- 搜索筛选 -->
    <el-form :inline="true" :model="query" class="user-search">
      <el-form-item label="搜索：">
        <el-input size="small" v-model="query.queryName" placeholder="输入名称"></el-input>
      </el-form-item>
      <el-form-item>
        <el-button size="small" type="primary" icon="el-icon-search" @click="search">搜索</el-button>
        <el-button size="small" type="primary" icon="el-icon-plus" @click="handleEdit()">添加</el-button>
      </el-form-item>
    </el-form>
    <!--列表 loading-->
    <el-table size="small" :data="listData" highlight-current-row v-loading="" border element-loading-text="拼命加载中" style="width: 100%;">
      <el-table-column align="center" type="selection" width="60">
      </el-table-column>
       #foreach ($field in $fields)
    #if($field.columnClass=='Date ')
      <el-table-column sortable prop="$field.variable" label="$field.comment" >
        <template slot-scope="scope">
          <div>{{scope.row.$field.variable}}</div>
        </template>
      </el-table-column>
      #else
      <el-table-column sortable prop="$field.variable" label="$field.comment" >
      </el-table-column>
     #end
    #if($global.del==$field.variable)
      <el-table-column align="center" sortable prop="isUse" label="$field.comment" min-width="100">
        <template slot-scope="scope">
          <el-switch v-model="scope.row.$field.variable==0?nshow:fshow" active-color="#13ce66" inactive-color="#ff4949" @change="editType(scope.$index, scope.row)">
          </el-switch>
        </template>
      </el-table-column>
      #end
     #end
      <el-table-column align="center" label="操作" min-width="151">
        <template slot-scope="scope">
          <el-button size="mini" @click="handleEdit(scope.$index, scope.row)">编辑</el-button>
          <el-button size="mini" type="danger" @click="tmcDelete(scope.$index, scope.row)">删除</el-button>
        </template>
      </el-table-column>
    </el-table>
    <!-- 编辑界面 -->
    <el-dialog :title="title" :visible.sync="editFormVisible" width="30%" @click="closeDialog">
      <el-form label-width="120px" :model="editForm" :rules="rules" ref="editForm">
        #foreach ($field in $fields)
         #if($field.columnClass=='Date ')
        <el-form-item label="$field.comment" prop="$field.variable">
          <el-date-picker
            v-model="editForm.$field.variable"
            type="datetime"
            placeholder="选择$field.comment"
            align="right">
          </el-date-picker>
        </el-form-item>
        #else
        <el-form-item label="$field.comment" prop="$field.variable">
          <el-input size="small" v-model="editForm.$field.variable" auto-complete="off" placeholder="请输入$field.comment"></el-input>
        </el-form-item>
        #end
        #end
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button size="small" @click="closeDialog">取消</el-button>
        <el-button size="small" type="primary" :loading="loading" class="title" @click="submitForm('editForm')">保存</el-button>
      </div>
    </el-dialog>
  </div>
</template>
<script>

import API from '../../api/api_user';
export default {
  data() {
    return {
      nshow: true, //switch开启
      fshow: false, //switch关闭
      loading: false, //是显示加载
      editFormVisible: false, //控制编辑页面显示与隐藏
      title: '添加',
      editForm: {
      #foreach ($field in $fields)
        $field.variable: '',
      #end
      },
      query: {
        queryName: ''
      },
      // rules表单验证
      rules: {
       #foreach ($field in $fields)
        $field.variable: [{ required: true, message: '请输入$field.comment', trigger: 'blur' }],
       #end
      },
      formInline: {
        pageNum: 1,
        pageSize: 10,
        #foreach ($field in $fields)
        $field.variable: '',
        #end
      },
      // 删除
      seletedata: {
        ids: '',

      },
      listData: [], //用户数据
      // 分页参数
      pageparm: {
        currentPage: 1,
        pageSize: 10,
        total: 10
      }
    }
  },
  // 注册组件
  components: {
  },
  /**
   * 数据发生改变
   */

  /**
   * 创建完毕
   */
  created() {
   // this.selecList(this.query)
  },

  /**
   * 里面的方法只有被调用才会执行
   */
  methods: {
    // 修改状态
    editType: function(index, row) {
      this.loading = true
      // 修改状态
      API.updateStatus(row.id).then(res => {
        this.loading = false
        if (res.result == false) {
          this.$message({
            type: 'info',
            message: res.msg
          })
        } else {
          this.$message({
            type: 'success',
            message: '状态修改成功'
          })
          this.selecList(this.query)
        }
      })
    },

    // 获取列表
    selecList(parameter) {
      this.loading = true
      /***
       * 调用接口，注释上面模拟数据 取消下面注释
       */
      API.queryPage(parameter)
        .then(res => {
          this.loading = false
          if (res.result == false) {
            this.$message({
              type: 'info',
              message: res.msg
            })
          } else {
            this.listData = res.data.list
            // 分页赋值
            this.pageparm.currentPage = this.formInline.pageNum
            this.pageparm.pageSize = this.formInline.pageSize
            this.pageparm.total = res.data.total
            this.$message({
              type: 'success',
              message: '刷新成功！'
            })
          }
        })
        .catch(err => {
          this.loading = false
          this.$message.error('菜单加载失败，请稍后再试！')
        })
    },
    // 分页插件事件
    callFather(parm) {
      this.query.pageNum = parm.currentPage
      this.query.pageSize = parm.pageSize
      this.selecList(this.query)

    },
    // 搜索事件
    search() {
      this.selecList(this.query)
    },
    //显示编辑界面
    handleEdit: function(index, row) {
      this.editFormVisible = true
      if (row != undefined && row != 'undefined') {
        this.title = '修改'
        #foreach ($field in $fields)
        this.editForm.$field.variable = row.$field.variable
        #end
      } else {
        this.title = '添加'
        #foreach ($field in $fields)
        this.editForm.$field.variable = ''
        #end
      }
    },
    // 编辑、增加页面保存方法
    submitForm(editData) {
      #*this.$refs[editData].validate(valid => {
        if (valid) {
          API.dd(this.editForm)
            .then(res => {
              this.editFormVisible = false
              this.loading = false
              this.selecList(this.query)
              this.$message({
                type: 'success',
                message: '保存成功！'
              })
            })
            .catch(err => {
              this.editFormVisible = false
              this.loading = false
              this.$message.error('保存失败，请稍后再试！')
            })
        } else {
          return false
        }
      })*#
    },
    // 删除
    tmcDelete(index, row) {
      this.$confirm('确定要删除吗?', '信息', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          API.updateDlete(row.id)
            .then(res => {
              this.$message({
                type: 'success',
                message: '已删除!'
              })
              this.selecList(this.query)
            })
            .catch(err => {
              this.loading = false
              this.$message.error('删除失败，请稍后再试！')
            })
        })
        .catch(() => {
          this.$message({
            type: 'info',
            message: '已取消删除'
          })
        })
    },
    // 关闭编辑、增加弹出框
    closeDialog() {
      this.editFormVisible = false
    }
  }
}
</script>

<style scoped>
.user-search {
  margin-top: 20px;
}
.userRole {
  width: 100%;
}
</style>


