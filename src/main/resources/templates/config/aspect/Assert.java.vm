package $global.pk.parent.$global.pk.config.$model;

import org.springframework.util.CollectionUtils;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
/**
 * 异常错误抛出工具
 */
public class Assert {


    /**
     * 对象为空时
     *
     * @param obj
     * @param msg
     */
    public static void isNull(Object obj, String msg) {
        if (Objects.isNull(obj)) {
            msg(msg);
        }
    }


    static public void assertEquals(String msg, int expected,
                                    int actual) {
        if (expected == actual) {
            return;
        } else {
            String cleanmsg = msg == null ? "" : msg;
            msg(cleanmsg);
        }
    }


    /**
     * 对象不为空时
     *
     * @param obj
     * @param msg
     */
    public static void isNotNull(Object obj, String msg) {
        if (obj != null) {
            msg(msg);
        }
    }


    /**
     * 对象不为空时
     *
     * @param obj
     * @param msg
     */
    public static void isNotNull(Integer obj, String msg) {
        if (obj != null && obj != 0) {
            msg(msg);
        }
    }


    /**
     * 对象为空时
     *
     * @param obj
     * @param codeEnum
     */
    public static void isNull(Object obj, ResultCodeEnum codeEnum) {
        if (Objects.isNull(obj)) {
            msg(codeEnum);
        }
    }

    /**
     * 对象为空时
     *
     * @param obj
     */
    public static void isNull(Object obj) {
        if (Objects.isNull(obj)) {
            msg();
        }
    }

    /**
     * 对象为空时
     *
     * @param obj
     * @param msg
     */
    public static void nonNull(Object obj, String msg) {
        if (Objects.nonNull(obj)) {
            msg(msg);
        }
    }

    /**
     * 集合为空时
     *
     * @param list
     * @param msg
     */
    public static void isEmpty(List list, String msg) {
        if (CollectionUtils.isEmpty(list)) {
            msg(msg);
        }
    }

    /**
     * 对象不为空时
     *
     * @param obj
     * @param codeEnum
     */
    public static void nonNull(Object obj, ResultCodeEnum codeEnum) {
        if (Objects.nonNull(obj)) {
            msg(codeEnum);
        }
    }

    /**
     * 对象不为空时
     *
     * @param obj
     */
    public static void nonNull(Object obj) {
        if (Objects.nonNull(obj)) {
            msg();
        }
    }


    /**
     * 对象相等时
     *
     * @param val1
     * @param val2
     * @param msg
     */
    public static void equals(Object val1, Object val2, String msg) {
        if (Objects.equals(val1, val2)) {
            msg(msg);
        }
    }

    /**
     * 对象相等时
     *
     * @param val1
     * @param val2
     * @param codeEnum
     */
    public static void equals(Object val1, Object val2, ResultCodeEnum codeEnum) {
        if (Objects.equals(val1, val2)) {
            msg(codeEnum);
        }
    }

    /**
     * 对象相等时
     *
     * @param val1
     * @param val2
     */
    public static void equals(Object val1, Object val2) {
        if (Objects.equals(val1, val2)) {
            msg();
        }
    }

    /**
     * 对象不相等时
     *
     * @param val1
     * @param val2
     * @param msg
     */
    public static void unequals(Object val1, Object val2, String msg) {
        if (!Objects.equals(val1, val2)) {
            msg(msg);
        }
    }

    /**
     * 对象不相等时
     *
     * @param val1
     * @param val2
     * @param codeEnum
     */
    public static void unequals(Object val1, Object val2, ResultCodeEnum codeEnum) {
        if (!Objects.equals(val1, val2)) {
            msg(codeEnum);
        }
    }

    /**
     * 对象不相等时
     *
     * @param val1
     * @param val2
     */
    public static void unequals(Object val1, Object val2) {
        if (!Objects.equals(val1, val2)) {
            msg();
        }
    }

    /**
     * 字符串相等时
     *
     * @param val1
     * @param val2
     * @param msg
     */
    public static void equals(String val1, String val2, String msg) {
        if (val1.equals(val2)) {
            msg(msg);
        }
    }

    /**
     * 字符串相等时
     *
     * @param val1
     * @param val2
     * @param codeEnum
     */
    public static void equals(String val1, String val2, ResultCodeEnum codeEnum) {
        if (val1.equals(val2)) {
            msg(codeEnum);
        }
    }

    /**
     * 字符串相等时
     *
     * @param val1
     * @param val2
     */
    public static void equals(String val1, String val2) {
        if (val1.equals(val2)) {
            msg();
        }
    }

    /**
     * 字符串不相等时
     *
     * @param val1
     * @param val2
     * @param msg
     */
    public static void unequals(String val1, String val2, String msg) {
        if (!val1.equals(val2)) {
            msg(msg);
        }
    }

    /**
     * 字符串不相等时
     *
     * @param val1
     * @param val2
     * @param codeEnum
     */
    public static void unequals(String val1, String val2, ResultCodeEnum codeEnum) {
        if (!val1.equals(val2)) {
            msg(codeEnum);
        }
    }

    /**
     * 字符串不相等时
     *
     * @param val1
     * @param val2
     */
    public static void unequals(String val1, String val2) {
        if (!val1.equals(val2)) {
            msg();
        }
    }


    /**
     * 集合不为空时
     *
     * @param coll
     * @param msg
     */
    public static void isNotEmpty(Collection coll, String msg) {
        if (!CollectionUtils.isEmpty(coll)) {
            msg(msg);
        }
    }

    /**
     * 集合不为空时
     *
     * @param coll
     * @param codeEnum
     */
    public static void isNotEmpty(Collection coll, ResultCodeEnum codeEnum) {
        if (!CollectionUtils.isEmpty(coll)) {
            msg(codeEnum);
        }
    }

    /**
     * 集合不为空时
     *
     * @param coll
     */
    public static void isNotEmpty(Collection coll) {
        if (!CollectionUtils.isEmpty(coll)) {
            msg();
        }
    }

    /**
     * 集合为空时
     *
     * @param coll
     * @param msg
     */
    public static void isEmpty(Collection coll, String msg) {
        if (CollectionUtils.isEmpty(coll)) {
            msg(msg);
        }
    }

    /**
     * 集合为空时
     *
     * @param coll
     * @param codeEnum
     */
    public static void isEmpty(Collection coll, ResultCodeEnum codeEnum) {
        if (CollectionUtils.isEmpty(coll)) {
            msg(codeEnum);
        }
    }

    /**
     * int为空时
     *
     * @param coll
     * @param msg
     */
    public static void isIntBlank(Integer coll, String msg) {
        if (coll == null) {
            msg(msg);
        }
    }

    /**
     * 集合为空时
     *
     * @param coll
     */
    public static void isEmpty(Collection coll) {
        if (CollectionUtils.isEmpty(coll)) {
            msg();
        }
    }


    /**
     * 字符串为空
     *
     * @param str
     * @param msg
     */
    public static void isBlank(String str, String msg) {
        if (isNull(str)) {
            msg(msg);
        }
    }

    /**
     * 字符串为空
     *
     * @param str
     * @param codeEnum
     */
    public static void isBlank(String str, ResultCodeEnum codeEnum) {
        if (isNull(str)) {
            msg(codeEnum);
        }
    }

    /**
     * 字符串为空
     *
     * @param str
     */
    public static void isBlank(String str) {
        if (isNull(str)) {
            msg();
        }
    }

    /**
     * 字符串不为空
     *
     * @param str
     * @param msg
     */
    public static void isNotBlank(String str, String msg) {
        if (!isNull(str)) {
            msg(msg);
        }
    }

    /**
     * 字符串不为空
     *
     * @param str
     * @param codeEnum
     */
    public static void isNotBlank(String str, ResultCodeEnum codeEnum) {
        if (!isNull(str)) {
            msg(codeEnum);
        }
    }

    /**
     * 字符串不为空
     *
     * @param str
     */
    public static void isNotBlank(String str) {
        if (!isNull(str)) {
            msg();
        }
    }

    /**
     * 判断为真时
     *
     * @param judgment
     * @param msg
     * @
     */
    public static void isTrue(boolean judgment, String msg) {
        if (judgment) {
            msg(msg);
        }
    }

    /**
     * 判断为真时
     *
     * @param judgment
     * @
     */
    public static void isTrue(boolean judgment) {
        if (judgment) {
            msg();
        }
    }

    /**
     * 判断为真时
     *
     * @param judgment
     * @param codeEnum
     * @
     */
    public static void isTrue(boolean judgment, ResultCodeEnum codeEnum) {
        if (judgment) {
            msg(codeEnum);
        }
    }

    /**
     * 判断为假时
     *
     * @param judgment
     * @param msg
     * @
     */
    public static void isFalse(boolean judgment, String msg) {
        isTrue(!judgment, msg);
    }

    /**
     * 判断为假时
     *
     * @param judgment
     * @param codeEnum
     * @
     */
    public static void isFalse(boolean judgment, ResultCodeEnum codeEnum) {
        isTrue(!judgment, codeEnum);
    }

    /**
     * 判断为假时
     *
     * @param judgment
     * @
     */
    public static void isFalse(boolean judgment) {
        isTrue(!judgment);
    }


    /**
     * 集合中包含obj时
     *
     * @param coll
     * @param obj
     * @param msg
     * @param <T>
     * @
     */
    public static <T> void contains(Collection<T> coll, T obj, String msg) {
        if (coll.contains(obj)) {
            msg(msg);
        }
    }

    /**
     * 集合中包含obj时
     *
     * @param coll
     * @param obj
     * @param codeEnum
     * @param <T>
     * @
     */
    public static <T> void contains(Collection<T> coll, T obj, ResultCodeEnum codeEnum) {
        if (coll.contains(obj)) {
            msg(codeEnum);
        }
    }

    /**
     * 集合中包含obj时
     *
     * @param coll
     * @param obj
     * @param <T>
     * @
     */
    public static <T> void contains(Collection<T> coll, T obj) {
        if (coll.contains(obj)) {
            msg();
        }
    }

    /**
     * 集合中不包含obj时
     *
     * @param coll
     * @param obj
     * @param msg
     * @param <T>
     * @
     */
    public static <T> void notContains(Collection<T> coll, T obj, String msg) {
        if (!coll.contains(obj)) {
            msg(msg);
        }
    }

    /**
     * 集合中不包含obj时
     *
     * @param coll
     * @param obj
     * @param codeEnum
     * @param <T>
     * @
     */
    public static <T> void notContains(Collection<T> coll, T obj, ResultCodeEnum codeEnum) {
        if (!coll.contains(obj)) {
            msg(codeEnum);
        }
    }

    /**
     * 集合中不包含obj时
     *
     * @param coll
     * @param obj
     * @param <T>
     * @
     */
    public static <T> void notContains(Collection<T> coll, T obj) {
        if (!coll.contains(obj)) {
            msg();
        }
    }

    /**
     * 默认数据异常
     */
    public static void msg() {
        msg(ResultCodeEnum.DATA_ERROR);
    }

    /**
     * 抛出业务异常
     *
     * @param msg
     */
    public static void msg(String msg) {
        throw new BusinessException(msg);
    }

    /**
     * 抛出业务异常
     *
     * @param codeEnum
     */
    public static void msg(ResultCodeEnum codeEnum) {
        throw new BusinessException(codeEnum);
    }

    /**
     * 抛出自定义异常
     *
     * @param code
     * @param msg
     */
    public static void msg(Integer code, String msg) {
        throw new BusinessException(msg, code);
    }

    private static boolean isNull(String str) {
        if (str != null && str.length() != 0) {
            return true;
        }
        return false;
    }


}

