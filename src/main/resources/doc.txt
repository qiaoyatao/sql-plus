1、annotation: 注解包
   1、bean: 校验，转对象与crud工具注解
      1、AsName: 对象拷贝注解
      2、Delete: 删除注解
      3、Status: 修改状态注解
      4、Update: 添加修改注解
   2、model: 为sql-plus注解
      1、ID: 主键注解
      2、TableField: 字段注解
      3、TableName: 表注解
   3、valid: 校验注解
      1、Group: 校验分组注解
      2、OR: 或者注解
2、constants：常量包
   1、Symbol:为sql-plus和valid 常量
   2、SqlSymbol：为sql-plus模块查询常量
   3、Generator：代码生成器常量
3、enums：枚举包
   1、IdType：注解枚举
4、group：valid 校验预定义分组包
   1、Add: 添加预定义组
   2、Delete：删除预定义组
   3、Query：查询预定义组
   3、Update：更新预定义组
5、helper：sql执行器包，两种执行器
   1、SqlHelper: sql执行器
   2、SqlSession2：sql执行器
   3、SqlHelperUtil：sql工具
6、core：sql-plus基础包,sql-plus最核心包
   1、Columns: 字段
   2、Model：基类实例
   3、SqlFormat：sql格式化类
7、system：系统工具包
   1、bean：BeanUils工具包
       1、BeanUtil: 操作对象工具
       2、StringUtil：字符串类
   2、crud：对象sql-plus,Crud进行业务封装工具
       1、CRUDUtil: 封装crud工具
   3、page：分页包
       1、Page: 分页工具类
       2、PageInfo：分页返回工具
   4、sql：sql-plus工具包
       1、ORDER_BY: sql排序
       2、QueryModel：查询类
       3、SqlUtil：sql执行器工具
   5、valid：校验工具包
       1、Assertion: 断言工具
       2、Validation：自定义校验工具
       3、ValidatorUtil：系统实体验证工具类
   7、http：工具包
       1、RemoteServer: 远程调用工具
7、generator：代码生成器工具包
   1、util：工具包
      1、DataSource: 数据源类
      2、GeneratorUtil: 代码生成器类
      3、GUtil: 代码工具
      4、IsCrud: CRUD方法生成类
      5、TField: 字段修饰
   2、AutoGenerator：代码主类
   3、CrudConfig：crud方法配置
   4、GlobalConfig：全局配置
   5、PackageConfig：包配置

1、bug 如果 mybatis 低于2.0 SQL类分页是没有OFFSET和LIMIT，会出现
    com.qiaoyatao.sqlplus.core.format.SqlFormat$16.OFFSET(J)Ljava/lang/Object; 错误
    2.0.4 解决了这个问题。