/*
Navicat MySQL Data Transfer

Source Server         : 5.5.49
Source Server Version : 50549
Source Host           : localhost:3306
Source Database       : sql_plus

Target Server Type    : MYSQL
Target Server Version : 50549
File Encoding         : 65001

Date: 2020-10-10 17:05:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_account
-- ----------------------------
DROP TABLE IF EXISTS `sys_account`;
CREATE TABLE `sys_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '账户id',
  `password` varchar(255) DEFAULT NULL COMMENT '账户密码',
  `user_name` varchar(255) DEFAULT NULL COMMENT '员工姓名',
  `phone` varchar(20) DEFAULT NULL COMMENT '登录用户名',
  `creat_uid` int(20) DEFAULT NULL COMMENT '常见人id',
  `creat_uname` varchar(50) DEFAULT NULL COMMENT '创建人',
  `creat_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_uid` int(20) DEFAULT NULL COMMENT '修改人id',
  `modify_uname` varchar(50) DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(10) DEFAULT '0' COMMENT '0-未删除，1-删除',
  `is_use` tinyint(4) DEFAULT '1' COMMENT '0-禁用1-启用',
  `sorting` int(11) DEFAULT NULL COMMENT '排序',
  `remarks` text COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COMMENT='平台账户表';

-- ----------------------------
-- Records of sys_account
-- ----------------------------
INSERT INTO `sys_account` VALUES ('1', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '1', '这是我备注');
INSERT INTO `sys_account` VALUES ('2', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '2', '这是我备注');
INSERT INTO `sys_account` VALUES ('3', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '3', '这是我备注');
INSERT INTO `sys_account` VALUES ('4', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '4', '这是我备注');
INSERT INTO `sys_account` VALUES ('5', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '5', '这是我备注');
INSERT INTO `sys_account` VALUES ('6', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '6', '这是我备注');
INSERT INTO `sys_account` VALUES ('7', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '7', '这是我备注');
INSERT INTO `sys_account` VALUES ('8', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '8', '这是我备注');
INSERT INTO `sys_account` VALUES ('9', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:27', null, null, null, '0', '1', '9', '这是我备注');
INSERT INTO `sys_account` VALUES ('10', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '10', '这是我备注');
INSERT INTO `sys_account` VALUES ('11', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '11', '这是我备注');
INSERT INTO `sys_account` VALUES ('12', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '12', '这是我备注');
INSERT INTO `sys_account` VALUES ('13', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '13', '这是我备注');
INSERT INTO `sys_account` VALUES ('14', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '14', '这是我备注');
INSERT INTO `sys_account` VALUES ('15', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '15', '这是我备注');
INSERT INTO `sys_account` VALUES ('16', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '16', '这是我备注');
INSERT INTO `sys_account` VALUES ('17', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '17', '这是我备注');
INSERT INTO `sys_account` VALUES ('18', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '18', '这是我备注');
INSERT INTO `sys_account` VALUES ('19', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '19', '这是我备注');
INSERT INTO `sys_account` VALUES ('20', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '20', '这是我备注');
INSERT INTO `sys_account` VALUES ('21', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '21', '这是我备注');
INSERT INTO `sys_account` VALUES ('22', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '22', '这是我备注');
INSERT INTO `sys_account` VALUES ('23', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '23', '这是我备注');
INSERT INTO `sys_account` VALUES ('24', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '24', '这是我备注');
INSERT INTO `sys_account` VALUES ('25', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '25', '这是我备注');
INSERT INTO `sys_account` VALUES ('26', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '26', '这是我备注');
INSERT INTO `sys_account` VALUES ('27', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '27', '这是我备注');
INSERT INTO `sys_account` VALUES ('28', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '28', '这是我备注');
INSERT INTO `sys_account` VALUES ('29', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '29', '这是我备注');
INSERT INTO `sys_account` VALUES ('30', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '30', '这是我备注');
INSERT INTO `sys_account` VALUES ('31', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '31', '这是我备注');
INSERT INTO `sys_account` VALUES ('32', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '32', '这是我备注');
INSERT INTO `sys_account` VALUES ('33', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '33', '这是我备注');
INSERT INTO `sys_account` VALUES ('34', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '34', '这是我备注');
INSERT INTO `sys_account` VALUES ('35', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '35', '这是我备注');
INSERT INTO `sys_account` VALUES ('36', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '36', '这是我备注');
INSERT INTO `sys_account` VALUES ('37', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '37', '这是我备注');
INSERT INTO `sys_account` VALUES ('38', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '38', '这是我备注');
INSERT INTO `sys_account` VALUES ('39', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '39', '这是我备注');
INSERT INTO `sys_account` VALUES ('40', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '40', '这是我备注');
INSERT INTO `sys_account` VALUES ('41', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '41', '这是我备注');
INSERT INTO `sys_account` VALUES ('42', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '42', '这是我备注');
INSERT INTO `sys_account` VALUES ('43', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '43', '这是我备注');
INSERT INTO `sys_account` VALUES ('44', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '44', '这是我备注');
INSERT INTO `sys_account` VALUES ('45', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '45', '这是我备注');
INSERT INTO `sys_account` VALUES ('46', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '46', '这是我备注');
INSERT INTO `sys_account` VALUES ('47', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '47', '这是我备注');
INSERT INTO `sys_account` VALUES ('48', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '48', '这是我备注');
INSERT INTO `sys_account` VALUES ('49', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '49', '这是我备注');
INSERT INTO `sys_account` VALUES ('50', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '50', '这是我备注');
INSERT INTO `sys_account` VALUES ('51', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '51', '这是我备注');
INSERT INTO `sys_account` VALUES ('52', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '52', '这是我备注');
INSERT INTO `sys_account` VALUES ('53', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '53', '这是我备注');
INSERT INTO `sys_account` VALUES ('54', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '54', '这是我备注');
INSERT INTO `sys_account` VALUES ('55', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '55', '这是我备注');
INSERT INTO `sys_account` VALUES ('56', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '56', '这是我备注');
INSERT INTO `sys_account` VALUES ('57', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '57', '这是我备注');
INSERT INTO `sys_account` VALUES ('58', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '58', '这是我备注');
INSERT INTO `sys_account` VALUES ('59', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '59', '这是我备注');
INSERT INTO `sys_account` VALUES ('60', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '60', '这是我备注');
INSERT INTO `sys_account` VALUES ('61', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '61', '这是我备注');
INSERT INTO `sys_account` VALUES ('62', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '62', '这是我备注');
INSERT INTO `sys_account` VALUES ('63', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '63', '这是我备注');
INSERT INTO `sys_account` VALUES ('64', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '64', '这是我备注');
INSERT INTO `sys_account` VALUES ('65', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '65', '这是我备注');
INSERT INTO `sys_account` VALUES ('66', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '66', '这是我备注');
INSERT INTO `sys_account` VALUES ('67', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '67', '这是我备注');
INSERT INTO `sys_account` VALUES ('68', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '68', '这是我备注');
INSERT INTO `sys_account` VALUES ('69', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '69', '这是我备注');
INSERT INTO `sys_account` VALUES ('70', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '70', '这是我备注');
INSERT INTO `sys_account` VALUES ('71', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '71', '这是我备注');
INSERT INTO `sys_account` VALUES ('72', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '72', '这是我备注');
INSERT INTO `sys_account` VALUES ('73', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '73', '这是我备注');
INSERT INTO `sys_account` VALUES ('74', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '74', '这是我备注');
INSERT INTO `sys_account` VALUES ('75', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '75', '这是我备注');
INSERT INTO `sys_account` VALUES ('76', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '76', '这是我备注');
INSERT INTO `sys_account` VALUES ('77', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '77', '这是我备注');
INSERT INTO `sys_account` VALUES ('78', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '78', '这是我备注');
INSERT INTO `sys_account` VALUES ('79', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '79', '这是我备注');
INSERT INTO `sys_account` VALUES ('80', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '80', '这是我备注');
INSERT INTO `sys_account` VALUES ('81', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '81', '这是我备注');
INSERT INTO `sys_account` VALUES ('82', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '82', '这是我备注');
INSERT INTO `sys_account` VALUES ('83', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '83', '这是我备注');
INSERT INTO `sys_account` VALUES ('84', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '84', '这是我备注');
INSERT INTO `sys_account` VALUES ('85', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '85', '这是我备注');
INSERT INTO `sys_account` VALUES ('86', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '86', '这是我备注');
INSERT INTO `sys_account` VALUES ('87', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '87', '这是我备注');
INSERT INTO `sys_account` VALUES ('88', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '88', '这是我备注');
INSERT INTO `sys_account` VALUES ('89', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '89', '这是我备注');
INSERT INTO `sys_account` VALUES ('90', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '90', '这是我备注');
INSERT INTO `sys_account` VALUES ('91', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '91', '这是我备注');
INSERT INTO `sys_account` VALUES ('92', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '92', '这是我备注');
INSERT INTO `sys_account` VALUES ('93', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '93', '这是我备注');
INSERT INTO `sys_account` VALUES ('94', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '94', '这是我备注');
INSERT INTO `sys_account` VALUES ('95', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '95', '这是我备注');
INSERT INTO `sys_account` VALUES ('96', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '96', '这是我备注');
INSERT INTO `sys_account` VALUES ('97', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '97', '这是我备注');
INSERT INTO `sys_account` VALUES ('98', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '98', '这是我备注');
INSERT INTO `sys_account` VALUES ('99', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '99', '这是我备注');
INSERT INTO `sys_account` VALUES ('100', 'admin', '乔小乔', '15510304125', null, '乔小乔', '2020-10-10 17:04:28', null, null, null, '0', '1', '100', '这是我备注');
