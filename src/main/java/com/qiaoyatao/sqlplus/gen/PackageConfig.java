package com.qiaoyatao.sqlplus.gen;

import lombok.Data;

/**
 * 生成代码：包名配置
 * author： qiaoyatao
 * date: 2019年12月23日16:13:14
 */
@Data
public class PackageConfig {

    private String parent = "com.qiaoyatao";

    private String controller = "controller";

    private String service = "service";

    private String serviceImpl = "service.impl";

    private String mapper = "mapper";

    private String xml = "mapper-xml";

    private String model = "model";

    private String dto = "dto";

    private String vo = "vo";

    private String api = "api";

    private String jpa = "jpa";

    private String config = "config";

    private String test = "test";
}
