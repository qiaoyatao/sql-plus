package com.qiaoyatao.sqlplus.gen;

import lombok.Data;
import java.sql.Connection;

/**
 * 生成代码：全局配置
 * author： qiaoyatao
 * date: 2019年12月23日16:13:14
 */
@Data
public class GlobalConfig {

    //文件输出路径
    private String outputDir = "D:";
    //作者
    private String author = "乔小乔";
    //Controller文件后缀名
    private String controllerName = "Controller";
    //Service接口文件后缀名
    private String serviceName = "Service";
    //Service实现文件后缀名
    private String serviceImplName = "ServiceImpl";
    //mapper后缀名称
    private String mapperName = "Mapper";

    private String daoName = "Repository";
    //swagger2
    private boolean swagger2 = false;
    //是否生成
    private boolean tableField = false;
    //操作库
    private String database;
    // 是否创建返回map
    private boolean baseResultMap = true;
    //是否创建查询列
    private boolean baseColumnList = true;
    //是否生成crud方法
    private boolean isCrud = false;
    //是否生成api
    private boolean api = false;
    //是否生成api中文名称
    private boolean apiCH = false;
    //是否生成jpa
    private boolean jpa = false;
    //是否对新增修改增强
    private boolean isUpdate = true;
    //是否生成vo
    private boolean vo = false;
    //是否生成xmlcrud方法
    private boolean crudXml = false;
    //Mapper是否继承BaseMapper
    private boolean baseMapper = false;
    //Mapper是否继承supperMapper
    private boolean supperMapper = false;
    //包路径配置
    private PackageConfig pk;
    //是否生成@PreAuthorize
    private boolean preAuthorize = false;
    //需要生成Crud
    private String[] crudInclude;
    //排除生成的crud
    private String[] crudExclude;
    //model继承父类对象
    private String baseModel;
    //实体类是否默认继承Model
    private boolean model = true;
    //是否生成配置文件
    private boolean config = false;
    //是否生成vue文件
    private boolean vue = false;
    //是否添加log注解
    private boolean log = false;
    //是否为controller包装返回信息
    private boolean result = false;
    //实体类添加validate方法
    private boolean validate = false;
    //是否生成测试方法
    private boolean test = false;
    //连接信息
    private Connection connection;
    // 排除生成的表
    private String[] exclude;
    //排除字段
    private String[] excludeField;
    //需要生成的表
    private String[] include;
    //指定删除字段添加@Delete
    private String del;
    //指定删除字段添加@status
    private String status;

}