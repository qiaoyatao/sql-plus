package com.qiaoyatao.sqlplus.gen.builder;

import lombok.Data;
import java.sql.*;

//数据库连接
@Data
public class DataSource {

    //数据库连接信息
    private String url;

    //数据库驱动
    private String driverName;

    //用户名
    private String username;

    //密码
    private String password;

    //连接信息
    private Connection conn;

    //创建连接
    public Connection connection() {
        try {
            Class.forName(driverName);
            return conn = DriverManager.
                    getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //关闭连接
    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}