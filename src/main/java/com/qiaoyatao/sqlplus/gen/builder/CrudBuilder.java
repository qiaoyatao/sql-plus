package com.qiaoyatao.sqlplus.gen.builder;

import com.qiaoyatao.sqlplus.constants.SP;
import com.qiaoyatao.sqlplus.gen.*;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import java.io.PrintWriter;
import java.util.Date;

/**
 * 自动生成Crud方法
 * author： qiaoyatao
 * date: 2020年1月14日15:09:11
 */
public class CrudBuilder {

    //web层生成crud方法
    public static void webCrud(GlobalConfig global, String table, PrintWriter print) {
        String serviveName = SpUtil.toUpperCase(table) + global.getServiceName();//首字母大写
        String model = SpUtil.toUpperCase(table);
        String lowerCase = StrUtil.toCamelCase(table) + global.getServiceName();//手字母小写
        print.println("    @Autowired");
        print.print("    private " + serviveName + " ");
        print.println(lowerCase + ";");
        print.println();
        update("add", model, lowerCase, global, print);
        update("update", model, lowerCase, global, print);
        queryById(model, lowerCase, global, print);
        delete(model, lowerCase, global, print);
        queryPage(model, lowerCase, global, print);
    }

    //web层生成分页
    private static void queryPage(String model, String lowerCase, GlobalConfig global, PrintWriter print) {
        if (global.isPreAuthorize()) {
            print.println("    @PreAuthorize(value =\"" + model + ":queryPage\")");
        }
        print.println("    @PostMapping(\"/queryPage\")");
        print.print("    public PageInfo<" + model + "> queryPage(@RequestBody ");
        print.println(SpUtil.toUpperCase(model) + "DTO model){");
        print.println("        return " + lowerCase + ".queryPage" + "(model);");
        print.println("    }");
        print.println();
    }

    //web层生成id查询
    private static void queryById(String model, String lowerCase, GlobalConfig global, PrintWriter print) {
        if (global.isPreAuthorize()) {
            print.println("    @PreAuthorize(value =\"" + model + ":queryById\")");
        }
        print.println("    @GetMapping(\"/queryById/{id}\")");
        print.println("    public " + model + " queryById(@PathVariable Integer id){");
        print.println("        return " + lowerCase + ".queryById" + "(id);");
        print.println("    }");
        print.println();
    }

    //web层生成删除
    private static void delete(String model, String lowerCase, GlobalConfig global, PrintWriter print) {
        if (global.isPreAuthorize()) {
            print.println("    @PreAuthorize(value =\"" + model + ":delete\")");
        }
        print.println("    @GetMapping(\"/delete/{id}\")");
        print.println("    public void  delete(@PathVariable Integer id){");
        print.println("        " + lowerCase + ".delete" + "(id);");
        print.println("    }");
        print.println();
    }

    //web层生成add,update方法
    private static void update(String name, String model, String lowerCase, GlobalConfig global, PrintWriter print) {
        if (global.isPreAuthorize()) {
            print.println("    @PreAuthorize(value =\"" + model + ":" + name + "\")");
        }
        print.println("    @PostMapping(\"/" + name + "\")");
        print.print("    public void  " + name + "(");
        print.println("@RequestBody " + model + " modle) {");
        print.println("        " + lowerCase + "." + name + "(modle);");
        print.println("    }");
        print.println();
    }

    //service接口生成crud方法
    public static void serviceInCrud(GlobalConfig global, String table, PrintWriter print) {
        String model = SpUtil.toUpperCase(table);
        print.println("    void add(" + model + " " + "model);");
        print.println();
        print.println("    void update(" + model + " " + "model);");
        print.println();
        print.println("    " + model + " queryById(Integer id);");
        print.println();
        print.println("    void" + " delete(Integer id);");
        print.println();
        print.println("    " + "PageInfo<" + model + "> queryPage(#DTO model);".replaceFirst("#", SpUtil.toUpperCase(model)));
        print.println();
    }

    public static void serviceCrud(GlobalConfig global, String table, PrintWriter print) {
        String model = SpUtil.toUpperCase(table);
        servieUpdate("add", model, print);
        servieUpdate("update", model, print);
        servieId(model, print);
        servieDelete(model, print);
        serviePage(model, print);
    }

    //service实现层生成add,update方法
    private static void servieUpdate(String name, String model, PrintWriter print) {
        print.println("    @Override");
        print.print("    public void " + name + "(");
        print.println(model + " model) {");
        print.println("        CrudUtil.update(model);");
        print.println("    }");
        print.println();
    }

    //service实现层生成id查询
    private static void servieId(String model, PrintWriter print) {
        print.println("    @Override");
        print.println("    public " + model + " queryById(Integer id) {");
        print.println("        return new " + model + "().selectByid(id);");
        print.println("    }");
        print.println();
    }

    //service实现层生成删除
    private static void servieDelete(String model, PrintWriter print) {
        print.println("    @Override");
        print.println("    public void delete(Integer id) {");
        print.println("        CrudUtil.delete(id," + model + ".class);");
        print.println("    }");
        print.println();
    }

    //web层生成分页
    private static void serviePage(String model, PrintWriter print) {
        print.println("    @Override");
        print.println("    " + "public PageInfo<" + model + "> queryPage(#DTO model){".replaceFirst("#", SpUtil.toUpperCase(model)));
        print.println("        return new " + model + "().selectPage(model,model);");
        print.println("    }");
        print.println();
    }

    //打印备注信息
    public static void remark(String table, PrintWriter print, GlobalConfig global) {
        print.println("/**");
        print.println(SP.AUTHOR + global.getAuthor());
        print.println(SP.DATE + StrUtil.formatDate(new Date()));
        String remark = SpUtil.remarks(table, global);
        if (remark != null) {
            print.println(" * " + remark);
        }
        print.println(" */");
    }


}
