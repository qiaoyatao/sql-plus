package com.qiaoyatao.sqlplus.gen.builder;

import com.qiaoyatao.gen.constants.Gen;
import com.qiaoyatao.sqlplus.constants.SP;
import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.gen.GlobalConfig;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import org.apache.velocity.*;
import org.apache.velocity.app.Velocity;
import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * 代码生成器方法
 * author： qiaoyatao
 * date: 2020年1月28日13:24:05
 */
public class SpUtil {

    //创建实体
    public static String path(GlobalConfig global, String path, String name) {
       return String.format(SP.PATH,global.getOutputDir(),
               global.getPk().getParent()
               .replaceAll("\\.", "/")
               ,path,name);
    }

    public static List<SpField> getField(String table, GlobalConfig global) {
        List<SpField> list = new ArrayList<>();
        try {
            PreparedStatement ps = global.getConnection().prepareStatement(String.format(SP.SHOW_FULL,table));
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                if (search(global.getExcludeField(), result.getString(1))) {
                    continue;
                }
                list.add(new SpField(result));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<String> getTable(GlobalConfig global) {
        if (global.getInclude() != null) {
            return Arrays.asList(global.getInclude());
        }
        List<String> list = new ArrayList<>();
        try {
            ResultSet rs = global.getConnection().getMetaData()
                    .getTables(global.getDatabase(), null, null, new String[]{"TABLE"});
            while (rs.next()) {
                list.add(rs.getString(3).toLowerCase());
            }
            if (global.getExclude() != null) {
                for (String table : global.getExclude()) {
                    list.remove(table);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 获取表备注信息
     *
     * @param table
     * @param global
     * @return
     */
    public static String remarks(String table, GlobalConfig global) {
        try {
            Statement stmt = global.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(String.format(Symbol.SHOW_CREATE,table));
            while (rs.next()) {
                //取值
                String remarks = rs.getString(2);
                int index = remarks.indexOf("COMMENT='");
                if (index < 0) {
                    return table;
                }
                String comment = remarks.substring(index + 9);
                return comment.substring(0, comment.length() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String toUpperCase(String table) {
        if (table==null){return null;};
        table = StrUtil.toCamelCase(table);
        return table.substring(0, 1).toUpperCase()+table.substring(1);
    }

    public static String toType(String type) {
        if (type.contains("bigint")) {
            return "Long ";
        }
        if (type.contains("int") || type.contains("tinyint") || type.contains("mediumint")) {
            return "Integer ";
        }
        if (type.contains("varchar") || type.contains("text") || type.contains("char")) {
            return "String ";
        }
        if (type.contains("datetime") || type.contains("year") || type.contains("date")) {
            return "Date ";
        }
        if (type.contains("bit")) {
            return "Boolean ";
        }
        if (type.contains("float")) {
            return "Float ";
        }
        if (type.contains("double")) {
            return "Double ";
        }
        if (type.contains("char")) {
            return "char ";
        }
        if (type.contains("smallint")) {
            return "Short ";
        }
        if (type.contains("decimal")) {
            return "BigDecimal ";
        } else {
            return "String ";
        }
    }

    public static boolean search(String[] array, String search) {
        if(array==null){return false;}
        for (String key : Arrays.asList(array)) {
            if (key.toLowerCase().equals(search.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public static void createFile(File checkFile) {
        try {
            if (checkFile.exists() && checkFile.isFile()) {
                checkFile.delete();
                checkFile.createNewFile();
            }
            File parentFile = checkFile.getParentFile();
            if (parentFile.exists()) {
                if (parentFile.isFile()) {
                    parentFile.delete();
                    parentFile.mkdirs();
                }
            } else {
                parentFile.mkdirs();
            }
            checkFile.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //vm输出到文件
    public static void createVM(String vmName, String path, VelocityContext ctx) {
        try {
            PrintWriter print = new PrintWriter(path);
            StringWriter sw = new StringWriter();
            getTemplate(vmName).merge(ctx, sw);
            print.println(sw.toString());
            print.flush();
            print.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //获取模板
    private static Template getTemplate(String template) {
        Properties prop = new Properties();
        prop.put(Gen.LOADER, Gen.VELOCITY);
        Velocity.init(prop);
        return  Velocity.getTemplate("templates/"+template, "UTF-8");
    }

    //获取主键
    public static SpField primaryKey(String table, GlobalConfig global) {
        try {
            ResultSet result = global.getConnection().prepareStatement(SP.SELECT + table)
                    .executeQuery();
            ResultSetMetaData data = result.getMetaData();
            for (int i = 1; i <= data.getColumnCount(); i++) {
                if (data.isAutoIncrement(i)) {
                   return new SpField(data,i);
                }
            }
            return new SpField(data,1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
