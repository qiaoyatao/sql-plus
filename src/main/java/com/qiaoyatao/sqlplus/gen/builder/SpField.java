package com.qiaoyatao.sqlplus.gen.builder;

import com.qiaoyatao.sqlplus.constants.SP;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

/**
 * 字段名称
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Data
public class SpField {

    //字段名称
    private String field;

    //成员变量
    private String variable;

    //字段备注
    private String comment;

    //包装字段
    private String privateField;

    //数据库类型
    private String columnType;

    //数据库java类型
    private String columnClass;

    public SpField(ResultSet result) throws Exception {
        setField(result.getString(1).toLowerCase());
        setVariable(StrUtil.toCamelCase(result.getString(1).toLowerCase()));
        setComment(result.getString(9).toLowerCase());
        setPrivateField(String.format(SP.PRIVATE, SpUtil.toType(result.getString(2)
                .toLowerCase()), StrUtil.toCamelCase(result.getString(1).toLowerCase())));
        setColumnType(result.getString(2));
        setColumnClass(SpUtil.toType(result.getString(2).toLowerCase()));
    }

    public SpField(ResultSetMetaData data, int index) throws Exception {
        setField(data.getColumnName(index).toLowerCase());
        setColumnType(SpUtil.toType(data.getColumnTypeName(index).toLowerCase()));
    }
}
