package com.qiaoyatao.sqlplus.gen;

import com.qiaoyatao.sqlplus.constants.*;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.gen.builder.*;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import lombok.extern.slf4j.Slf4j;
import java.io.*;
import java.util.List;

/**
 * 生成代码工具
 * author： qiaoyatao
 * date: 2019年12月23日16:13:14
 */
@Slf4j
public class GenFile extends SpUtil {

    private static void createDto(String table, String name, List<SpField> fields, String packages,
                                  GlobalConfig global) {
        try {
            PackageConfig pk = global.getPk();
            String DtpPath = path(global, packages, name+SP.JAVA);
            createFile(new File(DtpPath));
            PrintWriter print = new PrintWriter(DtpPath);
            print.append(SP.PACKAGE + pk.getParent() + "." + packages + ";");
            print.println();
            CrudBuilder.remark(table, print, global);
            print.println("@Data ");
            print.append(SP.PUBLIC + name);
            print.println("{");
            print.println();
            for (SpField field : fields) {
                if (field.getComment().length() != 0) {
                    print.println("    //" + field.getComment());
                    if (global.isSwagger2()) {
                        print.println(String.format(SP.APIMODELPROPERTY,field.getComment()));
                    }
                }
                if (global.isTableField()) {
                    if (!field.getField().equals(StrUtil.toCamelCase(field.getField()))) {
                        print.println(String.format(SP.TABLEFIELD,field.getField()));
                    }
                }
                print.println(field.getPrivateField());
                print.println();
            }
            print.println("}");
            print.flush();
            log.info("DTO生成完毕" + DtpPath);
        } catch (Exception e) {
            e.printStackTrace();
            //doesn't exist数据库连接失败
        }
    }

    private static void createDto(String table, GlobalConfig global) {
        try {
            PackageConfig pk = global.getPk();
            String name = toUpperCase(table) + "DTO";
            String path = path(global, pk.getDto(), name+SP.JAVA);
            createFile(new File(path));
            PrintWriter print = new PrintWriter(path);
            print.println(SP.PACKAGE + pk.getParent() + "." + pk.getDto() + ";");
            print.println();
            CrudBuilder.remark(table, print, global);
            print.println("@Data ");
            print.append(SP.PUBLIC + name);
            print.append(SP.EXTENDS + "Page");
            print.println("{");
            print.println();
            print.println("}");
            print.flush();
            log.info("DTO生成完毕" + path);
        } catch (Exception e) {
            e.printStackTrace();
            //doesn't exist数据库连接失败
        }
    }


    public static void createModel(GlobalConfig global) {
        PackageConfig pk = global.getPk();
        getTable(global).forEach(table -> {
        try {
            String name = toUpperCase(table);
            String path = path(global, pk.getModel(), name+ SP.JAVA);
            createFile(new File(path));
            PrintWriter print = new PrintWriter(path);
            print.println(SP.PACKAGE + pk.getParent() + "." + pk.getModel() + ";");
            print.println();
            CrudBuilder.remark(table, print, global);
            print.println("@Data ");
            print.append(SP.PUBLIC + name);
            if (global.getBaseModel() != null && global.getBaseModel().length() != 0) {
                print.append(SP.EXTENDS + global.getBaseModel() + "<");
            } else {
                print.append(SP.EXTENDS + SP.MODEL + "<");
            }
            print.append(name + ">");
            print.println("{");
            print.println();
            List<SpField> fields = getField(table, global);
            for (SpField field : fields) {
                if (field.getComment().length() != 0) {
                    print.println("    //" + field.getComment());
                    if (global.isSwagger2()) {
                        print.println(String.format(SP.APIMODELPROPERTY,field.getComment()));
                    }
                }
                if (global.isTableField()) {
                    if (!field.getField().equals(StrUtil.toCamelCase(field.getField()))) {
                        print.println(String.format(SP.TABLEFIELD, field.getField()));
                    }
                }
                if (field.getField().equals(global.getDel())) {
                    print.println("    @Delete");
                } else if (field.getField().equals(global.getStatus())) {
                    print.println("    @Status");
                }
                print.println(field.getPrivateField());
                print.println();
            }
            print.println("}");
            print.flush();
            log.info("实体类生成完毕" + path);
            createDto(table, global);
            if (global.isVo()) {
                createDto(table, name + "VO", fields, "vo", global);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //doesn't exist数据库连接失败
        }
        });
    }

    //创建web文件
    public static void createController(GlobalConfig global) {
        PackageConfig pk = global.getPk();
        getTable(global).forEach(table -> {
        try {
            String name = toUpperCase(table) + global.getControllerName();
            String path = path(global, pk.getController(), name+ SP.JAVA);
            createFile(new File(path));
            PrintWriter print = new PrintWriter(path);
            print.println(SP.PACKAGE + pk.getParent() + "." + pk.getController() + ";");
            print.println();
            CrudBuilder.remark(table, print, global);
            if (global.isSwagger2()) {
                String remark = remarks(table, global);
                if (remark != null) {
                    print.println(String.format(SP.API, remark));
                }
            }
            print.println("@RestController");
            print.println(String.format(SP.MAPPING, table));
            print.append(SP.PUBLIC + name);
            print.println("{");
            print.println();
            //crud方法
            if (global.isCrud()) {
                //需要生成表
                if (global.getCrudInclude() == null) {
                    //排除的
                    if (!search(global.getCrudExclude(), table)) {
                        CrudBuilder.webCrud(global, table, print);
                    }
                } else {
                    //需要生成的
                    if (search(global.getCrudInclude(), table)) {
                        CrudBuilder.webCrud(global, table, print);
                    }
                }
            }
            print.println();
            print.println("}");
            print.flush();
            log.info("Controller生成完毕" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        });
    }

    public static void createService(GlobalConfig global) {
        PackageConfig pk = global.getPk();
        getTable(global).forEach(table -> {
        try {
            String name = toUpperCase(table) + global.getServiceName();
            String path = path(global, pk.getService(), name+SP.JAVA);
            createFile(new File(path));
            PrintWriter print = new PrintWriter(path);
            print.println(SP.PACKAGE + pk.getParent() + "." + pk.getService() + ";");
            print.println();
            CrudBuilder.remark(table, print, global);
            print.append(SP.INTERFACE + name);
            print.println("{");
            print.println();
            //生成crud方法
            if (global.isCrud()) {
                //需要生成表
                if (global.getCrudInclude() == null) {
                    //排除的
                    if (!search(global.getCrudExclude(), table)) {
                        CrudBuilder.serviceInCrud(global, table, print);
                    }
                } else {
                    //需要生成的
                    if (search(global.getCrudInclude(), table)) {
                        CrudBuilder.serviceInCrud(global, table, print);
                    }
                }
            }
            print.println("}");
            print.flush();
            //实现
            String newName = name + global.getServiceImplName();
            String newPath = path(global, pk.getServiceImpl().replaceAll("\\.", "/"), newName+SP.JAVA);
            createFile(new File(newPath));
            PrintWriter service = new PrintWriter(newPath);
            service.println(SP.PACKAGE + pk.getParent() + "." + pk.getServiceImpl() + ";");
            service.println();
            CrudBuilder.remark(table, service, global);
            service.println("@Service ");
            service.append(SP.PUBLIC + newName);
            service.append(SP.IMPLEMENTS + name);
            service.println("{");
            service.println();
            //生成crud方法
            if (global.isCrud()) {
                //需要生成表
                if (global.getCrudInclude() == null) {
                    //排除的
                    if (!search(global.getCrudExclude(), table)) {
                        CrudBuilder.serviceCrud(global, table, service);
                    }
                } else {
                    //需要生成的
                    if (search(global.getCrudInclude(), table)) {
                        CrudBuilder.serviceCrud(global, table, service);
                    }
                }
            }
            service.println("}");
            service.flush();
            log.info("Service生成完毕" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        });
    }

    //创建Mapper文件
    public static void createMapper(GlobalConfig global) {
        PackageConfig pk = global.getPk();
        getTable(global).forEach(table -> {
        try {
            String name = toUpperCase(table) + global.getMapperName();
            String path = path(global, pk.getMapper(), name+SP.JAVA);
            createFile(new File(path));
            PrintWriter print = new PrintWriter(path);
            print.println(SP.PACKAGE + pk.getParent() + "." + pk.getMapper() + ";");
            print.println();
            CrudBuilder.remark(table, print, global);
            print.println("@Mapper ");
            print.append(SP.INTERFACE + name);
            print.println("{");
            print.println();
            print.println("}");
            print.flush();
            log.info("Mapper接口生成完毕" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        });
    }

    public static void createXml(GlobalConfig global) {
        PackageConfig pk = global.getPk();
        getTable(global).forEach(table -> {
        try {
            String name = toUpperCase(table) + global.getMapperName();
            String path = path(global, pk.getXml(), name+ ".xml");
            createFile(new File(path));
            PrintWriter print = new PrintWriter(path);
            print.println(SP.XML);
            print.println(SP.DOCTYPE);
            print.append(SP.NAMESPACE + '"' + pk.getParent() + "." + pk.getMapper() + "." + name + '"');
            print.println(Symbol.GT.trim());
            print.println();
            String remark = remarks(table, global);
            if (remark != null) {
                print.println(String.format(SP.REMARKS, remark));
            }
            //通用查询结果(是否生成)
            List<SpField> fields = getField(table, global);
            if (global.isBaseColumnList()) {
                print.println(SP.BASE);
                print.println(SP.SQL);
                StringBuffer sb = new StringBuffer();
                sb.append(SP.KG);
                for (SpField spField : fields) {
                    sb.append(spField.getField());
                    sb.append(!spField.getVariable().equals(spField.getField()) ? Symbol.AS + spField.getVariable() : "");
                    sb.append(",");
                }
                print.println(SqlUtil.substr(sb));
                print.println(SP.SQLTO);
                print.println();
            }
            //通用返回结果(是否生成)
            if (global.isBaseResultMap()) {
                print.println(SP.BASER);
                print.print(SP.RESULTMAP);
                print.println(pk.getParent() + "." + pk.getModel() + "." + toUpperCase(table) + "\">");
                for (SpField spField : fields) {
                    if (!spField.getVariable().equals(spField.getField())) {
                        print.println(SP.COLUMN + spField.getField() + '"' + SP.PROPERTY + spField.getVariable() + "\" />");
                    }
                }
                print.println(SP.RESULTMAPTO);
            }
            print.println();
            print.println(SP.MAPPER);
            print.flush();
            log.info("xml生成完毕" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
      });
    }

    public static void execute(GlobalConfig global) {
        long startTime = System.currentTimeMillis();
        GenFile.createModel(global);
        GenFile.createController(global);
        GenFile.createService(global);
        GenFile.createMapper(global);
        GenFile.createXml(global);
        long endTime = System.currentTimeMillis();
        log.info(Symbol.run_time + (endTime - startTime) + "ms");
    }
}