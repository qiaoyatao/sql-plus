package com.qiaoyatao.sqlplus.gen;

import com.qiaoyatao.sqlplus.constants.*;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.gen.builder.*;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import java.io.File;
import java.util.*;

/**
 * 第二种：模板代码生成工具
 * author： qiaoyatao
 * date: 2020年1月29日16:06:17
 */
@Slf4j
public class SpGenerator extends SpUtil {

    public static void createTest(GlobalConfig global) {
        if (!global.isTest()){return;}
        PackageConfig pk = global.getPk();
        getTable(global).forEach(table -> {
        String name = toUpperCase(table);
        String path = path(global,pk.getTest(), "Test"+name+SP.JAVA);
        createFile(new File(path));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Velocity.MODEL, name);
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.TABLE, table);
        ctx.put(Velocity.REMARK, remarks(table, global));
        createVM(Velocity.test, path, ctx);
        log.info("dto生成完毕" + path);
        });
    }

    public static void createDto(GlobalConfig global) {
        if (!global.isCrud()){return;}
        PackageConfig pk = global.getPk();
        getTable(global).forEach(table -> {
        String name = toUpperCase(table) + "DTO";
        String path = path(global,pk.getDto(), name+SP.JAVA);
        createFile(new File(path));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Velocity.MODEL, name);
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.REMARK, remarks(table, global));
        ctx.put(Velocity.DTO, true);
        createVM(Velocity.dto, path, ctx);
        log.info("dto生成完毕" + path);
        });
    }

    public static void createVo(GlobalConfig global) {
        if (!global.isCrud()){return;}
        PackageConfig pk = global.getPk();
        getTable(global).forEach(table -> {
        String name = toUpperCase(table) + "VO";
        String path = path(global, pk.getVo(), name+SP.JAVA);
        createFile(new File(path));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Velocity.MODEL, name);
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.FIELDS, getField(table, global));
        ctx.put(Velocity.REMARK, remarks(table, global));
        createVM(Velocity.dto, path, ctx);
        log.info("vo生成完毕" + path);
        });
    }

    public static void createModel(GlobalConfig global) {
        PackageConfig pc = global.getPk();
        getTable(global).forEach(table -> {
        String name = toUpperCase(table);
        String path = path(global, pc.getModel(), name+SP.JAVA);
        createFile(new File(path));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Velocity.BASE_MODEL, StringUtils.isBlank(global.getBaseModel())? SP.MODEL :global.getBaseModel());
        ctx.put(Velocity.MODEL, name);
        ctx.put(Velocity.FIELDS, getField(table, global));
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.TABLE, table);
        ctx.put(Velocity.REMARK, remarks(table, global));
        ctx.put(Velocity.PRIMARYKEY, primaryKey(table, global));
        createVM(Velocity.model, path, ctx);
        log.info("model生成完毕" + path);
       });
    }

    public static void createController(GlobalConfig global) {
        PackageConfig pc = global.getPk();
        getTable(global).forEach(table -> {
        String name = toUpperCase(table) + global.getControllerName();
        String path = path(global, pc.getController(), name+SP.JAVA);
        createFile(new File(path));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.TABLE, table);
        ctx.put(Velocity.REMARK, remarks(table, global));
        ctx.put(Velocity.MODEL, toUpperCase(table));
        ctx.put(Velocity.SERVICE, StrUtil.toCamelCase(table) + global.getServiceName());
        ctx.put(Velocity.PRIMARYKEY, primaryKey(table, global));
        createVM(Velocity.controller, path, ctx);
        log.info("contriller生成完毕" + path);
        });
    }

    public static void createService(GlobalConfig global) {
        PackageConfig pc = global.getPk();
        getTable(global).forEach(table -> {
        VelocityContext ctx = new VelocityContext();
        String name = toUpperCase(table) + global.getServiceName();
        String path = path(global, pc.getService(), name+SP.JAVA);
        SpField primaryKey = primaryKey(table, global);
        createFile(new File(path));
        ctx.put(Velocity.MODEL, toUpperCase(table));
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.REMARK, remarks(table, global));
        ctx.put(Velocity.PRIMARYKEY, primaryKey);
        createVM(Velocity.service, path, ctx);
        log.info("servie生成完毕" + path);
        //生成实现类
        VelocityContext ctx1 = new VelocityContext();
        String newName = toUpperCase(table) + global.getServiceImplName();
        String newPath = path(global, pc.getServiceImpl().replaceAll("\\.", "/"), newName+SP.JAVA);
        createFile(new File(newPath));
        ctx1.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx1.put(Velocity.GLOBAL, global);
        ctx1.put(Velocity.REMARK, remarks(table, global));
        ctx1.put(Velocity.MODEL, toUpperCase(table));
        ctx1.put(Velocity.CAMELCASE, StrUtil.toCamelCase(table));
        primaryKey.setField(StrUtil.toCamelCase(primaryKey.getField()));
        ctx1.put(Velocity.PRIMARYKEY, primaryKey);
        createVM(Velocity.achieve, newPath, ctx1);
        log.info("servie实现生成完毕" + newPath);
       });
    }

    public static void createMapper(GlobalConfig global) {
        PackageConfig pc = global.getPk();
        getTable(global).forEach(table -> {
        VelocityContext ctx = new VelocityContext();
        String path = path(global, pc.getMapper(), toUpperCase(table) + global.getMapperName()+SP.JAVA);
        createFile(new File(path));
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.REMARK, remarks(table, global));
        ctx.put(Velocity.MODEL, toUpperCase(table));
        createVM(Velocity.mapper, path, ctx);
        log.info("mapper生成完毕" + path);
      });
    }

    public static void createXml(GlobalConfig global) {
        PackageConfig pc = global.getPk();
        getTable(global).forEach(table -> {
        String path = path(global, pc.getXml(), toUpperCase(table) + global.getMapperName()+SP.XML_MAPPER);
        createFile(new File(path));
        List<SpField> lists = getField(table, global);
        String columnList = null;
        if (global.isBaseColumnList()) {
            StringBuffer sb = new StringBuffer();
            for (SpField spField : lists) {
                sb.append(spField.getField());
                sb.append(!spField.getVariable().equals(spField.getField())?Symbol.AS + spField.getVariable():"");
                sb.append(",");
            }
            columnList = SqlUtil.substr(sb);
        }
        VelocityContext ctx = new VelocityContext();
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.REMARK, remarks(table, global));
        ctx.put(Velocity.COLUMN, columnList);
        ctx.put(Velocity.MODEL, toUpperCase(table));
        ctx.put(Velocity.TABLE, table);
        ctx.put(Velocity.LISTS, lists);
        ctx.put(Velocity.PRIMARYKEY, primaryKey(table, global));
        createVM(Velocity.xml, path, ctx);
        log.info("xml生成完毕" + path);
       });
    }

    public static void creteApi(GlobalConfig global) {
        if (!global.isApi()){return;}
        PackageConfig pc = global.getPk();
        getTable(global).forEach(table -> {
        String name = toUpperCase(table) + global.getControllerName();
        String remarks = remarks(table, global);
        String apiName = global.isApiCH() == true ? (remarks == null ? name : remarks) + "接口文档" : name;
        String path = path(global, pc.getApi(), apiName+SP.JSON);
        createFile(new File(path));
        List<SpField> fields = getField(table, global);
        VelocityContext ctx = new VelocityContext();
        ctx.put(Velocity.MODEL, name);
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.REMARK, remarks);
        ctx.put(Velocity.TABLE, table);
        ctx.put(Velocity.FIELDS, fields);
        createVM(Velocity.api, path, ctx);
        log.info("api接口文档" + path);
      });
    }

    public static void createEntity(GlobalConfig global) {
        if (!global.isJpa()){return;}
        PackageConfig pc = global.getPk();
        getTable(global).forEach(table -> {
        String name = toUpperCase(table);
        String path = path(global, pc.getJpa() + "\\" + pc.getModel(), name+SP.JAVA);
        createFile(new File(path));
        List<SpField> fields = getField(table, global);
        VelocityContext ctx = new VelocityContext();
        ctx.put(Velocity.MODEL, name);
        ctx.put(Velocity.FIELDS, fields);
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.TABLE, table);
        ctx.put(Velocity.REMARK, remarks(table, global));
        ctx.put(Velocity.PRIMARYKEY, primaryKey(table, global));
        createVM(Velocity.entity, path, ctx);
        log.info("jpa-实体类生成完毕" + path);
       });
    }

    public static void createDao(GlobalConfig global) {
        if (!global.isJpa()){return;}
        PackageConfig pc = global.getPk();
        getTable(global).forEach(table -> {
        VelocityContext ctx = new VelocityContext();
        String name = toUpperCase(table) + global.getDaoName();
        String path = path(global, pc.getJpa() + "\\" + pc.getMapper(), name+SP.JAVA);
        createFile(new File(path));
        ctx.put(Velocity.DATE, StrUtil.formatDate(new Date()));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.REMARK, remarks(table, global));
        ctx.put(Velocity.MODEL, toUpperCase(table));
        ctx.put(Velocity.PRIMARYKEY, primaryKey(table, global));
        createVM(Velocity.dao, path, ctx);
        log.info("jpa-dao生成完毕" + path);
       });
    }

    public static void createConfig(GlobalConfig global) {
        if (!global.isConfig()){return;}
        PackageConfig pc = global.getPk();
        for (String model : SP.CONFIG) {
            VelocityContext ctx = new VelocityContext();
            String end = model.contains(SP.XML_MAPPER) ? "" : SP.JAVA;
            String path = path(global, pc.getConfig()+"/"+model,"");
            path=path.substring(0,path.length()-1)+end;
            createFile(new File(path));
            ctx.put(Velocity.GLOBAL, global);
            ctx.put(Velocity.MODEL, model.substring(0, model.indexOf("/")));
            createVM(pc.getConfig()+"/" + (model.contains(SP.XML_MAPPER) ?
                    model : model + Velocity.java), path, ctx);
            log.info("config生成完毕" + path);
        }
    }

    public static void createVue(GlobalConfig global) {
        if (!global.isVue()){return;}
        getTable(global).forEach(table -> {
        VelocityContext ctx = new VelocityContext();
        String name = toUpperCase(table);
        String path = path(global, "vue", name+".vue");
        List<SpField> fields = getField(table, global);
        createFile(new File(path));
        ctx.put(Velocity.GLOBAL, global);
        ctx.put(Velocity.FIELDS, fields);
        ctx.put(Velocity.REMARK, remarks(table, global));
        createVM("vue/vue.vue.vm", path, ctx);
        log.info("vue生成完毕" + path);
       });
    }
    /**
     * 生成三层类文件
     *
     * @param global
     */
    public static void execute(GlobalConfig global) {
        long startTime = System.currentTimeMillis();
        createModel(global);
        createController(global);
        createService(global);
        createMapper(global);
        createXml(global);
        createDto(global);
        createVo(global);
        creteApi(global);
        createTest(global);
        createEntity(global);
        createDao(global);
        createConfig(global);
        createVue(global);
        long endTime = System.currentTimeMillis();
        log.info(Symbol.run_time + (endTime - startTime) + "ms");
    }


}