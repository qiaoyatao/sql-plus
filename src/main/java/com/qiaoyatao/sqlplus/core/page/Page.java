package com.qiaoyatao.sqlplus.core.page;

import com.qiaoyatao.sqlplus.annotation.model.TableField;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 分页工具
 * author： qiaoyatao
 * date: 2019年11月17日15:20:55
 */
@Data
@Accessors(chain = true)
public class Page {

    @TableField(exist = false)
    private int offset = 0;

    @TableField(exist = false)
    private int limit = 10;

    public Page() {
    }

    public Page(Object param) {
        BeanUtil.copyAsName(param,this,true);
    }

    public Page(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }
}
