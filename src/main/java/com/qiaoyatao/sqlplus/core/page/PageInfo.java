package com.qiaoyatao.sqlplus.core.page;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.List;

/**
 * sql分页工具
 * author： qiaoyatao
 * date: 2019年11月17日15:20:45
 */
@AllArgsConstructor
@Data
public class PageInfo<T> {

    private int pageNum;

    private int limit;

    private long total;

    private int pages;

    private List<T> list;

    public PageInfo() { }

    public PageInfo(List<T> list, long total, Page page) {
        this();
        calcPage(list, total, page);
    }

    private void calcPage(List<T> list, long total, Page page) {
        this.list = list;
        this.total = total;
        page=(page==null?new Page(0, 10):page);
        this.pageNum = page.getOffset() == 0 ? 1 : page.getOffset();
        this.limit = page.getLimit();
        this.pages = (int) (total % this.limit == 0 ? total / this.limit : total / this.limit + 1);
    }
}
