package com.qiaoyatao.sqlplus.core;

import com.qiaoyatao.sqlplus.core.format.Desc;
import com.qiaoyatao.sqlplus.core.format.ShowCreate;
import com.qiaoyatao.sqlplus.helper.SqlSession3;
import com.qiaoyatao.sqlplus.core.format.SqlFormat;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.core.page.*;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.system.valid.Validation;
import java.io.Serializable;
import java.util.*;

/**
 * 基类实例
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public class SqlPlus2 {

    //1、插入数据
    public static int insert(Object model) {
        return SqlUtil.toFormat(model, 0) == null ? 0 : SqlSession3.insert(SqlFormat.insert(model));
    }

    //1、插入数据or更新 根据id是否有值
    public static int insertOrUpdate(Object model) {
        return SqlUtil.getIdv(model).length() == 0 ? insert(model) : updateById(model);
    }

    public static int insert(Class model, Object param) {
        return SqlUtil.toFormat(BeanUtil.copyBean(param, model), 0) == null ? 0 : SqlSession3.insert(SqlFormat.insert(BeanUtil.copyBean(param, model)));
    }

    public static int insert(Class model, Map<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.insert(SqlFormat.insert(SqlUtil.newInstance(model), param));
    }

    //2、插入多条
    public static void insertList(List modes) {
        modes.forEach(model -> {
            SqlSession3.insert(SqlFormat.insert(model));
        });
    }

    //3、插入多条(类型可以不相同)
    public static void insertList(Object... modes) {
        Arrays.asList(modes).forEach(model -> {
            SqlSession3.insert(SqlFormat.insert(model));
        });
    }

    //4、id删除数据
    public static int deleteById(Class model, Serializable id) {
        return id == null || String.valueOf(id).length() == 0 ? 0 : SqlSession3.delete(SqlFormat.deleteById(id, SqlUtil.newInstance(model)));
    }

    //4、ids删除数据
    public static int deleteByIds(Class model, Serializable... ids) {
        return ids == null || ids[0].equals("") ? 0 : SqlSession3.delete(SqlFormat.deleteByIds(SqlUtil.newInstance(model), ids));
    }

    //5、id删除数据
    public static int deleteById(Object model) {
        return SqlUtil.getIdv(model).length() == 0 ? 0 : SqlSession3.delete(SqlFormat.deleteById(model));
    }

    //6、条件查询 返回具体vo参数ORDER_BY类
    public static <R> List<R> selectList(Object model, String... ORDER_BY) {
        return (List<R>) SqlSession3.selectList(SqlFormat.selectList(model, ORDER_BY), model.getClass());
    }

    //6、条件查询 返回具体vo参数ORDER_BY类
    public static <R> List<R> selectList(Object model, Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(model, ORDER_BY), resultVO);
    }

    //6、条件查询 返回具体vo参数ORDER_BY类
    public static <R> List<R> selectList(Class model, Object param, Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(SqlUtil.newInstance(model), param, ORDER_BY), resultVO);
    }

    //6、条件查询
    public static <R> List<R> selectList(Class<R> model, Object param, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(SqlUtil.newInstance(model), param, ORDER_BY), model);
    }

    //6、条件查询
    public static List<Map<String, Object>> selectByList(Object model, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(model, model, ORDER_BY));
    }

    //8、条件更新model数据，where更新条件
    public static int update(Object model, Object where) {
        return (SqlUtil.toFormat(where) == null || SqlUtil.toSetClass(model) == null) ? 0 : SqlSession3.update(SqlFormat.update(model, where));
    }

    //10、id (param更新数据)
    public static int updateById(Object model, Serializable id) {
        return (id == null || String.valueOf(id).length() == 0) || SqlUtil.toSetClass(model) == null ? 0 : SqlSession3.update(SqlFormat.updateById(id, model));
    }

    //10、id (param更新数据)
    public static int updateById(Object model) {
        return SqlUtil.toSetClass(model) == null ? 0 : SqlSession3.update(SqlFormat.updateById(model));
    }

    //11、id查询
    public static <T> T selectByid(Class<T> model, Serializable id) {
        return id == null || String.valueOf(id).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(id, SqlUtil.newInstance(model)), model);
    }

    //12、id查询返回具体VO
    public static <R> R selectByid(Class model, Serializable id, Class<R> resultVO) {
        return id == null || String.valueOf(id).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(id, SqlUtil.newInstance(model)), resultVO);
    }

    //14、id查询（通过实体类id属性）返回具体VO类
    public static <R> R selectByid(Object model, Class<R> resultVO) {
        return SqlUtil.getIdv(model).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(model), resultVO);
    }

    //13、id查询（返回map）
    public static Map<String, Object> selectMapByid(Class model, Serializable id) {
        return id == null || String.valueOf(id).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(id, SqlUtil.newInstance(model)));
    }

    //13、id查询（通过实体类id属性）
    public static Map<String, Object> selectMapByid(Object model) {
        return SqlUtil.getIdv(model).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(model));
    }

    //15、条件删除
    public static int delete(Class model, Object param) {
        return SqlUtil.toFormat(SqlUtil.newInstance(model)) == null ? 0 : SqlSession3.delete(SqlFormat.delete(SqlUtil.newInstance(model), param));
    }

    //15、条件删除
    public static int delete(Object model) {
        return SqlUtil.toFormat(model) == null ? 0 : SqlSession3.delete(SqlFormat.delete(model));
    }

    //16、统计
    public Long count(Object model) {
        return SqlSession3.count(SqlFormat.selectCount(model, model));
    }

    //16、统计
    public Long count(Class model, Map<String, Object> param) {
        return SqlSession3.count(SqlFormat.selectCount(SqlUtil.newInstance(model), param));
    }

    //16、统计
    public Long count(Class model, QueryModel queryModel) {
        return SqlSession3.count(SqlFormat.selectCount(SqlUtil.newInstance(model), queryModel));
    }

    public Map<String, Object> selectMapPage(Page page, Object model, String... ORDER_BY) {
        return SqlUtil.setPage(page, SqlSession3.selectList(SqlFormat.selectPage(model, model, page, ORDER_BY)),
                count(model));
    }

    public Map<String, Object> selectMapPage(Class model, Page page, QueryModel queryModel, String... ORDER_BY) {
        return SqlUtil.setPage(page, SqlSession3.selectList(SqlFormat.selectPage(SqlUtil.newInstance(model), page, queryModel, ORDER_BY)),
                count(queryModel));
    }

    //21、分页
    public Map<String, Object> selectMapPage(Class model, Page page, Map<String, Object> param, String... ORDER_BY) {
        return SqlUtil.setPage(page, SqlSession3.selectList(SqlFormat.selectPage(page, param, SqlUtil.newInstance(model), ORDER_BY)),
                count(param));
    }

    //18、分页
    public <T> PageInfo<T> selectPage(Class<T> model, Page page, QueryModel queryModel, String... ORDER_BY) {
        return new PageInfo<T>(SqlSession3.selectList(SqlFormat.selectPage(SqlUtil.newInstance(model), page, queryModel, ORDER_BY), model),
                count(model,queryModel), page);
    }

    //18、分页
    public PageInfo selectPage(Page page, Object model, String... ORDER_BY) {
        return new PageInfo<>(SqlSession3.selectList(SqlFormat.selectPage(model, model, page, ORDER_BY), model.getClass()),
                count(model), page);
    }

    //19、分页
    public <R> PageInfo<R> selectPage(Page page, Object model, Class<R> resultVO, String... ORDER_BY) {
        return new PageInfo<R>(SqlSession3.selectList(SqlFormat.selectPage(model, model, page, ORDER_BY), resultVO),
                count(model), page);
    }


    //21、分页
    public <R> PageInfo<R> selectPage(Class model, Page page, Map<String, Object> param, Class<R> resultVO, String... ORDER_BY) {
        return new PageInfo<R>(SqlSession3.selectList(SqlFormat.selectPage(page, param, SqlUtil.newInstance(model), ORDER_BY), resultVO),
                count(param), page);
    }

    //21、分页
    public <T> PageInfo<T> selectPage(Class<T> model, Page page, Map<String, Object> param, String... ORDER_BY) {
        return new PageInfo<T>(SqlSession3.selectList(SqlFormat.selectPage(page, param, SqlUtil.newInstance(model), ORDER_BY), model),
                count(param), page);
    }

    //22、QueryMode条件查询
    public <T> List<T>  selectList(Class<T> model, QueryModel queryModel, String... ORDER_BY) {
        return (List<T>) SqlSession3.selectList(SqlFormat.selectList(queryModel, SqlUtil.newInstance(model), ORDER_BY), model);
    }

    //23、QueryMode条件查询
    public <R> List<R> selectList(Class model, QueryModel queryModel, Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(queryModel, SqlUtil.newInstance(model), ORDER_BY), resultVO);
    }

    //24、QueryMode条件查询一条
    public <T> T selectOne(Class<T> model, QueryModel queryModel, String... ORDER_BY) {
        return (T) selectOne(queryModel, model, ORDER_BY);
    }

    //25、QueryMode条件查询一条
    public <R> R selectOne(Class model, QueryModel queryModel, Class<R> resultVO, String... ORDER_BY) {
        return queryModel.sql() == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(queryModel, SqlUtil.newInstance(model), ORDER_BY), resultVO);
    }

    //26、查询一条(根据param属性决定查询条件)
    public Map<String, Object> selectMapOne(Object model, String... ORDER_BY) {
        return SqlUtil.toFormat(model) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(model, model, ORDER_BY));
    }

    //26、查询一条(根据param属性决定查询条件) 返回vo
    public <R> R selectOne(Object model, Class<R> resultVO, String... ORDER_BY) {
        return SqlUtil.toFormat(model) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(model, model, ORDER_BY), resultVO);
    }

    //26、查询一条(根据param属性决定查询条件) 返回vo
    public <R> R selectOne(Object model, String... ORDER_BY) {
        return SqlUtil.toFormat(model) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(model, model, ORDER_BY), model.getClass());
    }

    //26、查询一条(根据param属性决定查询条件) 返回vo
    public <R> R selectOne(Class model, Object param, Class<R> resultVO, String... ORDER_BY) {
        return SqlUtil.toFormat(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(SqlUtil.newInstance(model), param, ORDER_BY), resultVO);
    }

    //26、查询一条(map查询)
    public <T> T  selectOne(Class<T> model, Map<String, Object> param, String... ORDER_BY) {
        return SqlUtil.toMap(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(SqlUtil.newInstance(model), param, ORDER_BY), model);
    }

    //26、查询一条(map查询)
    public <R> R selectOne(Class model, Map<String, Object> param, Class<R> resultVO, String... ORDER_BY) {
        return SqlUtil.toMap(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(SqlUtil.newInstance(model), param, ORDER_BY), resultVO);
    }

    //26、查询一条(map查询)
    public Map<String, Object> selectMapOne(Class model, Map<String, Object> param, String... ORDER_BY) {
        return SqlUtil.toMap(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(SqlUtil.newInstance(model), param, ORDER_BY));
    }

    //26、查询一条(map查询)
    public <T> List<T> selectList(Class<T> model, Map<String, Object> param, String... ORDER_BY) {
        return (List<T>) SqlSession3.selectList(SqlFormat.selectList(SqlUtil.newInstance(model), param, ORDER_BY), SqlUtil.newInstance(model).getClass());
    }

    //26、查询一条(map查询)
    public List<Map<String, Object>> selectMapList(Class model, Map<String, Object> param, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(SqlUtil.newInstance(model), param, ORDER_BY));
    }

    //26、查询多条(map查询)
    public List<Map<String, Object>> selectMapList(Object model, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(model, ORDER_BY));
    }

    //26、查询多条(map查询)
    public <R> List<R> selectList(Object model, Map<String, Object> param, Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(model, param, ORDER_BY), resultVO);
    }

    //28、map条件删除
    public static int delete(Class model, Map<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.delete(SqlFormat.delete(param, SqlUtil.newInstance(model)));
    }

    //28、queryModel条件删除
    public static int delete(Class model, QueryModel queryModel) {
        return queryModel.sql() == null ? 0 : SqlSession3.delete(SqlFormat.delete(queryModel, SqlUtil.newInstance(model)));
    }

    //29、queryModel条件修改
    public static int update(Object model, QueryModel queryModel) {
        return queryModel.sql() == null ? 0 : SqlSession3.update(SqlFormat.update(queryModel, model));
    }

    //29、Map条件修改
    public static int update(Object model, Map<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.update(SqlFormat.update(param, model));
    }

    //30、创建表
    public boolean create(Object model) {
        return SqlSession3.insert(SqlFormat.create(model))>0?true:false;
    }

    //清空数据并重置id
    public static int truncate(Object model) {
        return SqlSession3.delete(SqlFormat.truncate(model));
    }

    //清空数据并重置id
    public static int truncate(Class model) {
        return SqlSession3.delete(SqlFormat.truncate(SqlUtil.newInstance(model)));
    }

    //删除表
    public static int dropTable(Object model) {
        return SqlSession3.delete(SqlFormat.dropTable(model));
    }

    //删除表
    public static int dropTable(Class model) {
        return SqlSession3.delete(SqlFormat.dropTable(SqlUtil.newInstance(model)));
    }

    //查看表结构
    public static Desc desc(Object model) {
        return SqlSession3.selectOne(SqlFormat.desc(model), Desc.class);
    }

    //查看表结构
    public static Desc desc(Class model) {
        return SqlSession3.selectOne(SqlFormat.desc(SqlUtil.newInstance(model)), Desc.class);
    }

    //查看创建语句
    public static ShowCreate showCreate(Object model) {
        return SqlSession3.selectOne(SqlFormat.showCreate(model), ShowCreate.class);
    }

    //查看创建语句
    public static ShowCreate showCreate(Class model) {
        return SqlSession3.selectOne(SqlFormat.showCreate(SqlUtil.newInstance(model)), ShowCreate.class);
    }

    //list拷贝到target对象中
    public static List copyToList(List list, Object target) {
        return copyToList(list, target, false);
    }

    //list拷贝到target对象中 replace 是否替换原值（如果新值没有，原值不动）
    public static List copyToList(List list, Object target, boolean replace) {
        List result = new ArrayList<>();
        list.forEach(d -> {
            result.add(BeanUtil.copyAsNameParam(d, target, replace));
        });
        return result;
    }

    //model拷贝到target对象中
    public static void copyTo(Object model, Object target) {
        BeanUtil.copyAsName(model, target);
    }

    //model拷贝到target对象中
    public static void copyTo(Object model, Object target, boolean replace) {
        BeanUtil.copyAsName(model, target, replace);
    }

    //model拷贝到target对象中
    public static <T> T copyTo(Object model, Class<T> target) {
        return BeanUtil.copyBean(model, target);
    }

    //list拷贝到target对象中
    public static List copyToList(List list, Class target) {
        return copyToList(list, target, false);
    }

    //list拷贝到target对象中
    public static List copyToList(List list, Class target, boolean replace) {
        List result = new ArrayList<>();
        list.forEach(d -> {
            result.add(BeanUtil.copyBean(d, target, replace));
        });
        return result;
    }

    //获取json请求参数
    public static String toParam(Class param) {
        return BeanUtil.toParam(param);
    }

    //获取json请求参数
    public static String toParam(Object model) {
        return BeanUtil.toParam(model.getClass());
    }

    //param转map
    public static Map<String, Object> beanToMap(Object param) {
        return BeanUtil.beanToMap(param);
    }

    //对象转map，字段转下划线
    public static Map<String, Object> toSymbolCase(Object param) {
        return BeanUtil.toSymbolCase(param);
    }

    //map转model
    public static <T> T toBean(Map<String, Object> map, Class<T> resultVO) {
        return BeanUtil.toBean(map, resultVO);
    }

    //model转map(param有值不会替换)
    public static Object toBean(Map<String, Object> map, Object param) {
        return BeanUtil.toBean(map, param);
    }

    //map转对象
    public static List toBeanList(List<Map<String, Object>> list, Class resultVO) {
        List result = new ArrayList<>();
        list.forEach(d -> {
            result.add(BeanUtil.toBean(d, resultVO));
        });
        return result;
    }

    //map转对象
    public static List toBeanList(List<Map<String, Object>> list, Object param) {
        List result = new ArrayList<>();
        list.forEach(d -> {
            result.add(BeanUtil.toBean(d, param));
        });
        return result;
    }

    //数据校验
    public static void valid(Object param) {
        Validation.valid(param);
    }

    public static String valid(Object param, boolean isMsg) {
        return Validation.valid(isMsg, param);
    }

    public static void validShow(Object param, boolean show) {
        Validation.valid(param, show, false);
    }

    public static String valid(Object param, boolean show, boolean isMsg) {
        return Validation.valid(param, null, show, isMsg);
    }

    public static String valid(Object param, Class group, boolean isMsg) {
        return Validation.valid(param, group, true, isMsg);
    }

    public static void valid(Object param, Class group) {
        Validation.valid(param, group, true, false);
    }

    public static void valid(Object param, boolean show, Class group) {
        Validation.valid(param, group, show, false);
    }

    //对象校验(继承，子类都可以)
    public static String valid(Object param, Class group, boolean show, boolean isMsg) {
        return Validation.valid(param, group, show, isMsg);
    }

}
