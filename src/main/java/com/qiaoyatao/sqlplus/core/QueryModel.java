package com.qiaoyatao.sqlplus.core;

import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import java.lang.reflect.*;
import java.util.*;

/**
 * sql条件执行器
 * author： qiaoyatao
 * date: 2019年11月17日15:19:28
 */
public class QueryModel {

    private StringBuffer sql = new StringBuffer();

    //and并且
    public QueryModel eq(String column, Object params) {
        format(column, params, Symbol.AND, Symbol.EQ);
        return this;
    }

    //不等于
    public QueryModel notEq(String column, Object params) {
        format(column, params, Symbol.AND, Symbol.NOTEQ);
        return this;
    }

    //大于等于 >=
    public QueryModel ge(String column, Object params) {
        format(column, params, Symbol.AND, Symbol.ge);
        return this;
    }

    //小于等于 <=
    public QueryModel le(String column, Object params) {
        format(column, params, Symbol.AND, Symbol.le);
        return this;
    }

    //大于等于 or >=
    public QueryModel geOr(String column, Object params) {
        format(column, params, Symbol.OR, Symbol.ge);
        return this;
    }

    //小于等于 or <=
    public QueryModel leOR(String column, Object params) {
        format(column, params, Symbol.OR, Symbol.le);
        return this;
    }

    //all and并且
    public QueryModel allEq(Map<String, Object> params) {
        allMap(params, Symbol.AND);
        return this;
    }

    //or 或者
    public QueryModel or(String column, Object params) {
        format(column, params, Symbol.OR, Symbol.EQ);
        return this;
    }

    //不等于
    public QueryModel notOr(String column, Object params) {
        format(column, params, Symbol.OR, Symbol.NOTEQ);
        return this;
    }

    //all or 或者
    public QueryModel allOr(Map<String, Object> params) {
        allMap(params, Symbol.OR);
        return this;
    }

    //and like 并且模块查询
    public QueryModel likeEq(String column, Object params) {
        formatLike(column, params, Symbol.AND);
        return this;
    }

    //or like 并且模块查询
    public QueryModel likeOr(String column, Object params) {
        formatLike(column, params, Symbol.OR);
        return this;
    }

    //and gt大于
    public QueryModel gtAnd(String column, Object params) {
        format(column, params, Symbol.AND, Symbol.GT);
        return this;
    }

    //or gt大于
    public QueryModel gtOr(String column, Object params) {
        format(column, params, Symbol.OR, Symbol.GT);
        return this;
    }

    //and lt小于
    public QueryModel ltAnd(String column, Object params) {
        format(column, params, Symbol.AND, Symbol.LT);
        return this;
    }

    //or lt小于
    public QueryModel ltOr(String column, Object params) {
        format(column, params, Symbol.OR, Symbol.LT);
        return this;
    }

    //between and
    public QueryModel between(String column, Object val1, Object val2) {
        sqlbetween(column, val1, val2, false);
        return this;
    }

    //notBetween and
    public QueryModel notBetween(String column, Object val1, Object val2) {
        sqlbetween(column, val1, val2, true);
        return this;
    }

    //column in (1,2,3)
    public QueryModel in(String column, Object[] vals) {
        sqlin(column, vals, false);
        return this;
    }

    //column in (1,2,3)
    public QueryModel inValue(String column, String inValue) {
        sqlin(column, inValue.split(","), false);
        return this;
    }

    //column in (1,2,3)
    public QueryModel notInValue(String column, String inValue) {
        sqlin(column, inValue.split(","), true);
        return this;
    }

    //column not in (1,2,3)
    public QueryModel notIn(String column, Object[] vals) {
        sqlin(column, vals, true);
        return this;
    }

    /**
     * @param column    键
     * @param vals      数组
     * @param condition true notIn
     */
    private void sqlin(String column, Object[] vals, boolean condition) {
        if (vals == null || vals.length == 0) {
            return;
        }
        sql.append(Symbol.AND + column);
        if (condition) {
            sql.append(Symbol.NOT);
        }
        StringBuffer sb = new StringBuffer();
        for (Object val : vals) {
            if (val instanceof String) {
                sb.append(single(val) + ",");
            } else if (val instanceof Date) {
                sb.append(single(StrUtil.formatDate(val)) + ",");
            } else {
                sb.append(val + ",");
            }
        }
        sql.append(StrUtil.append(Symbol.IN ,SqlUtil.substr(sb) , Symbol.RIGHTB));
    }

    /**
     * @param column    键
     * @param val       值1
     * @param val2      值2
     * @param condition true notBetween
     */
    private void sqlbetween(String column, Object val, Object val2, boolean condition) {
        if (StrUtil.isNull(val)|| StrUtil.isNull(val2)) {
            return;
        }
        sql.append(Symbol.AND + column);
        if (condition) {
            sql.append(Symbol.NOT);
        }
        sql.append(Symbol.BETWEEN);
        if (val instanceof String) {
            sql.append(StrUtil.append(single(val), Symbol.AND , single(val2)));
            return;
        }
        if (val instanceof Date) {
            sql.append(StrUtil.append(single(StrUtil.formatDate(val)) , Symbol.AND,
                    single(StrUtil.formatDate(val2))));
            return;
        }
        sql.append(StrUtil.append(val, Symbol.AND , val2));
    }

    //map基础类型
    private void allMap(Map<String, Object> params, String type) {
        if (params.isEmpty()) {
            return;
        }
        for (String key : params.keySet()) {
            Object value = params.get(key);
            if (!StrUtil.isNull(value)) {
                format(key, value, type, Symbol.EQ);
            }
        }
    }

    /**
     * 格式化
     *
     * @param column 字段
     * @param params 值
     * @param type   and 或者or and name='乔小乔'
     */
    private void format(String column, Object params, String type, String key) {
        if (StrUtil.isNull(params)) {
            return;
        }
        sql.append(StrUtil.append(type,column , key));
        if (params instanceof String) {
            sql.append(single(params));
            return;
        }
        if (params instanceof Date) {
            sql.append(single(StrUtil.formatDate(params)));
            return;
        }
        sql.append(params);
    }

    /**
     * @param column 键
     * @param params 值
     * @param type   and 或者or
     */
    private void formatLike(String column, Object params, String type) {
        if (StrUtil.isNull(params)) {
            return;
        }
        sql.append(type + column);
        if (params instanceof String) {
            sql.append(StrUtil.append(Symbol.LIKE ,"'%",params.toString(),"%'"));
            return;
        }
        if (params instanceof Date) {
            sql.append(StrUtil.append(Symbol.LIKE , "'%" , params.toString() , "%'"));
            return;
        }
        sql.append(Symbol.EQ + params);
    }

    //获取sql,去掉第一个and获取or
    public String sql() {
        if (sql.toString().length() == 0) {
            return null;
        }
        String psql = String.valueOf(sql).toLowerCase();
        if (psql.startsWith(Symbol.AND.trim()) || psql.startsWith(Symbol.AND)) {
            return psql.replaceFirst(Symbol.AND.trim(), "");
        }
        if (psql.startsWith(Symbol.OR.trim()) || psql.startsWith(Symbol.OR)) {
            return psql.replaceFirst(Symbol.OR.trim(), "");
        }
        return psql;
    }

    //包装单引号 'name'
    private String single(Object value) {
        return String.format(Symbol.TO,value);
    }

    //片段sql 例：id =# AND password =# and is_del=#
    public QueryModel snippet(String sqls, Object... params) {
        if (StrUtil.isBlank(sqls)) {return this;}
        StringBuffer sb = new StringBuffer();
        String[] split = sqls.split(Symbol.ALARM);
        int a = 0;
        for (int i = 0; i < params.length; i++) {
            if (a == split.length) {
                break;
            }
            Object param = Array.get(params, i);
            if (param instanceof String) {
                sb.append(Array.get(split, i) + single(param));
                a++;
                continue;
            }
            if (param instanceof Date) {
                sb.append(Array.get(split, i) + single(StrUtil.formatDate(param)));
                a++;
                continue;
            }
            sb.append(Array.get(split, i) + param.toString());
            a++;
        }
        sql.delete(0, sql.length());//清空
        sql.append(sb.toString());
        String end = Array.get(split, (split.length - 1)).toString();
        if (end.length() != 0 && (!sb.toString().contains(end) || sb.toString().toUpperCase().contains(Symbol.INTO))) {
            sql.append(end);
        }
        return this;
    }

    //片段sql 例：id =# AND password =# and is_del=#
    public QueryModel snippet(String sqls, Object params) {
        if (StrUtil.isBlank(sqls)) {return this;}
        StringBuilder sb = new StringBuilder();
        String[] split = sqls.split(Symbol.ALARM);
        for (String str : split) {
            for (Field field : SqlUtil.superField(params.getClass())) {
                field.setAccessible(true);
                if (str.contains(StrUtil.toSymbolCase(field.getName()))) {
                    Object value = SqlUtil.get(field, params);
                    if (StrUtil.isNull(value)) {
                        break;
                    }
                    if (value instanceof String) {
                        sb.append(str + single(value));
                        break;
                    }
                    if (value instanceof Date) {
                        sb.append(str + single(StrUtil.formatDate(value)));
                        break;
                    }
                    sb.append(str + value.toString());
                    break;
                }
            }
        }
        sql.delete(0, sql.length());//清空
        sql.append(sb.toString());
        String end = Array.get(split, (split.length - 1)).toString();
        if (end.length() != 0 && !sb.toString().contains(end)) {
            sql.append(end);
        }
        return this;
    }

    //全量sql
    public QueryModel full(String sql, Object... params) {
        return snippet(sql, params);
    }

    //全量sql
    public QueryModel full(String sql, Object params) {
        return snippet(sql, params);
    }

}
