package com.qiaoyatao.sqlplus.core.format;
import com.qiaoyatao.sqlplus.constants.Symbol;

/**
 * sql排序
 * author： qiaoyatao
 * date: 2019年12月21日18:17:53
 */
public class ORDER_BY {

    public static String asc(String field) {
        return field+" ";
    }

    public static String desc(String field) {
        return field+Symbol.DESC;
    }
}
