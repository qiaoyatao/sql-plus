package com.qiaoyatao.sqlplus.core.format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 统一返回
 * author： qiaoyatao
 * date: 2020年8月2日20:29:21
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class R<T>{

	//true成功，false失败
	private boolean result;

	//编码
	private Integer code;

	//响应信息
	private String msg;

	//数据
	private T data;

	public static R ok() {
		return new R(true,200,"成功！",null);
	}

	public static R ok(Object data) {
		return new R(true,200,"成功！",data);
	}

	public static R ok(String msg) {
		return new R(true,200,msg,null);
	}

	public static R ok(String msg,Object data) {
		return new R(true,200,msg,data);
	}

	public static R error(String msg) {
		return new R(false,500,msg,null);
	}

	public static R error(Integer code,String msg) {
		return new R(false,code,msg,null);
	}
}
