package com.qiaoyatao.sqlplus.core.format;

import com.qiaoyatao.sqlplus.annotation.model.*;
import com.qiaoyatao.sqlplus.constants.*;
import com.qiaoyatao.sqlplus.enums.IdType;
import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import org.apache.commons.lang3.StringUtils;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/**
 * format 语句生成类
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public class SqlFormat {

    public static String insert(Object model) {
        return SqlUtil.insert(model, SqlUtil.toFormat(model, 0));
    }

    public static String insert(Object model, Map<String, Object> param) {
        return SqlUtil.insert(model, SqlUtil.toMap(param));
    }

    public static String selectByid(Serializable id, Object model) {
        return SqlUtil.selectByid(model, id);
    }

    public static String deleteById(Serializable id, Object model) {
        return SqlUtil.deleteById(model, id);
    }

    public static String deleteByIds(Object model, Serializable... ids) {
        return String.format(SqlMethod.deleteByIds, SqlUtil.tableName(model),
                SqlUtil.getId(model), StringUtils.join(ids, ","));
    }

    public static String delete(Object model) {
        return delete(model, model);
    }

    public static String delete(Object model, Object param) {
        return SqlUtil.delete(model, SqlUtil.toFormat(param), true);
    }

    public static String delete(Map<String, Object> param, Object model) {
        return SqlUtil.delete(model, SqlUtil.toMap(param), true);
    }

    public static String delete(LinkedHashMap<String, Object> param, Object model) {
        return SqlUtil.delete(model, SqlUtil.toMap(param), false);
    }

    public static String selectList(Object model, Object param, String... ORDER_BY) {
        List<String> list = SqlUtil.toFormat(param);
        return String.format(SqlMethod.select, SqlUtil.toFormatKey(model), SqlUtil.tableName(model),
                list == null ? "" : String.format(SqlMethod.where, SqlUtil.where(list))
                , SqlUtil.ORDER_BY(model, ORDER_BY));
    }

    public static String selectList(Object model, String... ORDER_BY) {
        return selectList(model, model, ORDER_BY);
    }

    public static String update(Object model, Object param) {
        return SqlUtil.update(model, SqlUtil.toFormat(param), true);
    }

    public static String update(LinkedHashMap<String, Object> param, Object model) {
        return SqlUtil.update(model, SqlUtil.toMap(param), false);
    }

    public static String update(Map<String, Object> param, Object model) {
        return SqlUtil.update(model, SqlUtil.toMap(param), false);
    }

    public static String updateById(Serializable id, Object model) {
        return String.format(SqlMethod.updateById, SqlUtil.tableName(model), SqlUtil.toSetClass(model),
                SqlUtil.getId(model), id);
    }

    public static String updateById(Object model) {
        return updateById(SqlUtil.getIdv(model), model);
    }

    public static String deleteById(Object model) {
        return deleteById(SqlUtil.getIdv(model), model);
    }

    public static String selectByid(Object model) {
        return selectByid(SqlUtil.getIdv(model), model);
    }

    public static String selectCount(Object model, Object param) {
        return SqlUtil.count(model, SqlUtil.toFormat(param), true);
    }

    public static String selectCount(Object model, Map<String, Object> param) {
        return SqlUtil.count(model, SqlUtil.toMap(param), false);
    }

    public static String selectCount(Object model, QueryModel queryModel) {
        String sql = queryModel.sql();
        return SqlUtil.full(sql, Symbol.SELECT) ? sql : String.format(SqlMethod.count, SqlUtil.tableName(model),
                queryModel.sql() == null ? "" : String.format(SqlMethod.where, queryModel.sql()));
    }

    public static String selectPage(Page page, Map<String, Object> param, Object model, String... ORDER_BY) {
        return SqlUtil.page(page, model, SqlUtil.toMap(param), SqlUtil.where(SqlUtil.toMap(param)), ORDER_BY);
    }

    public static String selectPage(Object model, Object param, Page page, String... ORDER_BY) {
        return SqlUtil.page(page, model, SqlUtil.toFormat(param), SqlUtil.where(SqlUtil.toFormat(param)), ORDER_BY);
    }

    public static String selectPage(Object model, Page page, QueryModel queryModel, String... ORDER_BY) {
        Page newPage = SqlUtil.calcPage(page);
        return String.format(SqlMethod.page, SqlUtil.toFormatKey(model), SqlUtil.tableName(model),
                queryModel.sql() == null ? "" : String.format(SqlMethod.where, queryModel.sql()),
                SqlUtil.ORDER_BY(model, ORDER_BY),
                newPage.getLimit(),newPage.getOffset());
    }

    public static String selectList(QueryModel queryModel, Object model, String... ORDER_BY) {
        return SqlUtil.select(SqlMethod.select,queryModel,model,ORDER_BY);
    }

    public static String selectOne(QueryModel queryModel, Object model, String... ORDER_BY) {
        return SqlUtil.select(SqlMethod.selectOne,queryModel,model,ORDER_BY);
    }

    public static String delete(QueryModel queryModel, Object model) {
        String sql = queryModel.sql();
        SqlUtil.isBlank(sql);
        return SqlUtil.full(sql, Symbol.DELETE) ? sql :
                String.format(SqlMethod.delete, SqlUtil.tableName(model),sql);
    }

    public static String update(QueryModel queryModel, Object model) {
        String sql = queryModel.sql();
        SqlUtil.isBlank(sql);
        return SqlUtil.full(sql, Symbol.UPDATE) ? sql :
                String.format(SqlMethod.update, SqlUtil.tableName(model),SqlUtil.toSetClass(model),sql);
    }

    public static String create(Object model) {
        Class<? extends Object> clzzz = model.getClass();
        StringBuffer sb = new StringBuffer();
        sb.append(Symbol.CREATE);
        String comments = null;
        TableName table = clzzz.getAnnotation(TableName.class);
        if (table != null) {
            sb.append(table.value() + Symbol.LEFTB);
            comments = table.comment();
        } else {
            sb.append(StrUtil.toSymbolCase(clzzz.getSimpleName()) + Symbol.LEFTB);
        }
        boolean auto = false;
        String idVal = null;
        for (Field field : SqlUtil.superField(clzzz)) {
            field.setAccessible(true);
            if (field.getName().equals(Symbol.SERIALIZABLE)){
                continue;
            }
            TableField tableField = field.getAnnotation(TableField.class);
            ID idField = field.getAnnotation(ID.class);
            String comment = null;
            //数据库忽略字段
            if (tableField != null && tableField.exist() == false) {
                continue;
            }
            if (tableField != null) {
                if (tableField.value().length() != 0) {
                    sb.append(tableField.value());
                } else {
                    sb.append(StrUtil.toSymbolCase(field.getName()));
                }
                comment = tableField.comment();//备注
            } else if (idField != null) {
                if (idField.value().length() != 0) {
                    idVal = idField.value();
                    sb.append(idVal);
                } else {
                    idVal = StrUtil.toSymbolCase(field.getName());
                    sb.append(idVal);
                }
                comment = idField.comment();//备注
            } else {
                sb.append(StrUtil.toSymbolCase(field.getName()));
            }
            Class<?> type = field.getType();
            if (type == String.class) {
                sb.append(Symbol.VARCHAR);
            }
            if (type == Integer.class || type == int.class) {
                sb.append(Symbol.INT);
            }
            if (type == Long.class || type == long.class) {
                sb.append(Symbol.BIGINT);
            }
            if (type == Float.class || type == float.class) {
                sb.append(Symbol.FLOAT);
            }
            if (type == Double.class || type == double.class) {
                sb.append(Symbol.DOUBLE);
            }
            if (type == Boolean.class || type == boolean.class) {
                sb.append(Symbol.BIT);
            }
            if (type == BigDecimal.class) {
                sb.append(Symbol.DECIMAL);
            }
            if (type == char.class) {
                sb.append(Symbol.CHAR);
            }
            if (type == Short.class || type == short.class) {
                sb.append(Symbol.SMALLINT);
            }
            if (type == Byte.class || type == byte.class) {
                sb.append(Symbol.SMALLINT);
            }
            if (type == Date.class) {
                sb.append(Symbol.DATETIME);
            }
            if (idField != null && idField.type().getKey() == IdType.AUTO.getKey()) {
                sb.append(Symbol.AUTO_INCREMENT);
                auto = true;
            }
            sb.append(SqlUtil.setComment(comment));
        }
        if (auto) {
            sb.append(String.format(Symbol.PRIMARY_KEY,idVal));
        }
        String sql = SqlUtil.substr(sb) + Symbol.ENGINE;
        if (comments != null && comments.length() != 0) {
            return StrUtil.append(sql, Symbol.COMMENT, Symbol.EQ , Symbol.leftA , comments , Symbol.leftA , Symbol.FH);
        }
        return sql + Symbol.FH;
    }

    public static String showTable(Object model) {
        return String.format(Symbol.SHOW, SqlUtil.tableName(model));
    }

    public static String fullList(QueryModel queryModel, Object model) {
        String sql = queryModel.sql();
        if (sql.contains("*")) {
            return sql.replaceFirst("\\*", SqlUtil.toFormatKey(model));
        }
        return sql;
    }

    public static String selectOne(Object model, String... ORDER_BY) {
        return selectOne(model, model, ORDER_BY);
    }

    public static String selectOne(Object model, Object param, String... ORDER_BY) {
        return SqlUtil.selone(model,SqlUtil.toFormat(param),true,ORDER_BY);
    }

    public static String selectOne(Object model, Map<String, Object> param, String... ORDER_BY) {
        return SqlUtil.selone(model,SqlUtil.toMap(param),true,ORDER_BY);
    }

    public static String selectList(Object model, Map<String, Object> param, String... ORDER_BY) {
        return SqlUtil.selList(model,SqlUtil.toMap(param),false,ORDER_BY);
    }

    public static String selectList(Object model, LinkedHashMap<String, Object> param, String... ORDER_BY) {
      return SqlUtil.selList(model,SqlUtil.toMap(param),false,ORDER_BY);
    }

    public static String selectOne(Object model, LinkedHashMap<String, Object> param, String... ORDER_BY) {
        return SqlUtil.selone(model,SqlUtil.toMap(param),false,ORDER_BY);
    }

    public static String truncate(Object model) {
        return String.format(Symbol.TRUNCATE, SqlUtil.tableName(model));
    }

    public static String desc(Object model) {
        return Symbol.DESC + SqlUtil.tableName(model);
    }

    public static String showCreate(Object model) {
        return String.format(Symbol.SHOW_CREATE, SqlUtil.tableName(model));
    }

    public static String dropTable(Object model) {
        return String.format(Symbol.DROP_TABLE, SqlUtil.tableName(model));
    }
}
