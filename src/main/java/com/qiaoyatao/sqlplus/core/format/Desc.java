package com.qiaoyatao.sqlplus.core.format;

import lombok.Data;

/**
 * desc
 * author： qiaoyatao
 * date: 2019年12月5日18:37:21
 */
@Data
public class Desc {

    private String field;

    private String type;

    private String Null;

    private String key;

    private String Default;

    private String extra;
}
