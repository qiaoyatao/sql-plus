package com.qiaoyatao.sqlplus.core.format;

import lombok.Data;

/**
 * 当前表创建语句
 * author： qiaoyatao
 * date: 2019年12月5日18:37:21
 */
@Data
public class ShowCreate {

    // 表名
    private String table;

    //创建表sql
    private String createTable;

}
