package com.qiaoyatao.sqlplus.core.format;

import com.qiaoyatao.sqlplus.annotation.bean.*;
import com.qiaoyatao.sqlplus.annotation.model.*;
import com.qiaoyatao.sqlplus.constants.*;
import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.gen.builder.SpUtil;
import com.qiaoyatao.sqlplus.core.Model;
import com.qiaoyatao.sqlplus.helper.SqlSession3;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.core.page.*;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import java.io.Serializable;
import java.lang.reflect.*;
import java.util.*;

/**
 * sql执行器工具
 * author： qiaoyatao
 * date: 2019年11月17日15:21:21
 */
@Slf4j
public class SqlUtil {

    //key(id,name,password) value(1,2,3)
    public static List<String> toFormat(Object model, Integer... type) {
        if (model == null) {
            return null;
        }
        StringBuffer fields = new StringBuffer();
        StringBuffer values = new StringBuffer();
        StringBuffer likes = new StringBuffer();
        Arrays.asList(superField(model.getClass()))
        .forEach(field -> {
            field.setAccessible(true);
            if (field.getName().contains(Symbol.SERIALIZABLE)) {
                return;
            }
            Object value = get(field, model);
            if (StrUtil.isNull(value)) {
                return;
            }
            TableField tableField = field.getAnnotation(TableField.class);
            ID idField = field.getAnnotation(ID.class);
            //数据库忽略字段
            if (tableField != null && tableField.exist() == false) {
                return;
            }
            if (tableField != null && tableField.like()) {//是否为like
                likes.append(tableField.value().length() != 0 ? tableField.value() :
                        StrUtil.toSymbolCase(field.getName()) + ",");
            }
            if (tableField != null && tableField.value().length() != 0) {
                fields.append(tableField.value());
                fields.append(Symbol.COMMA);
            } else if (idField != null && idField.value().length() != 0) {
                //add并且主键自增长,且内容为空
                if (type.length != 0 && value == null) {
                    return;
                }
                fields.append(idField.value());
                fields.append(Symbol.COMMA);
            } else {
                fields.append(symbolCasekey(field.getName()));
            }
            if (field.getType() == String.class) {
                values.append(single(value.toString()));
            } else if (field.getType().equals(Date.class)) {
                values.append(single(StrUtil.formatDate(value)));
            } else {
                values.append(singleVal(value.toString()));
            }
        });
        if (fields.toString().length() == 0 || values.toString().length() == 0) {
            return null;
        }
        return Arrays.asList(substr(fields), substr(values), likes.toString());
    }

    public static List<String> toMap(Map<String, Object> param) {
        if (param.isEmpty()) {
            return null;
        }
        StringBuffer fields = new StringBuffer();
        StringBuffer values = new StringBuffer();
        param.keySet().forEach(key -> {
            Object value = param.get(key);
            if (StrUtil.isNull(value)) {
                return;
            }
            fields.append(StrUtil.toSymbolCase(key));
            fields.append(Symbol.COMMA);
            if (value.getClass().getTypeName().equals(Date.class.getName())) {
                values.append(single(StrUtil.formatDate(value)));
                return;
            }
            values.append(single(value.toString()));
        });
        if (fields.toString().length() == 0 || values.toString().length() == 0) {
            return null;
        }
        return Arrays.asList(substr(fields), substr(values));
    }

    //(id=1,name=乔小乔,password=admin)
    public static String toSetClass(Object model) {
        if (model == null) {
            return null;
        }
        StringBuffer fields = new StringBuffer();
        Arrays.asList(superField(model.getClass()))
        .forEach(field -> {
            field.setAccessible(true);
            if (field.getName().contains(Symbol.SERIALIZABLE)) {
                return;
            }
            Object value = get(field, model);
            if (StrUtil.isNull(value)) {
                return;
            }
            TableField tableField = field.getAnnotation(TableField.class);
            ID idField = field.getAnnotation(ID.class);
            if (tableField != null) {
                //数据库忽略字段
                if (tableField.exist() == false) {
                    return;
                }
                if (tableField.value().length() != 0) {
                    fields.append(tableField.value());
                    fields.append(Symbol.EQ);
                } else {
                    fields.append(symbolSetkey(field.getName()));
                }
            } else if (idField != null) {
                //不更新主键
                return;
                //set.append(idField.value() + Symbol.EQ);
            } else {
                fields.append(symbolSetkey(field.getName()));
            }
            if (field.getType() == String.class) {
                fields.append(single(value.toString()));
            } else if (field.getType().equals(Date.class)) {
                fields.append(single(StrUtil.formatDate(value)));
            } else {
                fields.append(singleVal(value.toString()));
            }
        });
        return substr(fields);
    }

    public static String anno(Object model, boolean del) {
        if (model == null) {
            return null;
        }
        for (Field field : superField(model.getClass())) {
            field.setAccessible(true);
            if (del) {
                Delete delete = field.getAnnotation(Delete.class);
                if (delete == null) {
                    continue;
                }
            } else {
                Status status = field.getAnnotation(Status.class);
                if (status == null) {
                    continue;
                }
            }
            return isDel(field, model);
        }
        return null;
    }

    private static String isDel(Field field, Object model) {
        String name = field.getName();
        TableField asField = field.getAnnotation(TableField.class);
        if (asField != null && asField.value().length() != 0) {
            //数据库忽略字段
            if (asField.exist() == false) {
                return null;
            }
            name = asField.value();
        }
        return symbolSetkey(name) + (new Integer(0).equals(get(field, model)) ? 1 : 0);
    }

    //获取key（查询from列） 例(id,name,password)
    public static String toFormatKey(Object model) {
        if (model == null) {
            return null;
        }
        StringBuffer fields = new StringBuffer();
        Arrays.asList(superField(model.getClass()))
        .forEach(field -> {
            field.setAccessible(true);
            if (field.getName().equals(Symbol.SERIALIZABLE)) {
                return;
            }
            TableField tableField = field.getAnnotation(TableField.class);
            ID idField = field.getAnnotation(ID.class);
            //数据库忽略字段
            if (tableField != null && tableField.exist() == false) {
                return;
            }
            if (tableField != null && tableField.value().length() != 0) {
                fields.append(isAsName(tableField.value(), field.getName()));
            } else if (idField != null && idField.value().length() != 0) {
                fields.append(isAsName(idField.value(), field.getName()));
            } else {
                fields.append(isAsName(StrUtil.toSymbolCase(field.getName()), field.getName()));
            }
        });
        return substr(fields);
    }

    private static String isAsName(String value, String value2) {
        return value.equals(value2) ? value + Symbol.COMMA : String.format(Symbol.ASNAME, value, value2);
    }

    //例如:user_name,
    public static String symbolCasekey(CharSequence value) {
        return StrUtil.toSymbolCase(value) + Symbol.COMMA;
    }

    //例如:user_name=
    public static String symbolSetkey(CharSequence value) {
        return StrUtil.toSymbolCase(value) + Symbol.EQ;
    }

    //例:乔小乔,
    public static String singleVal(CharSequence value) {
        return value + Symbol.COMMA;
    }

    public static String getId(Object model) {
        for (Field field : superField(model.getClass())) {
            field.setAccessible(true);
            ID id = field.getAnnotation(ID.class);
            if (id == null) {
                continue;
            }
            if (StrUtil.isNotBlank(id.value())) {
                return id.value();
            }
            return StrUtil.toSymbolCase(field.getName());
        }
        return Symbol.ID;
    }

    public static String getIdv(Object model) {
        if (model == null) {
            return "";
        }
        for (Field field : superField(model.getClass())) {
            field.setAccessible(true);
            Object value = get(field, model);
            if (field.getAnnotation(ID.class) != null) {
                if (value != null && value.toString().length() != 0) {
                    return value.toString();
                }
            }
            //不加id注解，默认拿字段为id的值
            if (value != null && field.getName().equals(Symbol.ID.trim())) {
                return value.toString();
            }
        }
        return "";
    }

    public static String setId(Serializable id) {
        return String.format(Symbol.TO, id);
    }

    public static String single(CharSequence key) {
        return String.format(Symbol.TO2, key);
    }

    //设置备注名称
    public static String setComment(String comment) {
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isNotBlank(comment)) {
            sb.append(StrUtil.append(Symbol.COMMENT, Symbol.leftA, comment, Symbol.leftA));
        }
        return sb.append(Symbol.COMMA).toString();
    }

    public static String where(List<String> list) {
        if (list == null) {
            return null;
        }
        StringBuffer sql = new StringBuffer();
        int index = 0;
        for (String key : list.get(0).split(Symbol.COMMA)) {
            sql.append(Symbol.AND);
            sql.append(key);
            String value = Array.get(list.get(1).split(Symbol.COMMA), index).toString();
            if (!search(list, key)) {
                sql.append(Symbol.EQ);
                sql.append(value);
            } else {
                sql.append(StrUtil.append(Symbol.LIKE, "'%", value.substring(1, value.length() - 1), "%'"));
            }
            index++;
        }
        return sql.toString().replaceFirst(Symbol.AND, "");
    }

    public static String mapWhere(List<String> list) {
        StringBuffer sql = new StringBuffer();
        int index = 0;
        for (String key : list.get(0).split(Symbol.COMMA)) {
            sql.append(StrUtil.append(Symbol.AND, key, Symbol.EQ,
                    Array.get(list.get(1).split(Symbol.COMMA), index).toString()));
            index++;
        }
        return sql.toString().replaceFirst(Symbol.AND, "");
    }

    private static boolean search(List<String> list, String search) {
        if (list.get(2).length() == 0) {
            return false;
        }
        return SpUtil.search(list.get(2).split(","), search);
    }

    public static Field[] superField(Class model) {
        Field[] fields = model.getDeclaredFields();
        Class superclass = model.getSuperclass();
        if (!superclass.equals(Object.class) && !superclass.equals(Model.class)) {
            Field[] superFields = superclass.getDeclaredFields();
            Field[] newField = new Field[fields.length + superFields.length];
            System.arraycopy(fields, 0, newField, 0, fields.length);
            System.arraycopy(superFields, 0, newField, fields.length, superFields.length);
            return newField;
        }
        return fields;
    }

    public static Object get(Field field, Object model) {
        try {
            field.setAccessible(true);
            return field.get(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void set(Field field, Object model, Object value) {
        try {
            field.set(model, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String substr(Object str) {
        return str.toString().length() == 0 ? null : str.toString().substring(0, str.toString().length() - 1);
    }

    public static boolean full(String sql, String type) {
        if (StrUtil.isNotBlank(sql) && (sql.toUpperCase().contains(type)
                || sql.toUpperCase().contains(Symbol.INTO))) {
            return true;
        }
        return false;
    }

    public static String ORDER_BY(Object model, String... ORDER_BY) {
        if (ORDER_BY == null || ORDER_BY.length == 0) {
            return SqlUtil.getId(model) + Symbol.DESC;
        }
        return Array.get(ORDER_BY, 0).toString();
    }

    public static Object newInstance(Class model) {
        try {
            return model.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String tableName(Object model) {
        Class<? extends Object> param = model.getClass();
        TableName tableName = param.getAnnotation(TableName.class);
        if (tableName == null || tableName.value().length() == 0) {
            return StrUtil.toSymbolCase(param.getSimpleName());
        }
        return tableName.value();
    }

    public static Page calcPage(Page page) {
        page = (page == null ? new Page(0, 10) : page);
        Page newPage = new Page();
        newPage.setOffset(page.getOffset() != 0 ? page.getOffset() * page.getOffset() - page.getOffset() : 0);
        newPage.setLimit(page.getLimit() == 0 ? 10 : page.getLimit());
        return newPage;
    }

    public static PageInfo queryPage(List list, long count, Page page) {
        return new PageInfo<>(list, count, page);
    }

    public static Map<String, Object> setPage(Page page, List<Map<String, Object>> list, Long count) {
        return BeanUtil.beanToMap(new PageInfo(list, count, page));
    }

    public static void isNull(List<String> list) {
        if (list == null) {
            throw new RuntimeException(SqlSymbol.UPDATEMSG);
        }
    }

    public static void isBlank(String str) {
        if (StrUtil.isBlank(str)) {
            throw new RuntimeException(SqlSymbol.UPDATEMSG);
        }
    }

    public static void isNotNull(String validate) {
        if (validate != null) {
            throw new RuntimeException(validate);
        }
    }

    public static boolean is(String sql) {
        return sql == null ? false : (SqlSession3.delete(sql) > 0 ? true : false);
    }

    public static String insert(Object model, List<String> list) {
        return String.format(SqlMethod.insert, SqlUtil.tableName(model), list.get(0), list.get(1));
    }

    public static String selectByid(Object model, Serializable id) {
        return String.format(SqlMethod.selectbyid, SqlUtil.toFormatKey(model), SqlUtil.tableName(model), SqlUtil.getId(model), id);
    }

    public static String deleteById(Object model, Serializable id) {
        return String.format(SqlMethod.deleteById, SqlUtil.tableName(model), SqlUtil.getId(model), id);
    }

    public static String delete(Object model, List<String> list, boolean param) {
        SqlUtil.isNull(list);
        return String.format(SqlMethod.delete, SqlUtil.tableName(model), param ? SqlUtil.where(list) : SqlUtil.mapWhere(list));
    }

    public static String update(Object model, List<String> list, boolean param) {
        SqlUtil.isNull(list);
        return String.format(SqlMethod.update, SqlUtil.tableName(model), SqlUtil.toSetClass(model),
                param ? SqlUtil.where(list) : SqlUtil.mapWhere(list));
    }

    public static String count(Object model, List<String> list, boolean param) {
        return String.format(SqlMethod.count, SqlUtil.tableName(model),
                list == null ? "" : String.format(SqlMethod.where, param ? SqlUtil.where(list) : SqlUtil.mapWhere(list)));
    }

    public static String page(Page page, Object model, List<String> list, String where, String... ORDER_BY) {
        Page newPage = SqlUtil.calcPage(page);
        return String.format(SqlMethod.page, SqlUtil.toFormatKey(model), SqlUtil.tableName(model),
                (list == null ? "" : String.format(SqlMethod.where, where))
                , SqlUtil.ORDER_BY(model, ORDER_BY),
                newPage.getLimit(), newPage.getOffset());
    }

    public static String select(String method, QueryModel queryModel, Object model, String... ORDER_BY) {
        String sql = queryModel.sql();
        return SqlUtil.full(sql, Symbol.SELECT) ? sql :
                String.format(method, SqlUtil.toFormatKey(model), SqlUtil.tableName(model),
                        StrUtil.isBlank(sql) ? "" : String.format(SqlMethod.where, sql)
                        , SqlUtil.ORDER_BY(model, ORDER_BY));
    }

    public static String status(Model model, Serializable id, boolean del) {
        String idVal = (id == null ? SqlUtil.getIdv(model) : id.toString());
        return String.format(SqlMethod.updateById, SqlUtil.tableName(model), SqlUtil.anno(model.selectByid(idVal), del),
                SqlUtil.getId(model), idVal);
    }

    public static String selone(Object model, List<String> list, boolean where, String... ORDER_BY) {
        SqlUtil.isNull(list);
        return String.format(SqlMethod.selectOne, SqlUtil.toFormatKey(model),
                SqlUtil.tableName(model),
                String.format(SqlMethod.where, where ? SqlUtil.where(list) : SqlUtil.mapWhere(list)),
                SqlUtil.ORDER_BY(model, ORDER_BY));
    }

    public static String selList(Object model, List<String> list, boolean where, String[] ORDER_BY) {
        return String.format(SqlMethod.select, SqlUtil.toFormatKey(model),
                SqlUtil.tableName(model), String.format(SqlMethod.where, where ? SqlUtil.where(list) : SqlUtil.mapWhere(list)),
                SqlUtil.ORDER_BY(model, ORDER_BY));
    }

    private static String toNameList(Field[] list) {
        StringBuffer sv = new StringBuffer();
        for (Field field : list) {
            String name = field.getName();
            if (name.equals(Symbol.SERIALIZABLE)) {
                continue;
            }
            sv.append(String.format(Symbol.NAMES,StrUtil.toSymbolCase(field.getName())));
        }
        return substr(sv);
    }

    public static void outLog(long runTime, SqlSession sqlSession, Integer total, Class result) {
        if (total != null && result!=null) {
            if (!result.equals(Map.class) && !result.equals(Long.class)){
                log.info(String.format(Symbol.COLUMNS, toNameList(superField(result))));
            }
            log.info(String.format(Symbol.TOTAL, total));
        }else if (total!=null) {
            log.info(String.format(Symbol.UPDATES, total));
        }
        log.info(String.format(Symbol.run_time, runTime));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
    }
}
