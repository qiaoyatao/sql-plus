package com.qiaoyatao.sqlplus.core;

import com.qiaoyatao.sqlplus.core.format.*;
import com.qiaoyatao.sqlplus.system.bean.*;
import com.qiaoyatao.sqlplus.helper.SqlSession3;
import com.qiaoyatao.sqlplus.core.page.*;
import com.qiaoyatao.sqlplus.system.valid.Validation;
import java.io.Serializable;
import java.util.*;

/**
 * 基类实例
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public class Model<T> {

    public int insert() {
        return SqlUtil.toFormat(this, 0) == null ? 0 : SqlSession3.insert(SqlFormat.insert(this));
    }

    public int insertOrUpdate() {
        return SqlUtil.getIdv(this).length() == 0 ? insert() : updateById();
    }

    public int insertOrUpdate(Object param) {
        return SqlUtil.getIdv(this.copyBean(param)).length() == 0 ? insert(param) : this.copyBean(param).updateById();
    }

    public int insert(Object param) {
        return SqlUtil.toFormat(copyBean(param), 0) == null ? 0 : SqlSession3.insert(SqlFormat.insert(copyBean(param)));
    }

    public static int insert(Class model, Object param) {
        return SqlUtil.toFormat(BeanUtil.copyBean(param, model), 0) == null ? 0 : SqlSession3.insert(SqlFormat.insert((Model) BeanUtil.copyBean(param, model)));
    }

    public int insert(Map<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.insert(SqlFormat.insert(this, param));
    }

    public static int insert(Class model, Map<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.insert(SqlFormat.insert(SqlUtil.newInstance(model), param));
    }

    public static void insertList(List modes) {
        modes.forEach(model -> {
            SqlSession3.insert(SqlFormat.insert(model));
        });
    }

    public static void insertList(Object... modes) {
        Arrays.asList(modes).forEach(model -> {
            SqlSession3.insert(SqlFormat.insert(model));
        });
    }

    public int deleteById(Serializable id) {
        return id == null || String.valueOf(id).length() == 0 ? 0 : SqlSession3.delete(SqlFormat.deleteById(id, this));
    }

    public int deleteByIds(Serializable... ids) {
        return ids == null || ids[0].equals("") ? 0 : SqlSession3.delete(SqlFormat.deleteByIds(this, ids));
    }

    public int deleteById() {
        return SqlUtil.getIdv(this).length() == 0 ? 0 : SqlSession3.delete(SqlFormat.deleteById(this));
    }

    public int deleteById(Object param) {
        return SqlUtil.getIdv(this.copyBean(param)).length() == 0 ? 0 : SqlSession3.delete(SqlFormat.deleteById(this.copyBean(param)));
    }

    public List<T> selectList(String... ORDER_BY) {
        return (List<T>) SqlSession3.selectList(SqlFormat.selectList(this, ORDER_BY), this.getClass());
    }

    //条件查询 返回具体vo参数ORDER_BY类
    public <R> List<R> selectList(Object param, Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(this, param, ORDER_BY), resultVO);
    }

    //条件查询
    public List<T> selectList(Object param, String... ORDER_BY) {
        return (List<T>) SqlSession3.selectList(SqlFormat.selectList(this, param, ORDER_BY), this.getClass());
    }

    //条件查询
    public List<Map<String, Object>> selectByList(Object param, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(this, param, ORDER_BY));
    }

    //条件查询实体类属性决定查询条件参数ORDER_BY类
    public <R> List<R> selectList(Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(this, ORDER_BY), resultVO);
    }

    //条件更新
    public int update(Object param) {
        return SqlUtil.toFormat(param) == null ? 0 : SqlSession3.update(SqlFormat.update(this, param));
    }

    //条件更新model数据，where更新条件
    public int update(Object model, Object where) {
        return (SqlUtil.toFormat(where) == null || SqlUtil.toSetClass(this.copyBean(model)) == null) ? 0 : SqlSession3.update(SqlFormat.update(this.copyBean(model), where));
    }

    //id更新数据
    public int updateById() {
        return SqlUtil.getIdv(this).length() == 0 ? 0 : SqlSession3.update(SqlFormat.updateById(this));
    }

    //id更新数据
    public int updateById(Serializable id) {
        return (id == null || String.valueOf(id).length() == 0) || SqlUtil.toSetClass(this) == null ? 0 : SqlSession3.update(SqlFormat.updateById(id, this));
    }

    //id (param更新数据)
    public int updateById(Serializable id, Object param) {
        return (id == null || String.valueOf(id).length() == 0) || SqlUtil.toSetClass(this.copyBean(param)) == null ? 0 : SqlSession3.update(SqlFormat.updateById(id, this.copyBean(param)));
    }

    //id (param更新数据)
    public int updateByid(Object param) {
        return SqlUtil.toSetClass(this.copyBean(param)) == null ? 0 : SqlSession3.update(SqlFormat.updateById(this.copyBean(param)));
    }

    //id查询
    public T selectByid(Serializable id) {
        return id == null || String.valueOf(id).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(id, this), this.getClass());
    }

    //id查询返回具体VO
    public <R> R selectByid(Serializable id, Class<R> resultVO) {
        return id == null || String.valueOf(id).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(id, this), resultVO);
    }

    //id查询（通过实体类id属性）
    public T selectByid() {
        return SqlUtil.getIdv(this).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(this), this.getClass());
    }

    //id查询（通过实体类id属性）返回具体VO类
    public <R> R selectByid(Class<R> resultVO) {
        return SqlUtil.getIdv(this).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(this), resultVO);
    }

    //id查询（通过param id属性）
    public T selectByid(Object param) {
        return SqlUtil.getIdv(this.copyBean(param)).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(this.copyBean(param)), this.getClass());
    }

    //id查询（通过param id属性）
    public <R> R selectByid(Object param, Class<R> resultVO) {
        return SqlUtil.getIdv(this.copyBean(param)).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(this.copyBean(param)), resultVO);
    }

    //id查询（返回map）
    public Map<String, Object> selectMapByid(Serializable id) {
        return id == null || String.valueOf(id).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(id, this));
    }

    //id查询（通过实体类id属性）
    public Map<String, Object> selectMapByid() {
        return SqlUtil.getIdv(this).length() == 0 ? null : SqlSession3.selectOne(SqlFormat.selectByid(this));
    }

    //条件删除
    public int delete() {
        return SqlUtil.toFormat(this) == null ? 0 : SqlSession3.delete(SqlFormat.delete(this));
    }

    //条件删除
    public int delete(Object param) {
        return SqlUtil.toFormat(param) == null ? 0 : SqlSession3.delete(SqlFormat.delete(this, param));
    }

    //统计
    public Long count(Object param) {
        return SqlSession3.count(SqlFormat.selectCount(this, param));
    }

    //统计
    public Long count(Map<String, Object> param) {
        return SqlSession3.count(SqlFormat.selectCount(this, param));
    }

    //统计
    public Long count() {
        return count(this);
    }

    //统计
    public Long count(QueryModel queryModel) {
        return SqlSession3.count(SqlFormat.selectCount(this, queryModel));
    }

    //分页
    public Map<String, Object> selectMapPage(Page page, String... ORDER_BY) {
        return selectMapPage(page, this, ORDER_BY);
    }

    public Map<String, Object> selectMapPage(Page page, Object param, String... ORDER_BY) {
        return SqlUtil.setPage(page, SqlSession3.selectList(SqlFormat.selectPage(this, param, page, ORDER_BY)),
                count(param));
    }

    public Map<String, Object> selectMapPage(Page page, QueryModel queryModel, String... ORDER_BY) {
        return SqlUtil.setPage(page,
                SqlSession3.selectList(SqlFormat.selectPage(this, page, queryModel, ORDER_BY)),
                count(queryModel));
    }

    public Map<String, Object> selectMapPage(Page page, Map<String, Object> param, String... ORDER_BY) {
        return SqlUtil.setPage(page,
                SqlSession3.selectList(SqlFormat.selectPage(page, param, this, ORDER_BY)),
                count(param));
    }

    public PageInfo<T> selectPage(Page page, String... ORDER_BY) {
        return selectPage(page, this, ORDER_BY);
    }

    public PageInfo<T> selectPage(Page page, QueryModel queryModel, String... ORDER_BY) {
        return new PageInfo<T>((List) SqlSession3.selectList(SqlFormat.selectPage(this, page, queryModel, ORDER_BY), this.getClass()),
                count(queryModel), page);
    }

    public <R> PageInfo<R> selectPage(Page page, QueryModel queryModel, Class<R> resultVO, String... ORDER_BY) {
        return new PageInfo<R>(SqlSession3.selectList(SqlFormat.selectPage(this, page, queryModel, ORDER_BY),
                resultVO), count(queryModel),
                page);
    }

    public PageInfo<T> selectPage(Page page, Object param, String... ORDER_BY) {
        return new PageInfo(SqlSession3.selectList(SqlFormat.selectPage(this, param, page, ORDER_BY),
                this.getClass()), count(param), page);
    }

    public <R> PageInfo<R> selectPage(Page page, Object param, Class<R> resultVO, String... ORDER_BY) {
        return new PageInfo<R>(SqlSession3.selectList(SqlFormat.selectPage(this, param, page, ORDER_BY),
                resultVO), count(param), page);
    }

    public <R> PageInfo<R> selectPage(Page page, Class<R> resultVO, String... ORDER_BY) {
        return selectPage(page, this, resultVO, ORDER_BY);
    }

    public <R> PageInfo<R> selectPage(Page page, Map<String, Object> param, Class<R> resultVO, String... ORDER_BY) {
        return new PageInfo<R>(SqlSession3.selectList(SqlFormat.selectPage(page, param, this, ORDER_BY), resultVO),
                count(param), page);
    }

    public PageInfo<T> selectPage(Page page, Map<String, Object> param, String... ORDER_BY) {
        return new PageInfo<T>((List) SqlSession3.selectList(SqlFormat.selectPage(page, param, this, ORDER_BY), this.getClass()), count(param), page);
    }

    //QueryMode条件查询
    public List<T> selectList(QueryModel queryModel, String... ORDER_BY) {
        return (List<T>) SqlSession3.selectList(SqlFormat.selectList(queryModel, this, ORDER_BY), this.getClass());
    }

    //QueryMode条件查询
    public <R> List<R> selectList(QueryModel queryModel, Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(queryModel, this, ORDER_BY), resultVO);
    }

    //QueryMode条件查询一条
    public T selectOne(QueryModel queryModel, String... ORDER_BY) {
        return (T) selectOne(queryModel, this.getClass(), ORDER_BY);
    }

    //QueryMode条件查询一条
    public <R> R selectOne(QueryModel queryModel, Class<R> resultVO, String... ORDER_BY) {
        return queryModel.sql() == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(queryModel, this, ORDER_BY), resultVO);
    }

    //查询一条(根据实体类属性决定查询条件)
    public T selectOne(String... ORDER_BY) {
        return SqlUtil.toFormat(this) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, ORDER_BY), this.getClass());
    }

    //查询一条(根据param属性决定查询条件)
    public T selectOne(Object param, String... ORDER_BY) {
        return SqlUtil.toFormat(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, param, ORDER_BY), this.getClass());
    }

    //查询一条(根据实体类属性决定查询条件)
    public Map<String, Object> selectMapOne(String... ORDER_BY) {
        return SqlUtil.toFormat(this) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, ORDER_BY));
    }

    //查询一条(根据param属性决定查询条件)
    public Map<String, Object> selectMapOne(Object param, String... ORDER_BY) {
        return SqlUtil.toFormat(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, param, ORDER_BY));
    }

    //查询一条(根据param属性决定查询条件) 返回vo
    public <R> R selectOne(Object param, Class<R> resultVO, String... ORDER_BY) {
        return SqlUtil.toFormat(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, param, ORDER_BY), resultVO);
    }

    //查询一条(根据实体类属性决定查询条件) 返回vo
    public <R> R selectOne(Class<R> resultVO, String... ORDER_BY) {
        return SqlUtil.toFormat(this) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, ORDER_BY), resultVO);
    }

    //查询一条(map查询)
    public T selectOne(Map<String, Object> param, String... ORDER_BY) {
        return SqlUtil.toMap(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, param, ORDER_BY), this.getClass());
    }

    //查询一条(map查询)
    public <R> R selectOne(Map<String, Object> param, Class<R> resultVO, String... ORDER_BY) {
        return SqlUtil.toMap(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, param, ORDER_BY), resultVO);
    }

    //查询一条(map查询)
    public Map<String, Object> selectMapOne(Map<String, Object> param, String... ORDER_BY) {
        return SqlUtil.toMap(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, param, ORDER_BY));
    }

    //LinkedHashMap 有序map查询
    public List<T> selectLinkedList(LinkedHashMap<String, Object> param, String... ORDER_BY) {
        return (List<T>) SqlSession3.selectList(SqlFormat.selectList(this, param, ORDER_BY), this.getClass());
    }

    public <R> List<R> selectLinkedList(LinkedHashMap<String, Object> param, Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(this, param, ORDER_BY), resultVO);
    }

    public T selectLinkedOne(LinkedHashMap<String, Object> param, String... ORDER_BY) {
        return SqlUtil.toMap(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, param, ORDER_BY), this.getClass());
    }

    public <R> R selectLinkedOne(LinkedHashMap<String, Object> param, Class<R> resultVO, String... ORDER_BY) {
        return SqlUtil.toMap(param) == null ? null : SqlSession3.selectOne(SqlFormat.selectOne(this, param, ORDER_BY), resultVO);
    }

    //查询一条(map查询)
    public List<T> selectList(Map<String, Object> param, String... ORDER_BY) {
        return (List<T>) SqlSession3.selectList(SqlFormat.selectList(this, param, ORDER_BY), this.getClass());
    }

    //查询一条(map查询)
    public List<Map<String, Object>> selectMapList(Map<String, Object> param, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(this, param, ORDER_BY));
    }

    //查询多条(map查询)
    public List<Map<String, Object>> selectMapList(String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(this, ORDER_BY));
    }

    //查询多条(map查询)
    public <R> List<R> selectList(Map<String, Object> param, Class<R> resultVO, String... ORDER_BY) {
        return SqlSession3.selectList(SqlFormat.selectList(this, param, ORDER_BY), resultVO);
    }

    //LinkedHashMap条件删除
    public int deleteLinked(LinkedHashMap<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.delete(SqlFormat.delete(param, this));
    }

    //map条件删除
    public int delete(Map<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.delete(SqlFormat.delete(param, this));
    }

    //queryModel条件删除
    public int delete(QueryModel queryModel) {
        return queryModel.sql() == null ? 0 : SqlSession3.delete(SqlFormat.delete(queryModel, this));
    }

    //queryModel条件修改
    public int update(QueryModel queryModel) {
        return queryModel.sql() == null ? 0 : SqlSession3.update(SqlFormat.update(queryModel, this));
    }

    //LinkedHashMap条件修改
    public int updateLinked(LinkedHashMap<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.update(SqlFormat.update(param, this));
    }

    //Map条件修改
    public int update(Map<String, Object> param) {
        return SqlUtil.toMap(param) == null ? 0 : SqlSession3.update(SqlFormat.update(param, this));
    }

    //创建表
    public boolean create() {
        return SqlSession3.insert(SqlFormat.create(this))>0?true:false;
    }

    //查询全量sql
    public List<T> fullList(QueryModel queryModel) {
        return (List<T>) SqlSession3.selectList(SqlFormat.fullList(queryModel, this), this.getClass());
    }

    //查询全量sql
    public <R> List<R> fullList(QueryModel queryModel, Class<R> resultVO) {
        return SqlSession3.selectList(SqlFormat.fullList(queryModel, this), resultVO);
    }

    //修改通过id@Delete状态
    public boolean del(Serializable id) {
        return SqlUtil.is(SqlUtil.status(this, id, true));
    }

    //修改通过id@Delete状态
    public boolean del() {
        return del(null);
    }

    //修改通过id@Status状态
    public boolean status() {
        return status(null);
    }

    //修改通过id@Status状态
    public boolean status(Serializable id) {
        return SqlUtil.is(SqlUtil.status(this, id, false));
    }

    //清空数据并重置id
    public int truncate() {
        return truncate(this.getClass());
    }

    //清空数据并重置id
    public static int truncate(Class model) {
        return SqlSession3.delete(SqlFormat.truncate(SqlUtil.newInstance(model)));
    }

    //删除表
    public int dropTable() {
        return dropTable(this.getClass());
    }

    //删除表
    public static int dropTable(Class model) {
        return SqlSession3.delete(SqlFormat.dropTable(SqlUtil.newInstance(model)));
    }

    //查看表结构
    public Desc desc() {
        return desc(this.getClass());
    }

    //查看表结构
    public static Desc desc(Class model) {
        return SqlSession3.selectOne(SqlFormat.desc(SqlUtil.newInstance(model)), Desc.class);
    }

    //查看创建语句
    public ShowCreate showCreate() {
        return showCreate(this.getClass());
    }

    //查看创建语句
    public static ShowCreate showCreate(Class model) {
        return SqlSession3.selectOne(SqlFormat.showCreate(SqlUtil.newInstance(model)), ShowCreate.class);
    }

    //list拷贝到target对象中
    public static List copyToList(List list, Object target) {
        return copyToList(list, target, false);
    }

    //list拷贝到target对象中 replace 是否替换原值（如果新值没有，原值不动）
    public static List copyToList(List list, Object target, boolean replace) {
        List result = new ArrayList<>();
        list.forEach(model -> {
            result.add(BeanUtil.copyAsNameParam(model, target, replace));
        });
        return result;
    }

    //this拷贝到target对象中
    public void copyTo(Object target) {
        BeanUtil.copyAsName(this, target);
    }

    //this拷贝到target对象中
    public void copyTo(Object target, boolean replace) {
        BeanUtil.copyAsName(this, target, replace);
    }

    //this拷贝到target对象中
    public <T> T copyTo(Class<T> target) {
        return BeanUtil.copyBean(this, target);
    }

    //list拷贝到target对象中
    public static List copyToList(List list, Class target) {
        return copyToList(list, target, false);
    }

    //list拷贝到target对象中
    public static List copyToList(List list, Class target, boolean replace) {
        List result = new ArrayList<>();
        list.forEach(model -> {
            result.add(BeanUtil.copyBean(model, target, replace));
        });
        return result;
    }

    //param拷贝到this对象中
    public Model copyBean(Object param) {
        return copyBean(param, false);
    }

    //param拷贝到this对象中
    public Model copyBean(Object param, boolean replace) {
        BeanUtil.copyAsName(param, this);
        return this;
    }

    //获取json请求参数
    public String toParam() {
        return toParam(this.getClass());
    }

    //获取json请求参数
    public static String toParam(Class param) {
        return BeanUtil.toParam(param);
    }

    //获取json请求参数
    public String toParam(Object param) {
        return toParam(param.getClass());
    }

    //this转map
    public Map<String, Object> beanToMap() {
        return beanToMap(this);
    }

    //param转map
    public static Map<String, Object> beanToMap(Object param) {
        return BeanUtil.beanToMap(param);
    }

    //对象转map，字段下划线
    public Map<String, Object> toSymbolCase() {
        return toSymbolCase(this);
    }

    //对象转map，字段下划线
    public Map<String, Object> toSymbolCase(Object param) {
        return BeanUtil.toSymbolCase(param);
    }

    //map转this
    public static <T> T toBean(Map<String, Object> map, Class<T> resultVO) {
        return BeanUtil.toBean(map, resultVO);
    }

    //this转map(param有值不会替换)
    public static Object toBean(Map<String, Object> map, Object param) {
        return BeanUtil.toBean(map, param);
    }

    //map转对象
    public static List toBeanList(List<Map<String, Object>> list, Class resultVO) {
        List result = new ArrayList<>();
        list.forEach(model -> {
            result.add(BeanUtil.toBean(model, resultVO));
        });
        return result;
    }

    //map转对象
    public static List toBeanList(List<Map<String, Object>> list, Object param) {
        List result = new ArrayList<>();
        list.forEach(model -> {
            result.add(BeanUtil.toBean(model, param));
        });
        return result;
    }

    //数据校验
    public void valid() {
        Validation.valid(this);
    }

    public String valid(boolean isMsg) {
        return Validation.valid(isMsg, this);
    }

    public void validShow(boolean show) {
        Validation.valid(this, show, false);
    }

    public String valid(boolean show, boolean isMsg) {
        return Validation.valid(this, null, show, isMsg);
    }

    public String valid(Class group, boolean isMsg) {
        return Validation.valid(this, group, true, isMsg);
    }

    public void valid(Class group) {
        Validation.valid(this, group, true, false);
    }

    public void valid(boolean show, Class group) {
        Validation.valid(this, group, show, false);
    }

    //对象校验(继承，子类都可以)
    public String valid(Class group, boolean show, boolean isMsg) {
        return Validation.valid(this, group, show, isMsg);
    }

}
