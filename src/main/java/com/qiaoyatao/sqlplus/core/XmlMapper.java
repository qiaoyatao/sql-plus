package com.qiaoyatao.sqlplus.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Mapper
public interface XmlMapper {

    int insert(@Param("table") String table, @Param("param") Map<String, Object> param);

    int deleteByid(@Param("table") String table, @Param("id") Serializable id);

    Map<String, Object> selectById(@Param("table") String table, @Param("id") Serializable id);

    int updateById(@Param("table") String table, @Param("id") Serializable id, @Param("param") Map<String, Object> param);

    List<Map<String, Object>> selectList(@Param("table") String table, @Param("param") Map<String, Object> param);

    int delete(@Param("table") String table, @Param("param") Map<String, Object> param);

    int update(@Param("table") String table, @Param("model") Map<String, Object> model, @Param("param") Map<String, Object> param);
}
