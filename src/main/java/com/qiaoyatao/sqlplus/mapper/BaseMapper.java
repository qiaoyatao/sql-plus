package com.qiaoyatao.sqlplus.mapper;

import org.apache.ibatis.annotations.Param;
import java.io.Serializable;
import java.util.*;

public interface BaseMapper<T> {

    int insert(T model);

    int deleteById(Serializable id);

    int deleteByParam(T param);

    int updateById(T model);

    int updateByParam(@Param("model") T model, @Param("param") T param);

    T selectById(Serializable id);

    T selectOne(T param);

    List<T> selectList(T model);

    List<T> selectMapList(@Param("param") Map<String, Object> param);

}
