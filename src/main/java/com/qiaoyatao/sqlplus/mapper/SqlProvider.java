package com.qiaoyatao.sqlplus.mapper;

import com.qiaoyatao.sqlplus.core.QueryModel;
import com.qiaoyatao.sqlplus.core.format.SqlFormat;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import java.io.Serializable;
import java.util.Map;

/**
 * 注解mapper,sql文件
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public class SqlProvider {

    public String queryById(Serializable id, Class model) {
        return SqlFormat.selectByid(id, SqlUtil.newInstance(model));
    }

    public String queryModelList(QueryModel queryModel, Class model, String... ORDER_BY) {
        return SqlFormat.selectList(queryModel, SqlUtil.newInstance(model));
    }

    public String queryModel(QueryModel queryModel, Class model, String... ORDER_BY) {
        return SqlFormat.selectOne(queryModel, SqlUtil.newInstance(model),ORDER_BY);
    }

    public String queryList(Object model, String... ORDER_BY) {
        return SqlFormat.selectList(model, model, ORDER_BY);
    }

    public String queryLimit(Page page, Object model, String... ORDER_BY) {
        return SqlFormat.selectPage(model, model, page, ORDER_BY);
    }

    public String queryCount(Object model) {
        return SqlFormat.selectCount(model, model);
    }

    public String newInsert(Object model) {
        return SqlFormat.insert(model);
    }

    public String insertParam(Class model, Object param) {
        return SqlFormat.insert(BeanUtil.copyBean(param,model));
    }

    public String insertMap(Class model, Map<String, Object> param) {
        return SqlFormat.insert(SqlUtil.newInstance(model), param);
    }

    public String removeMap(Class model, Map<String, Object> param) {
        return SqlFormat.delete(param, SqlUtil.newInstance(model));
    }

    public String modifyMap(Object model, Map<String, Object> param) {
        return SqlFormat.update(param, model);
    }

    public String removeById(Serializable id, Class model) {
        return SqlFormat.deleteById(id, SqlUtil.newInstance(model));
    }

    public String removeId(Object model) {
        return SqlFormat.deleteById(model);
    }

    public String remove(Object model) {
        return SqlFormat.delete(model);
    }

    public String removeQueryModel(QueryModel queryModel, Class model) {
        return SqlFormat.delete(queryModel, SqlUtil.newInstance(model));
    }

    public String removeByIds(Class model, Serializable... ids) {
        return SqlFormat.deleteByIds(SqlUtil.newInstance(model), ids);
    }

    public String modify(Object model, Object param) {
        return SqlFormat.update(model, param);
    }

    public String modifyId(Object model) {
        return SqlFormat.updateById(model);
    }

    public String modifyById(Serializable id, Object model) {
        return SqlFormat.updateById(id, model);
    }

    public String modifyQueryModel(QueryModel queryModel, Object model) {
        return SqlFormat.update(queryModel, model);
    }

    public String queryByModel(Object model, String... ORDER_BY) {
        return SqlFormat.selectOne(model,ORDER_BY);
    }

    public String queryByParam(Class model, Object param, String... ORDER_BY) {
        return SqlFormat.selectOne(SqlUtil.newInstance(model), param,ORDER_BY);
    }

    public String queryByModelList(Class model, String... ORDER_BY) {
        return SqlFormat.selectList(SqlUtil.newInstance(model),ORDER_BY);
    }

    public String queryByParamList(Class model, Object param, String... ORDER_BY) {
        return SqlFormat.selectList(SqlUtil.newInstance(model), param,ORDER_BY);
    }

    public String queryByParamLimit(Class model, Page page, Object param, String... ORDER_BY) {
        return SqlFormat.selectPage(SqlUtil.newInstance(model), param, page, ORDER_BY);
    }

    public String queryByParamCount(Class model, Object param) {
        return SqlFormat.selectCount(SqlUtil.newInstance(model), param);
    }

    public String queryModelLimit(Class model, Page page, QueryModel queryModel, String... ORDER_BY) {
        return SqlFormat.selectPage(SqlUtil.newInstance(model), page, queryModel, ORDER_BY);
    }

    public String queryModelCount(Class model, QueryModel queryModel) {
        return SqlFormat.selectCount(SqlUtil.newInstance(model), queryModel);
    }

    public String queryMapLimit(Class model, Page page, Map<String, Object> param, String... ORDER_BY) {
        return SqlFormat.selectPage(page, param, SqlUtil.newInstance(model), ORDER_BY);
    }

    public String queryMapOne(Class model, Map<String, Object> param, String... ORDER_BY) {
        return SqlFormat.selectOne(SqlUtil.newInstance(model), param,ORDER_BY);
    }

    public String queryMapCount(Class model, Map<String, Object> param) {
        return SqlFormat.selectCount(SqlUtil.newInstance(model), param);
    }

    public String queryMapList(Class model, Map<String, Object> param, String... ORDER_BY) {
        return SqlFormat.selectList(SqlUtil.newInstance(model), param, ORDER_BY);
    }

}
