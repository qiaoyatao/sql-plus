package com.qiaoyatao.sqlplus.mapper;

import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.core.page.Page;
import com.qiaoyatao.sqlplus.core.QueryModel;
import org.apache.ibatis.annotations.*;
import java.io.Serializable;
import java.util.*;

/**
 * 注解mapper
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
@Mapper
public interface SupperMapper<T> {

    @InsertProvider(type = SqlProvider.class, method = Symbol.newInsert)
    int Insert(T model);

    @InsertProvider(type = SqlProvider.class, method = Symbol.insertParam)
    int insertParam(Class model, Object param);

    @InsertProvider(type = SqlProvider.class, method = Symbol.insertMap)
    int insertMap(Class model, Map<String, Object> param);

    @InsertProvider(type = SqlProvider.class, method = Symbol.removeMap)
    int removeMap(Class model, Map<String, Object> param);

    @InsertProvider(type = SqlProvider.class, method = Symbol.modifyMap)
    int modifyMap(T model, Map<String, Object> param);

    @DeleteProvider(type = SqlProvider.class, method = Symbol.removeById)
    int removeById(Serializable id, Class model);

    @DeleteProvider(type = SqlProvider.class, method = Symbol.removeId)
    int removeId(T model);

    @DeleteProvider(type = SqlProvider.class, method = Symbol.removeByIds)
    int removeByIds(Class model, Serializable... ids);

    @DeleteProvider(type = SqlProvider.class, method = Symbol.remove)
    int remove(T model);

    @DeleteProvider(type = SqlProvider.class, method = Symbol.removeQueryModel)
    int removeQueryModel(QueryModel queryModel, Class model);

    @UpdateProvider(type = SqlProvider.class, method = Symbol.modifyById)
    int modifyById(Serializable id, T model);

    @UpdateProvider(type = SqlProvider.class, method = Symbol.modifyId)
    int modifyId(T model);

    @UpdateProvider(type = SqlProvider.class, method = Symbol.modify)
    int modify(T model, Object param);

    @UpdateProvider(type = SqlProvider.class, method = Symbol.modifyQueryModel)
    int modifyQueryModel(QueryModel queryModel, T model);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryById)
    T queryById(Serializable id, Class model);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryByModel)
    T queryByModel(T model, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryByModelList)
    List<T> queryByModelList(Class model, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryByParam)
    T queryByParam(Class model, Object param, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryByParamList)
    List<T> queryByParamList(Class model, Object param, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryByParamLimit)
    List<T> queryByParamLimit(Class model, Page page, Object param, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryByParamCount)
    long queryByParamCount(Class model, Object param);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryModelList)
    List<T> queryModelList(QueryModel queryModel, Class model, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryModelLimit)
    List<T> queryModelLimit(Class model, Page page, QueryModel queryModel, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryModelCount)
    long queryModelCount(Class model, QueryModel queryModel);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryMapLimit)
    List<T> queryMapLimit(Class model, Page page, Map<String, Object> param, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryMapList)
    List<T> queryMapList(Class model, Map<String, Object> param, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryMapCount)
    long queryMapCount(Class model, Map<String, Object> param);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryMapOne)
    T queryMapOne(Class model, Map<String, Object> param, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryModel)
    T queryModel(QueryModel queryModel, Class model, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryLimit)
    List<T> queryLimit(Page page, T model, String... ORDER_BY);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryCount)
    long queryCount(T model);

    @SelectProvider(type = SqlProvider.class, method = Symbol.queryList)
    List<T> queryList(T model, String... ORDER_BY);

}
