package com.qiaoyatao.sqlplus.system.valid;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.qiaoyatao.sqlplus.annotation.valid.OR;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import javax.validation.constraints.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/**
 * 自定义校验工具
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public class Validation {

    public static void valid(Object param) {
        valid(false, param);
    }

    public static String valid(boolean isMsg, Object param) {
        return valid(param, null, true, isMsg);
    }

    public static void valid(Object param, boolean show) {
        valid(param, show, false);
    }

    public static String valid(Object param, boolean show, boolean isMsg) {
        return valid(param, null, show, isMsg);
    }

    public static String valid(Object param, Class<?> group, boolean isMsg) {
        return valid(param, group, true, isMsg);
    }

    public static void valid(Object param, Class<?> group) {
        valid(param, group, true, false);
    }

    public static void valid(Object param, boolean show, Class<?> group) {
        valid(param, group, show, false);
    }

    /**
     * @param param 校验对象
     */
    public static void validList(Object param) {
        validList(param, null, true, false);
    }

    /**
     * @param param 校验对象
     */
    public static String validList(boolean isMsg, Object param) {
        return validList(param, null, true, isMsg);
    }

    /**
     * @param param 校验对象
     * @param group 分组校验
     */
    public static void validList(Object param, Class<?> group) {
        validList(param, group, true, false);
    }

    /**
     * @param param 校验对象
     * @param show  是否显示name true显示 flase 不显示
     * @param isMsg 是否返回错误信息 true返回false不返回
     */
    public static String validList(Object param, boolean show, boolean isMsg) {
        return validList(param, null, show, isMsg);
    }

    /**
     * @param param 校验对象
     * @param show  是否显示name true显示 flase 不显示
     */
    public static void validList(Object param, boolean show) {
        validList(param, null, show, false);
    }

    /**
     * @param param 校验对象
     * @param group 分组校验
     */
    public static String validList(Object param, Class<?> group, boolean show, boolean isMsg) {
        if (!param.getClass().getTypeName().equals(new ArrayList<>().getClass().getName())) {
            return valid(param, group, show, isMsg);
        }
        StringBuffer msg = new StringBuffer();
        for (Object field : (List) param) {
            msg.append(valid(field, group, show, isMsg) + ",");
        }
        return SqlUtil.substr(msg);
    }

    /**
     * @param param 校验对象
     * @param group 分组
     * @param show  是否显示字段名称
     */
    //对象校验(继承，子类都可以)
    public static String valid(Object param, Class<?> group, boolean show, boolean isMsg) {
        if (param == null) {
            return null;
        }
        StringBuffer msg = new StringBuffer();
        for (Field field : SqlUtil.superField(param.getClass())) {
            if (validType(field.getType())) {
                Object newClass = SqlUtil.get(field,param);
                if (newClass == null) {
                    continue;
                }
                for (Field field1 : newClass.getClass().getDeclaredFields()) {
                    String fieldName = validField(SqlUtil.get(field,param), group, field1, show);
                    if (fieldName != null) {
                        msg.append(fieldName + ",");
                    }
                }
                continue;
            }
            String fieldName = validField(param, group, field, show);
            if (fieldName != null) {
                msg.append(fieldName + ",");
            }
        }
        if (msg.toString().length() != 0) {
            if (isMsg) {
                return SqlUtil.substr(msg);
            }
            throw new RuntimeException(SqlUtil.substr(msg));
        }
        return null;
    }

    private static String validField(Object param, Class<?> group, Field field, boolean show) {
        field.setAccessible(true);
        Null aNull = field.getAnnotation(Null.class);
        Object value = SqlUtil.get(field, param);
        if (aNull != null && (value != null && value.toString().length() != 0)) {
            return ck(aNull.groups(), group, field, aNull.message(), show);
        }
        NotNull notNull = field.getAnnotation(NotNull.class);
        if (notNull != null && value == null) {
            return ck(notNull.groups(), group, field, notNull.message(), show);
        }
        NotBlank notBlank = field.getAnnotation(NotBlank.class);
        if (notBlank != null && (value == null || value.toString().length() == 0)) {
            return ck(notBlank.groups(), group, field, notBlank.message(), show);
        }
        Min min = field.getAnnotation(Min.class);
        if (min != null && (value != null && Integer.parseInt(value.toString()) < min.value())) {
            return ck(min.groups(), group, field, min.message(), show);
        }
        Max max = field.getAnnotation(Max.class);
        if (max != null && (value != null && Integer.parseInt(value.toString()) > max.value())) {
            return ck(max.groups(), group, field, max.message(), show);
        }
        Range range = field.getAnnotation(Range.class);
        if (range != null && (value != null)) {
            int val = Integer.parseInt(value.toString());
            if (val < range.min() || val > range.max()) {
                return ck(range.groups(), group, field, range.message(), show);
            }
        }
        Email email = field.getAnnotation(Email.class);
        if (email != null && (value != null && value.toString().length() != 0)) {
            String val = value.toString();
            /*if (Pattern.matches(Symbol.EMAIL,val)){
                return ck(email.groups(), group, field,email.message());
            }*/
        }
        Past past = field.getAnnotation(Past.class);
        if (past != null && (value != null && value.toString().length() != 0)) {
            if (new Date().before((Date) value)) {
                return ck(past.groups(), group, field, past.message(), show);
            }
        }
        Future future = field.getAnnotation(Future.class);
        if (future != null && (value != null && value.toString().length() != 0)) {
            if (new Date().after((Date) value)) {
                return ck(future.groups(), group, field, future.message(), show);
            }
        }
        Length length = field.getAnnotation(Length.class);
        if (length != null && value != null) {
            String val = value.toString();
            if (val.length() < length.min() || val.length() > length.max()) {
                return ck(length.groups(), group, field, length.message(), show);
            }
        }
        AssertTrue aTrue = field.getAnnotation(AssertTrue.class);
        if (aTrue != null && value != null) {
            boolean b = Boolean.parseBoolean(value.toString());
            if (!b) {
                return ck(aTrue.groups(), group, field, aTrue.message(), show);
            }
        }
        AssertFalse aFalse = field.getAnnotation(AssertFalse.class);
        if (aFalse != null && value != null) {
            boolean b = Boolean.parseBoolean(value.toString());
            if (b) {
                return ck(aFalse.groups(), group, field, aFalse.message(), show);
            }
        }
        OR or = field.getAnnotation(OR.class);
        if (or != null && value != null) {
            String[] split = or.value().split(",");
            if (Arrays.binarySearch(split, value.toString()) < 0) {
                return ck(or.groups(), group, field, or.message(), show);
            }
        }
        return null;
    }

    private static String ck(Class<?>[] groups, Class<?> group, Field field, String message, boolean show) {
        if (group != null && groups.length != 0) {
            String name = groups(groups, group, field);
            //show 是否显示字段名称
            return name == null?null:(show?(name += ":" + message):message);
        } else if (group != null) {
            return null;
        } else if (group == null && groups.length != 0) {
            return null;
        }
        return show? (field.getName() + ":" + message):message;
    }

    //获取组
    private static String groups(Class<?>[] groups, Class<?> group, Field field) {
        for (Class<?> model : groups) {
            //允许组继承
            if (model.equals(group) || model.equals(group.getSuperclass())) {
                return field.getName();
            }
        }
        return null;
    }
    //判断是否为包装类型
    public static boolean validType(Class<?> type) {
        if (type.equals(String.class) || type.equals(Set.class)) {
            return false;
        } else if (type.equals(Integer.class) || type.equals(int.class)) {
            return false;
        } else if (type.equals(BigDecimal.class) || type.equals(Date.class)) {
            return false;
        } else if (type.equals(Long.class) || type.equals(long.class)) {
            return false;
        } else if (type.equals(Double.class) || type.equals(double.class)) {
            return false;
        } else if (type.equals(Boolean.class) || type.equals(boolean.class)) {
            return false;
        } else if (type.equals(Byte.class) || type.equals(byte.class)) {
            return false;
        } else if (type.equals(Short.class) || type.equals(short.class)) {
            return false;
        } else if (type.equals(Char.class) || type.equals(char.class)) {
            return false;
        } else if (type.equals(List.class) || type.equals(Map.class)) {
            return false;
        } else if (type.equals(HashMap.class) || type.equals(HashSet.class)) {
            return false;
        }
        return true;
    }
}
