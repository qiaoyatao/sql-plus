package com.qiaoyatao.sqlplus.system.valid;

import com.alibaba.druid.util.StringUtils;
import com.qiaoyatao.sqlplus.constants.Symbol;
import org.springframework.util.CollectionUtils;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 断言工具
 * 2019年12月27日02:23:23
 */
public class Assert {

    /**
     * 对象为空时
     */
    public static void isNull(Object obj, String message) {
        if (Objects.isNull(obj)) {
            message(message);
        }
    }

    public static void assertEquals(String message, int expected,
                                    int actual) {
        if (expected == actual) {
            return;
        } else {
            String cleanMessage = message == null ? "" : message;
            message(cleanMessage);
        }
    }

    /**
     * 对象不为空时
     */
    public static void isNotNull(Object obj, String message) {
        if (obj != null) {
            message(message);
        }
    }

    /**
     * 对象不为空时
     */
    public static void isNotNull(Integer obj, String message) {
        if (obj != null && obj != 0) {
            message(message);
        }
    }

    /**
     * 对象为空时
     */
    public static void isNull(Object obj) {
        if (Objects.isNull(obj)) {
            message(Symbol.NULL);
        }
    }

    /**
     * 集合为空时
     */
    public static void isEmpty(List list, String message) {
        if (CollectionUtils.isEmpty(list)) {
            message(message);
        }
    }

    /**
     * 对象不为空时
     */
    public static void nonNull(Object obj, String message) {
        if (Objects.nonNull(obj)) {
            message(message);
        }
    }

    /**
     * 对象不为空时
     */
    public static void nonNull(Object obj) {
        if (Objects.nonNull(obj)) {
            message(Symbol.MUST);
        }
    }

    /**
     * 对象相等时
     */
    public static void equals(Object val1, Object val2, String message) {
        if (Objects.equals(val1, val2)) {
            message(message);
        }
    }

    /**
     * 对象相等时
     */
    public static void equals(Object val1, Object val2) {
        if (Objects.equals(val1, val2)) {
            message("对象相等时");
        }
    }

    /**
     * 对象不相等时
     */
    public static void unequals(Object val1, Object val2, String message) {
        if (!Objects.equals(val1, val2)) {
            message(message);
        }
    }

    /**
     * 对象不相等时
     */
    public static void unequals(Object val1, Object val2) {
        if (!Objects.equals(val1, val2)) {
            message("对象不相等时");
        }
    }

    /**
     * 字符串相等时
     */
    public static void equals(String val1, String val2, String message) {
        if (StringUtils.equals(val1, val2)) {
            message(message);
        }
    }

    /**
     * 字符串相等时
     */
    public static void equals(String val1, String val2) {
        if (StringUtils.equals(val1, val2)) {
            message("字符串相等时");
        }
    }


    /**
     * 字符串不相等时
     */
    public static void unequals(String val1, String val2, String message) {
        if (!StringUtils.equals(val1, val2)) {
            message(message);
        }
    }

    /**
     * 字符串不相等时
     */
    public static void unequals(String val1, String val2) {
        if (!StringUtils.equals(val1, val2)) {
            message("字符串不相等时");
        }
    }

    /**
     * 集合不为空时
     */
    public static void isNotEmpty(Collection coll, String message) {
        if (!CollectionUtils.isEmpty(coll)) {
            message(message);
        }
    }


    /**
     * 集合不为空时
     */
    public static void isNotEmpty(Collection coll) {
        if (!CollectionUtils.isEmpty(coll)) {
            message("集合不为空时");
        }
    }

    /**
     * 集合为空时
     */
    public static void isEmpty(Collection coll, String message) {
        if (CollectionUtils.isEmpty(coll)) {
            message(message);
        }
    }

    /**
     * int为空时
     */
    public static void isIntBlank(Integer coll, String message) {
        if (coll == null) {
            message(message);
        }
    }

    /**
     * 集合为空时
     */
    public static void isEmpty(Collection coll) {
        if (CollectionUtils.isEmpty(coll)) {
            message("集合为空时");
        }
    }


    /**
     * 字符串为空
     */
    public static void isBlank(String str, String message) {
        if (isNull(str)) {
            message(message);
        }
    }

    /**
     * 字符串为空
     */
    public static void isBlank(String str) {
        if (isNull(str)) {
            message("字符串为空");
        }
    }

    /**
     * 字符串不为空
     */
    public static void isNotBlank(String str, String message) {
        if (!isNull(str)) {
            message(message);
        }
    }

    /**
     * 字符串不为空
     */
    public static void isNotBlank(String str) {
        if (!isNull(str)) {
            message("字符串不为空");
        }
    }
    /**
     * 判断为真时
     */
    public static void isTrue(boolean judgment, String message) {
        if (judgment) {
            message(message);
        }
    }

    /**
     * 判断为真时
     * @param judgment
     * @
     */
    public static void isTrue(boolean judgment) {
        if (judgment) {
            message("判断为true");
        }
    }

    /**
     * 判断为假时
     */
    public static void isFalse(boolean judgment, String message) {
        isTrue(!judgment, message);
    }

    /**
     * 判断为假时
     */
    public static void isFalse(boolean judgment) {
        isTrue(!judgment, "判断为false");
    }

    /**
     * 集合中包含obj时
     */
    public static <T> void contains(Collection<T> coll, T obj, String message) {
        if (coll.contains(obj)) {
            message(message);
        }
    }
    /**
     * 集合中不包含obj时
     */
    public static <T> void notContains(Collection<T> coll, T obj, String message) {
        if (!coll.contains(obj)) {
            message(message);
        }
    }

    /**
     * 集合中不包含obj时
     */
    public static <T> void notContains(Collection<T> coll, T obj) {
        if (!coll.contains(obj)) {
            message("集合中不包含obj时");
        }
    }

    /**
     * 抛出业务异常
     * @param message
     */
    public static void message(String message) {
        throw new RuntimeException(message);
    }

    private static boolean isNull(String str) {
        if (str == null || str.length() == 0) {
            return true;
        }
        return false;
    }

}

