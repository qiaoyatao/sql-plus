package com.qiaoyatao.sqlplus.system.valid;

import com.qiaoyatao.sqlplus.constants.Symbol;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * 系统实体验证工具类
 *
 * @author machengxuan
 * @date 2019/03/07
 */
public class ValidatorUtil {

    private static ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    /**
     * 验证并且在不通过时抛出异常消息
     * @param t
     * @param groups
     * @param <T>
     */
    public static <T> void validate(T t, Class... groups) {
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<T>> validateResult = validator.validate(t, groups);
        String message = generalMethod(validateResult);
        Assert.isTrue(!StringUtils.isEmpty(message), message);
    }

    /**
     * Collection 验证并且在不通过时抛出异常消息
     *
     * @param collection
     * @param groups
     * @param <T>
     */
    public static <T> void validateCollection(Collection<T> collection, Class... groups) {
        StringBuffer message = new StringBuffer();
        Validator validator = validatorFactory.getValidator();
        collection.forEach(e -> {
            Set<ConstraintViolation<T>> validateResult = validator.validate(e, groups);
            if (!validateResult.isEmpty()) {
                message.append(generalMethod(validateResult));
            }
        });
        Assert.isTrue(!StringUtils.isEmpty(message.toString()), message.toString());
    }

    /**
     * 通用validateResult处理方法
     *
     * @param validateResult
     * @param <T>
     * @return
     */
    private static <T> String generalMethod(Set<ConstraintViolation<T>> validateResult) {
        if (!validateResult.isEmpty()) {
            List<String> errorList = new ArrayList<String>();
            validateResult.forEach(e -> {
                errorList.add(e.getPropertyPath().toString() + ":" + e.getMessage());
            });
            return errorList.toString();
        }
        return null;
    }

    /**
     * 获取验证结果set
     *
     * @param t
     * @param groups
     * @param <T>
     */
    public static <T> Set<ConstraintViolation<T>> getValidateResult(T t, Class... groups) {
        Validator validator = validatorFactory.getValidator();
        return validator.validate(t, groups);
    }

    /**
     * 获取 Validator
     *
     * @param <T>
     */
    public static <T> Validator getValidator() {
        return validatorFactory.getValidator();
    }


    /**
     * 解析 BindingResult
     *
     * @param bindingResult
     * @return
     */
    public static String parseBindingResult(BindingResult bindingResult) {
        List<String> errorList = new ArrayList<String>();
        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            errorList.add(fieldError.getField() + Symbol.COLON + fieldError.getDefaultMessage());
        }
        return errorList.toString();
    }

}
