package com.qiaoyatao.sqlplus.system.core;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.util.Random;

@Slf4j
public class UploadUtil {

    /**
     * 上传图片
     * @param path
     * @param file
     * @return
     */
    public static String uploadImg(String path, MultipartFile file) {
        String fileName = file.getOriginalFilename();//获取文件名加后缀
        if (fileName != null && fileName.length()!=0) {
            String fileF = fileName.substring(fileName.lastIndexOf("."), fileName.length());//文件后缀
            fileName = System.currentTimeMillis() + "_" + new Random().nextInt(1000) + fileF;//新的文件名
            File f = new File(path);
            if (!f.exists() && !f.isDirectory()) {
                f.mkdirs();
            }
            try {
                File targetFile = new File(f, fileName);
                //将上传的文件写到服务器上指定的文件。
                file.transferTo(targetFile);
                return targetFile.getPath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
