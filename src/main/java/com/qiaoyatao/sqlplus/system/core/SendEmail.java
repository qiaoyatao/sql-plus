package com.qiaoyatao.sqlplus.system.core;

import lombok.Data;
import org.apache.commons.mail.EmailAttachment;

@Data
public class SendEmail {

    //用户名
    private String userName;

    //密码
    private String password;

    //设置邮件主题
    private String subject;

    //发送人
    private String name;

    //邮件内容
    private String htmlMsg;

    //发送附近
    private EmailAttachment attach;

    //接收人
    private String[] toList;

    //抄送人
    private String[] ccList;
}
