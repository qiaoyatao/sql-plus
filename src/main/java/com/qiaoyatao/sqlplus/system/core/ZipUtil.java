package com.qiaoyatao.sqlplus.system.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * 压缩
 */
public class ZipUtil {

    private static final int BUFFER_SIZE = 20 * 1024;
    private static int BUFFERSIZE = 2 << 10;

    /**
     * 压缩
     * @param srcDir 源文件
     * @param endUrl 压缩成的文件
     * @param k 是否保留原来的目录结构,true:保留目录结构;
     * @throws Exception
     */
    public static void toZip(String srcDir, String endUrl, boolean k)
            throws Exception {
        File file = new File(endUrl);
        if (file.isFile()) {
            file.delete();
        }
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(new File(endUrl)));
        File sourceFile = new File(srcDir);
        compress(sourceFile, zos, sourceFile.getName(), k);
        zos.close();
    }


    /**
     * 压缩成ZIP 方法2
     *
     * @param srcFiles 需要压缩的文件列表
     * @throws RuntimeException 压缩失败会抛出运行时异常
     */
    public static void toZip(List<File> srcFiles, String endUrl)
            throws Exception {
        ZipOutputStream zos =
                new ZipOutputStream(new FileOutputStream(new File(endUrl)));
        for (File srcFile : srcFiles) {
            byte[] buf = new byte[BUFFER_SIZE];
            zos.putNextEntry(new ZipEntry(srcFile.getName()));
            int len;
            FileInputStream in = new FileInputStream(srcFile);
            while ((len = in.read(buf)) != -1) {
                zos.write(buf, 0, len);
            }
            zos.closeEntry();
            in.close();
        }
        zos.close();
    }


    /**
     * 递归压缩方法
     *
     * @param sourceFile 源文件
     * @param zos        zip输出流
     * @param name       压缩后的名称
     * @param k          是否保留原来的目录结构,true:保留目录结构;
     *                   false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
     * @throws Exception
     */
    private static void compress(File sourceFile, ZipOutputStream zos, String name,
                                 boolean k) throws Exception {
        byte[] buf = new byte[BUFFER_SIZE];
        if (sourceFile.isFile()) {
            // 向zip输出流中添加一个zip实体，构造器中name为zip实体的文件的名字
            zos.putNextEntry(new ZipEntry(name));
            // copy文件到zip输出流中
            int len;
            FileInputStream in = new FileInputStream(sourceFile);
            while ((len = in.read(buf)) != -1) {
                zos.write(buf, 0, len);
            }
            zos.closeEntry();
            in.close();
        } else {
            File[] listFiles = sourceFile.listFiles();
            if (listFiles == null || listFiles.length == 0) {
                // 需要保留原来的文件结构时,需要对空文件夹进行处理
                if (k) {
                    // 空文件夹的处理
                    zos.putNextEntry(new ZipEntry(name + "/"));
                    // 没有文件，不需要文件的copy
                    zos.closeEntry();
                }
            } else {
                for (File file : listFiles) {
                    // 判断是否需要保留原来的文件结构
                    if (k) {
                        // 注意：file.getName()前面需要带上父文件夹的名字加一斜杠,
                        // 不然最后压缩包中就不能保留原来的文件结构,即：所有文件都跑到压缩包根目录下了
                        compress(file, zos, name + "/" + file.getName(), k);
                    } else {
                        compress(file, zos, file.getName(), k);
                    }
                }
            }
        }
    }


    /**
     * 压缩
     *
     * @param paths
     * @param fileName
     */
    public static void zip(String[] paths, String fileName)
            throws Exception {
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(fileName));
        for (String filePath : paths) {
            //递归压缩文件
            File file = new File(filePath);
            String relativePath = file.getName();
            if (file.isDirectory()) {
                relativePath += File.separator;
            }
            zipFile(file, relativePath, zos);
        }
        zos.close();
    }

    public static void zipFile(File file, String relativePath, ZipOutputStream zos)
            throws Exception {
        if (!file.isDirectory()) {
            ZipEntry zp = new ZipEntry(relativePath);
            zos.putNextEntry(zp);
            InputStream is = new FileInputStream(file);
            byte[] buffer = new byte[BUFFERSIZE];
            int length = 0;
            while ((length = is.read(buffer)) >= 0) {
                zos.write(buffer, 0, length);
            }
            zos.flush();
            zos.closeEntry();
        } else {
            String tempPath = null;
            for (File f : file.listFiles()) {
                tempPath = relativePath + f.getName();
                if (f.isDirectory()) {
                    tempPath += File.separator;
                }
                zipFile(f, tempPath, zos);
            }
        }
        zos.close();
    }


    /**
     * 解压缩
     *
     * @param fileName
     * @param path
     */
    public static void unzip(String fileName, String path)
            throws Exception {
        FileOutputStream fos = null;
        InputStream is = null;
        ZipFile zf = new ZipFile(new File(fileName));
        Enumeration en = zf.entries();
        while (en.hasMoreElements()) {
            ZipEntry zn = (ZipEntry) en.nextElement();
            if (!zn.isDirectory()) {
                is = zf.getInputStream(zn);
                File f = new File(path + zn.getName());
                File file = f.getParentFile();
                file.mkdirs();
                fos = new FileOutputStream(path + zn.getName());
                int len = 0;
                byte bufer[] = new byte[BUFFERSIZE];
                while (-1 != (len = is.read(bufer))) {
                    fos.write(bufer, 0, len);
                }
                fos.close();
            }
        }
        is.close();
        fos.close();
    }

    /**
     * 删除文件夹
     * @param dir
     * @return
     */
    public static boolean deletedDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deletedDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}
