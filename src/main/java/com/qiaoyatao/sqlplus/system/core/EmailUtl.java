package com.qiaoyatao.sqlplus.system.core;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class EmailUtl {

    /**
     * 发送邮件
     * @param HtmlMsg 发送内容
     * @param toList 接收人
     * @param ccList 抄送人
     */
    public static void sendQQ(String HtmlMsg,String[] toList,String[] ccList) {
        EmailAttachment attach = new EmailAttachment();//附件对象
        attach.setPath("C:/Users/17619/Desktop/disc/测试.txt");
        SendEmail email = new SendEmail();
        email.setUserName("2521542324@qq.com");
        email.setPassword("cxebwzugfoumecge");
        email.setName("我是老王我骄傲！");
        email.setHtmlMsg(HtmlMsg);
        email.setSubject("将进酒");
        email.setAttach(attach);
        email.setToList(toList);
        email.setCcList(ccList);
        sendQQEmail(email);
    }
    /**
     * 发送邮件
     * @param dto
     */
    public static void sendQQEmail(SendEmail dto) {
        try {
            HtmlEmail email = new HtmlEmail();//创建电子邮件对象
            email.setSSL(true);
            email.setDebug(true);
            email.setHostName("SMTP.qq.com");//设置发送电子邮件使用的服务器主机名
            email.setSmtpPort(587);//设置发送电子邮件使用的邮件服务器的TCP端口地址
            email.setAuthenticator(new DefaultAuthenticator(dto.getUserName(),
                    dto.getPassword()));//邮件服务器身份验证
            email.setFrom(dto.getUserName(), dto.getName(), "utf-8");//设置发信人邮箱
            email.setSubject(dto.getSubject());//设置邮件主题
            email.setCharset("utf-8");
            email.setSSLOnConnect(false);
            email.setHtmlMsg(dto.getHtmlMsg());
            EmailAttachment attach = dto.getAttach();
            if (attach!=null){
                attach.setDescription(EmailAttachment.ATTACHMENT);
                email.attach(attach);//添加附件
            }
            if (dto.getToList()==null){
                return;
            }
            for (String mail : dto.getToList()) {
                email.addTo(mail);
            }
            if (dto.getCcList()!=null){
                for (String cc : dto.getCcList()) {
                    email.addCc(cc);
                }
            }
            email.send();//发送邮件
        } catch (EmailException e1) {
            e1.printStackTrace();
        }
    }
}
