package com.qiaoyatao.sqlplus.system.core;

import lombok.Data;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Data
public class ExcelDTO {

    //文件名称
    private String filename;
    //输出流
    private HttpServletResponse resp;
    //需要的字段
    private String[] titles;
    //输出数据
    private List<Object[]> list;
    //列表数据
    private List data;
}
