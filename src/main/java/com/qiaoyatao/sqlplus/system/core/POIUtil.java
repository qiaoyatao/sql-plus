package com.qiaoyatao.sqlplus.system.core;

import com.qiaoyatao.sqlplus.annotation.bean.AsName;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class POIUtil {
    /**
     * 获取行
     *
     * @param start   开始行
     * @param sheetAt 单元格
     * @param file    excel文件
     * @return
     */
    public static List<HSSFRow> getRow(int start, int sheetAt,
                                       MultipartFile file) {
        try {
            if (file == null) {
                return null;
            }
            List<HSSFRow> list = new ArrayList<>();
            BufferedInputStream bf = new BufferedInputStream(file.getInputStream());
            HSSFWorkbook workbook = new HSSFWorkbook(new POIFSFileSystem(bf));
            HSSFSheet sheet = workbook.getSheetAt(sheetAt);
            for (int i = start; i <= sheet.getLastRowNum(); i++) {
                HSSFRow row = sheet.getRow(i);
                if (row == null) {
                    break;
                }
                list.add(row);
            }
            bf.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取每列数据
     *
     * @param rows
     * @param clazz
     * @return
     */
    public static <T> List<T> getCellList(List<HSSFRow> rows, Class<T> clazz) {
        if (rows == null) {
            return null;
        }
        List<T> list = new ArrayList<>();
        rows.forEach(row -> {
            list.add(getCell(row, clazz));
        });
        return list;
    }

    /**
     * 获取一列数据
     *
     * @param row
     * @param clazz
     * @return
     */
    public static <T> T getCell(HSSFRow row, Class<T> clazz) {
        try {
            if (row == null) {
                return null;
            }
            Object model = clazz.newInstance();
            Field[] fields = model.getClass().getDeclaredFields();
            for (int i = 0; i < row.getLastCellNum(); i++) {
                if (fields.length == i) {
                    break;
                }
                Field field = (Field) Array.get(fields, i);
                field.setAccessible(true);
                if (row.getCell(i) != null) {
//                    row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
                    field.set(model, row.getCell(i).getStringCellValue());
                }
            }
            return (T) model;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //下载文件
    public static void downExcel(String path, String filename,
                                 HttpServletResponse resp) {
        File file = new File(path + "/" + filename);
        log.info("开始下载" + file.getPath());
        if (file.exists()) {
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                resp.setHeader("content-type", "application/octet-stream");
                resp.setContentType("application/octet-stream");
                // 下载文件能正常显示中文
                resp.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
                byte[] buffer = new byte[1024];
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = resp.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                log.info("导出模板完毕！");
                try {
                    if (bis != null) {
                        bis.close();
                    }
                    if (fis != null) {
                        fis.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //导出
    public static void createExcel(ExcelDTO dto) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(dto.getFilename());
        HSSFRow row0 = sheet.createRow(0);
        row0.setHeightInPoints(30);
        HSSFCell cell = row0.createCell(0);
        cell.setCellValue(dto.getFilename());
        HSSFCellStyle style = workbook.createCellStyle();
//        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);//水平居中
//        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);//垂直居中
        style.setWrapText(true);//自动换行
        cell.setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, dto.getTitles().length - 1));
        HSSFRow row = sheet.createRow(1);
        for (int i = 0; i < dto.getTitles().length; i++) {
            row.createCell(i).setCellValue(Array.get(dto.getTitles(), i).toString());
        }
        row.setHeightInPoints(20); // 设置行的高度
        AtomicReference<Integer> index = new AtomicReference<>(2);
        dto.getList().forEach(e -> {
            HSSFRow row1 = sheet.createRow(index.get());
            for (int i = 0; i < e.length; i++) {
                row1.createCell(i).setCellValue(Array.get(e, i).toString() == "#" ? null : Array.get(e, i).toString());
            }
            index.getAndSet(index.get() + 1);
        });
        workbook.setActiveSheet(0);
        try {
            HttpServletResponse resp = dto.getResp();
            resp.setHeader("content-type", "application/octet-stream");
            // 下载文件能正常显示中文
            resp.setContentType("application/octet-stream");
            resp.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(dto.getFilename() + ".xls", "UTF-8"));
            OutputStream os = resp.getOutputStream();
            workbook.write(os);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //反射设置参数
    public static void setExcel(ExcelDTO dto) {
        List<Object[]> data = new ArrayList<>();
        for (Object p : dto.getData()) {
            List<Object> array = new ArrayList<>();
            Class<? extends Object> clazz = p.getClass();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                try {
                    AsName asName = field.getAnnotation(AsName.class);
                    if (asName == null) {
                        continue;
                    }
                    boolean search = search(dto.getTitles(), asName.value());
                    if (search) {
                        if (field.getType().getName().contains("Date")) {
                            array.add(field.get(p) == null ? "#" : StrUtil.formatDate(field.get(p)));
                            continue;
                        }
                        array.add(field.get(p) == null ? "#" : field.get(p).toString());
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            data.add(array.toArray());
        }
        dto.setList(data);
        createExcel(dto);
    }

    //查询
    public static boolean search(String[] array, String searchValue) {
        if(array==null){return false;}
        for (String key : Arrays.asList(array)) {
            if (key.toLowerCase().equals(searchValue.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
/*
    String [] titles={"编号","项目编码","项目名称","部门名称","密级","项目类型","项目改造规模","维修内容","项目负责人","项目地址",
            "项目总投资"};
    ExcelDTO ex = new ExcelDTO();
		ex.setFilename("投资计划项目信息");
		ex.setTitles(titles);
		ex.setResp(resp);
		if (info.getList().size()==0){
        throw  new RuntimeException("无数据不可导出！");
    }
		ex.setData(info.getList());
		GenTableUtil.setExcel(ex);*/
}
