package com.qiaoyatao.sqlplus.system.crud;

import com.qiaoyatao.sqlplus.annotation.bean.Delete;
import com.qiaoyatao.sqlplus.annotation.model.ID;
import com.qiaoyatao.sqlplus.constants.SqlSymbol;
import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.mapper.BaseMapper;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import org.springframework.stereotype.Component;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

@Component
public class CrudMapper {

    public void update(BaseMapper mapper, Object param) {
        Object idv = idv(param);
        List<Object> list = mapper.selectList(param);
        if (idv != null && !idv.equals(0)) {
            Object one = mapper.selectById((Serializable) idv);
            if (one == null) {
                throw new RuntimeException(SqlSymbol.NULL);
            }
            if (list.size() > 0) {
                if (!idv.equals(idv(list.get(0)))) {
                    throw new RuntimeException(SqlSymbol.REPEAT);
                }
            }
            mapper.updateById(param);
            return;
        }
        if (list.size() > 0) {
            throw new RuntimeException(SqlSymbol.REPEAT);
        }
        mapper.insert(param);
    }

    private static Object idv(Object param) {
        for (Field field : SqlUtil.superField(param.getClass())) {
            ID id = field.getAnnotation(ID.class);
            if (id != null) {
                return SqlUtil.get(field,param);
            }
            if (field.getName().equals(Symbol.ID.trim())) {
               return SqlUtil.get(field,param);
            }
        }
        return null;
    }

    public void delete(BaseMapper mapper, Serializable id) {
        Object one = mapper.selectById(id);
        if (one == null) {
            throw new RuntimeException(SqlSymbol.NULL);
        }
        for (Field field : SqlUtil.superField(one.getClass())) {
            field.setAccessible(true);
            Delete delete = field.getAnnotation(Delete.class);
            if (delete != null) {
                SqlUtil.set(field,one,new Integer(0).equals(SqlUtil.get(field,one)) ? 1 : 0);
                mapper.updateById(one);
                return;
            }
        }
    }
}
