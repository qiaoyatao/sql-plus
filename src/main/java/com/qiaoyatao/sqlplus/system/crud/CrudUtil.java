package com.qiaoyatao.sqlplus.system.crud;

import com.qiaoyatao.sqlplus.annotation.bean.*;
import com.qiaoyatao.sqlplus.constants.SqlSymbol;
import com.qiaoyatao.sqlplus.core.Model;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.core.page.*;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;

/**
 * 封装crud工具，单模块无须手写业务逻辑
 * author： qiaoyatao
 * date: 2019年12月11日11:37:52
 */
public class CrudUtil {

    public static <T> void update(Object model) {
        List<Object> update = updateQuery(model);
        List<Model> list = ((Model) update.get(0)).selectList();
        String id = SqlUtil.getIdv(model);
        Model one = (Model) ((Model) model).selectByid(id);
        if (one != null && id != null) {
            if (list.size() != 0) {
                if (!id.equals(SqlUtil.getIdv((Model) list.get(0)))) {
                    throw new RuntimeException(update.get(1).toString() + SqlSymbol.REPEAT);
                }
            }
            ((Model) model).updateById(id);
            return;
        }
        if (list.size() > 0) {
            throw new RuntimeException(update.get(1).toString() + SqlSymbol.REPEAT);
        }
        ((Model) model).insert();
    }

    //id删除
    public static boolean delete(Serializable id, Class model) {
        Model one = (Model) ((Model) SqlUtil.newInstance(model)).selectByid(id);
        if (one == null) {
            throw new RuntimeException(SqlSymbol.NULL);
        }
        boolean b = false;
        for (Field field : SqlUtil.superField(one.getClass())) {
            field.setAccessible(true);
            Delete delete = field.getAnnotation(Delete.class);
            if (delete != null) {
                SqlUtil.set(field,one, new Integer(0).equals(SqlUtil.get(field,one)) ? 1 : 0);
                one.updateById(id);
                b = true;
                return true;
            }
            SqlUtil.set(field,one, null);
        }
        if (!b) {
            throw new RuntimeException(SqlSymbol.DELETE);
        }
        return false;
    }

    //修改状态
    public static boolean status(Serializable id, Class model) {
        Model one = (Model) ((Model) SqlUtil.newInstance(model)).selectByid(id);
        if (one == null) {
            throw new RuntimeException(SqlSymbol.NULL);
        }
        boolean b = false;
        for (Field field : SqlUtil.superField(one.getClass())) {
            field.setAccessible(true);
            Status status = field.getAnnotation(Status.class);
            if (status != null) {
                SqlUtil.set(field,one, new Integer(0).equals(SqlUtil.get(field,one)) ? 1 : 0);
                one.updateById(id);
                b = true;
                return true;
            }
            //不更新del之前的字段
            SqlUtil.set(field,one, null);
        }
        if (!b) {
            throw new RuntimeException(SqlSymbol.STATUS);
        }
        return false;
    }

    //id查询
    public static <T> T queryById(Serializable id, Class<T> model) {
        return (T) (Model) ((Model) SqlUtil.newInstance(model)).selectByid(id);
    }

    //列表查询
    public static <T> List<T> queryList(Object param, Class model, Class<T> result) {
        List list = ((Model) getInstance(param, model)).selectList();
        List<T> results = new ArrayList<>();
        list.forEach(d -> {
            results.add(BeanUtil.copyBean(d, result));
        });
        return results;
    }

    //分页查询
    public static <T> PageInfo<T> queryPage(Object param, Class model, Class<T> result) {
        PageInfo info = ((Model) getInstance(param, model)).selectPage((Page) param);
        if (info.getList().size() != 0) {
            List<T> list = new ArrayList<>();
            info.getList().forEach(d -> {
                list.add(BeanUtil.copyBean(d, result));
            });
            info.setList(list);
        }
        return info;
    }

    //获取查询条件
    private static <T> T getInstance(Object param, Class model) {
        Object newParam = SqlUtil.newInstance(model);
        Class<?> tClass = SqlUtil.newInstance(model).getClass();
        for (Field field : SqlUtil.superField(tClass)) {
            field.setAccessible(true);
            Delete delete = field.getAnnotation(Delete.class);
            if (delete == null) {
                continue;
            }
            field.setAccessible(true);
            SqlUtil.set(field,newParam, 0);
            break;
        }
        try {
            for (Field field : param.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    Field newField = tClass.getDeclaredField(field.getName());
                    newField.setAccessible(true);
                    newField.set(newParam, field.get(param));
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //获取@update注解值
    private static List<Object> updateQuery(Object model) {
        StringBuffer errro = new StringBuffer();
        Class<?> aClass = model.getClass();
        Object newInstance = SqlUtil.newInstance(aClass);
        for (Field field : SqlUtil.superField(aClass)) {
            field.setAccessible(true);
            Update update = field.getAnnotation(Update.class);
            if (update != null) {
                field.setAccessible(true);
                SqlUtil.set(field,newInstance, SqlUtil.get(field, model));
                errro.append(field.getName() + ",");
            }
        }
        if (errro.toString().length() == 0) {
            throw new RuntimeException(SqlSymbol.UPDATE);
        }
        return Arrays.asList(newInstance, errro);
    }
}
