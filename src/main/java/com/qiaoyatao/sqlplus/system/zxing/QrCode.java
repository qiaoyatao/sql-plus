package com.qiaoyatao.sqlplus.system.zxing;

import com.google.zxing.*;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Objects;
import java.util.Random;
/**
 * 二维码工具
 * author： qiaoyatao
 * date: 2020年2月24日11:45:34
 */
public class QrCode {

    private static final String CHARSET = "utf-8";

    private static final String FORMAT_NAME = "JPG";

    private static BufferedImage create(ZxingCF cf) throws Exception {
        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(cf.getContent(),
                BarcodeFormat.QR_CODE, cf.getQrcode_size(), cf.getQrcode_size(), hints);
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000
                        : 0xFFFFFFFF);
            }
        }
        if (cf.getLogoUrl() == null || cf.getLogoUrl().length() == 0) {
            return image;
        }
        // 插入图片
        QrCode.insert(image, cf);
        return image;
    }

    //插入LOGO
    private static void insert(BufferedImage source, ZxingCF cf) throws Exception {
        File file = new File(cf.getLogoUrl());
        if (!file.exists()) {
            return;
        }
        Image src = ImageIO.read(new File(cf.getLogoUrl()));
        int width = src.getWidth(null);
        int height = src.getHeight(null);
        if (cf.isZip()) { // 压缩LOGO
            if (width > cf.getWidth()) {
                width = cf.getWidth();
            }
            if (height > cf.getHeight()) {
                height = cf.getHeight();
            }
            Image image = src.getScaledInstance(width, height,
                    Image.SCALE_SMOOTH);
            BufferedImage tag = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null); // 绘制缩小后的图
            g.dispose();
            src = image;
        }
        // 插入LOGO
        Graphics2D graph = source.createGraphics();
        int x = (cf.getQrcode_size() - width) / 2;
        int y = (cf.getQrcode_size() - height) / 2;
        graph.drawImage(src, x, y, width, height, null);
        Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
        graph.setStroke(new BasicStroke(3f));
        graph.draw(shape);
        graph.dispose();
    }

    //设置背景图片
    private static BufferedImage drawImage(ZxingCF cf, BufferedImage zxing) throws IOException {
        //读取背景图的图片流
        //Try-with-resources 资源自动关闭,会自动调用close()方法关闭资源,只限于实现Closeable或AutoCloseable接口的类
        try (InputStream imagein = new FileInputStream(cf.getBackgroundUrl())) {
            return drawImage(cf, ImageIO.read(imagein), zxing);
        }
    }

    /**
     * 将图片绘制在背景图上
     */
    private static BufferedImage drawImage(ZxingCF cf, BufferedImage bkImage, BufferedImage zxing) throws IOException {
        Objects.requireNonNull(bkImage, ">>>>>背景图不可为空");
        Objects.requireNonNull(zxing, ">>>>>二维码不可为空");
        //二维码宽度+x不可以超过背景图的宽度,长度同理
        if ((zxing.getWidth() + cf.getBX()) > bkImage.getWidth() || (zxing.getHeight() + cf.getBY()) > bkImage.getHeight()) {
            throw new IOException(">>>>>二维码宽度+x不可以超过背景图的宽度,长度同理");
        }
        //合并图片
        Graphics2D g = bkImage.createGraphics();
        g.drawImage(zxing, cf.getBX(), cf.getBY(),
                zxing.getWidth(), zxing.getHeight(), null);
        return bkImage;
    }

    private static BufferedImage drawFont(BufferedImage image, ZxingCF cf) {
        Font font = new Font("微软雅黑", Font.PLAIN, 12);
        font = font.deriveFont(42F).deriveFont(Font.PLAIN);
        //绘制文字
        Graphics2D g = image.createGraphics();
        //设置颜色
        g.setColor(new Color(0, 0, 0));
        //消除锯齿状
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        //设置字体
        g.setFont(font);
        //绘制文字
        g.drawString(cf.getFontText(), cf.getFontX(), cf.getFontY());
        return image;
    }

    //创建文件
    private static void mkdirs(String destPath) {
        File file = new File(destPath);
        if (!file.exists() && !file.isDirectory()) {
            file.mkdirs();
        }
    }

    //生成二维码
    public static String encode(ZxingCF cf) {
        try {
            BufferedImage image = QrCode.create(cf);
            mkdirs(cf.getDestPath());
            if (cf.isBackground()) {
                image = drawImage(cf, image);
            }
            if (cf.isFont()) {
                image = drawFont(image, cf);
            }
            String file = cf.getDestPath() + "/" + System.currentTimeMillis() + "_" + new Random().nextInt(1000) + ".jpg";
            ImageIO.write(image, FORMAT_NAME, new File(file));
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //下载二维码
    public static void encodeDow(ZxingCF cf, HttpServletResponse resp) {
        try {
            BufferedImage image = QrCode.create(cf);
            mkdirs(cf.getDestPath());
            if (cf.isBackground()) {
                image = drawImage(cf, image);
            }
            if (cf.isFont()) {
                image = drawFont(image, cf);
            }
            resp.setHeader("content-type", "application/octet-stream");
            resp.setContentType("application/octet-stream");
            resp.setHeader("Content-Disposition", "attachment;filename=" +
                    URLEncoder.encode(System.currentTimeMillis() + new Random().nextInt(1000) + ".jpg", "UTF-8"));
            ServletOutputStream out = resp.getOutputStream();
            ImageIO.write(image, FORMAT_NAME, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 生成二维码(内嵌LOGO)
     * 输出流
     */
    public static void encode(ZxingCF cf, OutputStream out) throws Exception {
        BufferedImage image = QrCode.create(cf);
        mkdirs(cf.getDestPath());
        if (cf.isBackground()) {
            image = drawImage(cf, image);
        }
        if (cf.isFont()) {
            image = drawFont(image, cf);
        }
        ImageIO.write(image, FORMAT_NAME, out);
    }

    /**
     * 生成二维码(输出到页面)
     * 输出流
     */
    public static void encode(ZxingCF cf, HttpServletResponse resp) throws Exception {
        encode(cf,resp.getOutputStream());
    }

    /**
     * 解析二维码
     * @param file 二维码图片
     */
    public static String decode(File file) {
        try {
            BufferedImage image = ImageIO.read(file);
            if (image == null) {
                return null;
            }
//            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(null));
            Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
            hints.put(DecodeHintType.CHARACTER_SET, CHARSET);
            return new MultiFormatReader().decode(bitmap, hints).getText();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解析二维码
     * @param path 二维码图片地址
     */
    public static String decode(String path) {
        return QrCode.decode(new File(path));
    }
}
