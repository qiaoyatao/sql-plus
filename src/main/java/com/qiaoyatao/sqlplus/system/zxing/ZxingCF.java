package com.qiaoyatao.sqlplus.system.zxing;
import lombok.Data;
/**
 * 二维码配置类
 * author： qiaoyatao
 * date: 2020年2月24日11:45:56
 */
@Data
public class ZxingCF {

    // 二维码尺寸
    private Integer qrcode_size = 530;
    // LOGO宽度
    private Integer width = 100;
    // LOGO高度
    private Integer height = 100;
    //内容
    private String content;
    //LOGO图片地址
    private String logoUrl;
    //是否压缩
    private boolean isZip = true;
    //是否背景图片
    private boolean background = false;
    //背景图片地址
    private String backgroundUrl;
    // LOGO宽度
    private Integer bX = 272;
    // LOGO高度
    private Integer bY = 352;
    //输出路径
    private String destPath;
    //是否添加文字
    private boolean font = false;
    //文字内容
    private String fontText;
    // 字体位置
    private Integer fontX = 310;
    // 字体位置
    private Integer fontY = 1050;

}
