package com.qiaoyatao.sqlplus.system.bean;

import com.qiaoyatao.sqlplus.annotation.bean.AsName;
import com.qiaoyatao.sqlplus.core.Model;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.system.valid.Validation;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qiaoyatao.sqlplus.constants.Symbol;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;

/**
 * 操作对象工具
 * author： qiaoyatao
 * date: 2019年12月5日18:37:21
 */
public class BeanUtil {

    //list拷贝到target对象中 replace 是否替换原来值
    public static List copyToList(List list, Class target,boolean replace) {
        List result = new ArrayList<>();
        list.forEach(d -> {
            result.add(copyBean(d, target,replace));
        });
        return result;
    }

    //list拷贝到target对象中 replace 是否替换原来值
    public static List copyToList(List list, Class target) {
        return copyToList(list,target,false);
    }

    //list拷贝到target对象中
    public static List copyToList(List list, Object target) {
        return copyToList(list,target,false);
    }

    //list拷贝到target对象中
    public static List copyToList(List list, Object target,boolean replace) {
        List result = new ArrayList<>();
        list.forEach(d -> {
            result.add(copyAsNameParam(d, target));
        });
        return result;
    }

    /**
     * 1、通过注解实现不同名称字段拷贝
     * source拷贝到target对象中
     * @param source 原对象
     * @param target 被拷贝对象
     */
    public static void copyAsName(Object source, Object target) {
        copyAsName(source,target,false);
    }

    /**
     * 1、通过注解实现不同名称字段拷贝
     * source拷贝到target对象中 replace 是否替换原值（如果新值没有，原值不动）
     * @param source 原对象
     * @param target 被拷贝对象
     */
    public static void copyAsName(Object source, Object target,boolean replace) {
        if (source==null || target==null){return;}
        set(source.getClass(), source, target,replace);
        Class<?> superClass = source.getClass().getSuperclass();
        if (!superClass.equals(Object.class) && !superClass.equals(Model.class)) {
            set(superClass, source, target,replace);
        }
    }

    public static Object copyAsNameParam(Object source, Object param) {
        return copyAsNameParam(source,param,false);
    }

    public static Object copyAsNameParam(Object source, Object param,boolean replace) {
        if (source==null || param==null){return null;}
        Object newInstance = SqlUtil.newInstance(param.getClass());
        copyAsName(param,newInstance,replace);
        copyAsName(source,newInstance,replace);
        return newInstance;
    }

    private static void set(Class model, Object source, Object target,boolean replace) {
        Arrays.asList(SqlUtil.superField(target.getClass())).forEach(field->{
            field.setAccessible(true);
            AsName asName = field.getAnnotation(AsName.class);
            if (Validation.validType(field.getType())) {
                return;
            }
            try {
                Field newFile = model.getDeclaredField(asName != null ? asName.value() : field.getName());
                if (newFile != null) {
                    newFile.setAccessible(true);
                    if (replace && newFile.get(source)!=null){ // 是否替换原值
                        field.set(target, newFile.get(source));
                        return;
                    }
                    field.set(target, field.get(target) == null ? newFile.get(source) : field.get(target));
                    return;
                }
            } catch (Exception e) {}
        });
    }

    /**
     * 2、对象直接拷贝 target 不需要创建
     *  source拷贝到target对象中
     */
    public static <T> T copyBean(Object source, Class<T> target) {
        return copyBean(source,target,false);
    }

    /**
     * 2、对象直接拷贝 target 不需要创建
     *  source拷贝到target对象中
     *  replace 是否替换原值
     */
    public static <T> T copyBean(Object source, Class<T> target,boolean replace) {
        if (source==null || target==null){return null;}
        Object newInstance = SqlUtil.newInstance(target);
        copyAsName(source,newInstance,replace);
        return (T)newInstance;
    }

    //3、获取json请求参数
    public static String toParam(Class model) {
        if (model==null){return null;}
        Object newModel = SqlUtil.newInstance(model);
        Field[] fields = SqlUtil.superField(newModel.getClass());//数据不可为一个死循环(继承时有可能发生)
        try {
            for (Field field : fields) {
                setFieldObj(field, newModel);
                setField(field, newModel);
            }
            return new ObjectMapper().writeValueAsString(newModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void setFieldObj(Field field, Object param) throws Exception {
        field.setAccessible(true);
        if (Validation.validType(field.getType())) {
            Class<?> type = (Class<?>) field.getGenericType();
            Object o = type.newInstance();
            for (Field newField : SqlUtil.superField(o.getClass())) {
                setFieldObj(newField, o);
                setField(newField, o);
            }
            field.set(param, o);
            return;
        }
        if (field.getType().equals(List.class)) {
            Class model = (Class) ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];
            List<Object> list = new ArrayList<>();
            list.add(model.newInstance());
            for (Object p : list) {
                for (Field newField : SqlUtil.superField(p.getClass())) {
                    setFieldObj(newField, p);
                    setField(newField, p);
                }
            }
            field.set(param, list);
            return;
        }
        if (field.getType().equals(Map.class) || field.getType().equals(HashMap.class)) {
            field.set(param, setMapVal((Class) ((ParameterizedType) field.getGenericType())
                    .getActualTypeArguments()[1]));
            return;
        }
    }

    private static HashMap setMapVal(Class type) {
        HashMap<String, Object> result = new HashMap<>();
        if (type.equals(Integer.class)) {
            result.put(Symbol.KEY, 0);
            return result;
        }
        if (type.equals(Double.class)) {
            result.put(Symbol.KEY, 0d);
            return result;
        }
        if (type.equals(Float.class)) {
            result.put(Symbol.KEY, 0f);
            return result;
        }
        if (type.equals(String.class)) {
            result.put(Symbol.KEY, "");
            return result;
        }
        result.put(Symbol.KEY, null);
        return result;
    }

    private static void setField(Field field, Object in) throws Exception {
        field.setAccessible(true);
        if (field.getType().equals(String.class)) {
            field.set(in, "");
            return;
        }
        if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) {
            field.set(in, 0);
            return;
        }
        if (field.getType().equals(Date.class)) {
            field.set(in, new Date());
            return;
        }
    }

    //对象转map
    public static <T> Map<String, Object> beanToMap(T model) {
        if (model==null){return null;}
        Map<String, Object> result = new HashMap<>();
        Arrays.asList(SqlUtil.superField(model.getClass())).forEach(field->{
            if (field.getName().equals(Symbol.SERIALIZABLE)){
                return;
            }
            Object value = SqlUtil.get(field,model);
            if (value != null && value.toString().length() != 0) {
                result.put(field.getName(), value);
            }
        });
        return result;
    }

    //对象(字段转下划线)转map
    public static <T> Map<String, Object> toSymbolCase(T model) {
        if (model==null){return null;}
        Map<String, Object> result = new HashMap<>();
        Arrays.asList(SqlUtil.superField(model.getClass())).forEach(field->{
            if (field.getName().equals(Symbol.SERIALIZABLE)){
                return;
            }
            Object value =  SqlUtil.get(field,model);
            if (value != null && value.toString().length() != 0) {
                result.put(StrUtil.toSymbolCase(field.getName()), value);
            }
        });
        return result;
    }

    //map转对象
    public static Object toBean(Map<String, Object> map, Object param) {
        if (map==null || param==null){ return null;}
        Object model = SqlUtil.newInstance(param.getClass());
        copyAsName(param,model);
        Arrays.asList(SqlUtil.superField(model.getClass())).forEach(field->{
            field.setAccessible(true);
            if (field.getName().equals(Symbol.SERIALIZABLE)){
                return;
            }
            if (SqlUtil.get(field,model)==null || SqlUtil.get(field,model).toString().length()==0){
                SqlUtil.set(field,model, map.get(StrUtil.toSymbolCase(field.getName()))==null
                        ?map.get(StrUtil.toCamelCase(field.getName()))
                        :map.get(StrUtil.toSymbolCase(field.getName())));

            }
        });
        return model;
    }

    //map转对象
    public static <R> R toBean(Map<String, Object> map, Class param) {
        if (map==null){return null;}
        Object model = SqlUtil.newInstance(param);
        Arrays.asList(SqlUtil.superField(model.getClass())).forEach(field->{
            field.setAccessible(true);
            SqlUtil.set(field,model, map.get(StrUtil.toSymbolCase(field.getName()))==null
                    ?map.get(StrUtil.toCamelCase(field.getName()))
                    :map.get(StrUtil.toSymbolCase(field.getName())));
        });
        return (R)model;
    }
}
