package com.qiaoyatao.sqlplus.system.bean;

import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class StrUtil {

    // 转下划线
    public static String toSymbolCase(CharSequence str) {
        if (str == null) {
            return null;
        }
        char symbol = '_';
        int length = str.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; ++i) {
            char c = str.charAt(i);
            Character preChar = i > 0 ? str.charAt(i - 1) : null;
            if (Character.isUpperCase(c)) {
                Character nextChar = i < str.length() - 1 ? str.charAt(i + 1) : null;
                if (null != preChar && Character.isUpperCase(preChar)) {
                    sb.append(c);
                } else if (null != nextChar && Character.isUpperCase(nextChar)) {
                    if (null != preChar && symbol != preChar) {
                        sb.append(symbol);
                    }
                    sb.append(c);
                } else {
                    if (null != preChar && symbol != preChar) {
                        sb.append(symbol);
                    }
                    sb.append(Character.toLowerCase(c));
                }
            } else {
                if (sb.length() > 0 && Character.isUpperCase(sb.charAt(sb.length() - 1)) && symbol != c) {
                    sb.append(symbol);
                }
                sb.append(c);
            }
        }
        return sb.toString();
    }

    //下划线转驼峰
    public static String toCamelCase(CharSequence name) {
        if (null == name) {
            return null;
        }
        String name2 = name.toString();
        if (name2.contains("_")) {
            StringBuilder sb = new StringBuilder(name2.length());
            boolean upperCase = false;
            for (int i = 0; i < name2.length(); ++i) {
                char c = name2.charAt(i);
                if (c == '_') {
                    upperCase = true;
                } else if (upperCase) {
                    sb.append(Character.toUpperCase(c));
                    upperCase = false;
                } else {
                    sb.append(Character.toLowerCase(c));
                }
            }
            return sb.toString();
        }
        return name2;
    }

    /**
     * 生成key
     *
     * @param prefix     前缀
     * @param className  类名
     * @param methodName 方法名
     * @return prefix className.methodName
     */
    public static String genKey(String prefix, String className, String methodName) {
        return prefix + "userId_" +
                "_" +
                className +
                "." +
                methodName;
    }

    public static void clear(Object param) {
        if (param == null) {
            return;
        }
        Arrays.asList(SqlUtil.superField(param.getClass())).forEach(field -> {
            field.setAccessible(true);
            Object value = SqlUtil.get(field, param);
            if (value == null) {
                return;
            }
            if (!Symbol.SERIALIZABLE.equals(value) && value.equals(String.class.getSimpleName().toLowerCase())) {
                SqlUtil.set(field, param, null);
            }
        });
    }

    public static String append(Object... values) {
        StringBuffer sb = new StringBuffer();
        Arrays.asList(values).forEach(value -> {
            sb.append(value);
        });
        return sb.toString();
    }

    public static boolean isNull(Object model) {
        return model == null || model.toString().length() == 0 ? true : false;
    }
    public static boolean isNotNull(Object model) {
        return !isNull(model);
    }

    public static boolean isBlank(String str) {
        return str == null || str.length() == 0 ? true : false;
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    public static String formatDate(Object date) {
        return null == date ? null : new SimpleDateFormat(Symbol.PATTERN).format(date);
    }
}
