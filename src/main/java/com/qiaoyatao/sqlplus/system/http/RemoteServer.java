package com.qiaoyatao.sqlplus.system.http;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 远程调用工具
 * author： qiaoyatao
 * date: 2020年1月17日18:31:16
 */
@SuppressWarnings("all")
@Slf4j
@Component
public class RemoteServer {
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 发送json
     * @param url
     * @param param
     * @return
     */
    public String httpPost(String url, Object param) {
        log.debug("##########开始远程接口调用##########" + url);
        log.debug(String.format("call httpPost{url=%s,requestBody=%s}", url, param));
        try {
            log.debug(String.format("准备发起远程接口调用，接口地址：%s，请求报文：%s", url, param));
            ResponseEntity<String> response = restTemplate.postForEntity(url, new HttpEntity(param, new HttpHeaders() {{
                setContentType(MediaType.APPLICATION_JSON);
                set(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.toString());
            }}), String.class);
            log.debug(String.format("远程接口调用成功，响应报文：%s", response.getBody()));
            log.debug("##########远程接口调用结束##########");
            return null == response ? null : response.getBody();
        } catch (RestClientException e) {
            log.debug(String.format("生成请求报文失败 call httpPost error, url:{%s} requestBody:{%s}, cause:{%s}", url, param, e.getMessage()), e);
        }
        return null;
    }

    /**
     * 发送json 携带header
     * @param url
     * @param header
     * @param param
     * @return
     */
    public String httpPost(String url, Map<String, Object> header, Object param) {
        log.debug(String.format("call httpPost{url=%s,requestBody=%s}", url, param));
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(url, new HttpEntity(param, new HttpHeaders() {{
                setContentType(MediaType.APPLICATION_JSON);
                set(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.toString());
                if (header != null && header.size() > 0) {
                    header.forEach((k, v) -> {
                        set(k, v.toString());
                    });
                }
            }}), String.class);
            return null == response ? null : response.getBody();
        } catch (RestClientException e) {
            log.debug(String.format("call httpPost error, url:{%s} requestBody:{%s}, cause:{%s}", url, param, e.getMessage()), e);
        }
        return null;
    }

    /**
     * json发送请求
     * @param url
     * @param param
     * @return
     */
    public String httpPost(String url, Map<String, String> param) {
        try {
            log.debug(String.format("call httpPost{url=%s,requestBody=%s}", url, param.toString()));
            ResponseEntity<String> response = restTemplate.postForEntity(url, new HttpEntity(new LinkedMultiValueMap<String, String>(param.size()) {{
                param.forEach((String key, String value) -> {
                    add(key, value);
                });
            }}, new HttpHeaders() {{
                setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                set(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.toString());
            }}), String.class);
            return null == response ? null : response.getBody();
        } catch (Exception e) {
            log.debug(String.format("call httpPost error, url:{%s} requestBody:{%s}, cause:{%s}", url, param.toString(), e.getMessage()), e);
        }
        return null;
    }

    /**
     * get方式
     * @param url
     * @return
     */
    public String httpGet(String url) {
        log.debug(String.format("call httpGet{url=%s}", url));
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            return null == response ? null : response.getBody();
        } catch (RestClientException e) {
            log.debug(String.format("call httpGet error, url:{%s}, cause:{%s}", url, e.getMessage()), e);
        }
        return null;
    }

    /**
     * @param url
     * @param param
     * @return
     */
    public String httpGet(String url, Map<String, Object> param) {
        if (!param.isEmpty()){
            StringBuffer sql = new StringBuffer();
            sql.append("?");
            param.keySet().forEach(key->{
                if (StringUtils.isNotBlank(param.get(key).toString())){
                    sql.append(key+"="+param.get(key)+"&");
                }
            });
            if (sql.length()!=0){
                String data = sql.toString();
                url+=data.substring(0,data.length()-1);
            }
        }
        log.debug(String.format("call httpGet{url=%s}", url));
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        return null == response ? null : response.getBody();
    }


}
