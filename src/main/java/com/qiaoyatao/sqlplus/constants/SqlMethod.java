package com.qiaoyatao.sqlplus.constants;

public class SqlMethod {

    public static String insert = "INSERT INTO %s (%s) VALUES (%s)";

    public static String selectbyid = "SELECT %s FROM %s WHERE %s = '%s' LIMIT 1 OFFSET 0";

    public static String selectOne = "SELECT %s FROM %s %s ORDER BY %s LIMIT 1 OFFSET 0";

    public static String select = "SELECT %s FROM %s %s ORDER BY %s";

    public static String deleteById = "DELETE FROM %s WHERE %s='%s'";

    public static String deleteByIds = "DELETE FROM %s WHERE (%s in (%s))";

    public static String delete = "DELETE FROM %s WHERE %s";

    public static String where = "WHERE %s";

    public static String updateById = "UPDATE %s SET %s WHERE %s = '%s'";

    public static String update = "UPDATE %s SET %s WHERE %s";

    public static String count = "SELECT count(1) FROM %s %s";

    public static String page = "SELECT %s FROM %s %s ORDER BY %s LIMIT %s OFFSET %s";
}
