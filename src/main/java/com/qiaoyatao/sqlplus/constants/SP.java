package com.qiaoyatao.sqlplus.constants;

/**
 * SP
 * author： qiaoyatao
 * date: 2019年12月23日16:13:14
 */
public class SP {

    public static final String PUBLIC = "public class ";

    public static final String INTERFACE = "public interface ";

    public static final String PRIVATE = "    private %s%s;";

    public static final String JAVA = ".java";

    public static final String XML_MAPPER = ".xml";

    public static final String JSON = ".json";

    public static final String EXTENDS = " extends ";

    public static final String IMPLEMENTS = " implements ";

    public static final String PACKAGE = "package ";

    public static final String MODEL = "Model";

    public static final String APIMODELPROPERTY = "    @ApiModelProperty(value = \"%s\")";

    public static final String TABLEFIELD = "    @TableField(value = \"%s\")";

    public static final String SHOW_FULL = "show full columns from %s";

    public static final String SELECT = "select * from ";

    public static final String NAMESPACE = "<mapper namespace=";

    public static final String MAPPER = "</mapper>";

    public static final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    public static final String DOCTYPE = "<!DOCTYPE mapper PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\"\n" +
            "        \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">";
    public static final String SQL = "    <sql id=\"Base_Column_List\">";

    public static final String SQLTO = "    </sql>";

    public static final String BASE = "    <!-- 通用查询结果列 -->";

    public static final String BASER = "    <!-- 通用返回结果 -->";

    public static final String REMARKS = "    <!-- %s -->";

    public static final String API = "@Api(description = \"%s\")";

    public static final String KG = "        ";

    public static final String RESULTMAP = "   <resultMap id=\"BaseResultMap\" type=\"";

    public static final String RESULTMAPTO = "   </resultMap>";

    public static final String COLUMN = "\t\t<result column=\"";

    public static final String PROPERTY = "  property=\"";

    public static final String AUTHOR = " * @author: ";

    public static final String DATE = " * @date: ";

    public static final String MAPPING = "@RequestMapping(\"/api/%s\")";

    public static final String PATH = "%s/%s/%s/%s";

    public static final String[] CONFIG = {"base/SqlPlusCF","base/Constants","base/PageHelperCF",
                    "base/Swagger2CF","base/ScheduledTask","base/pom.xml",
                    "adapter/SecurityInterceptor","adapter/WebSecurityAdapter",
                    "aspect/Assert","aspect/BusinessException","aspect/R",
                    "aspect/ResponseBodyUnifiedHandler","aspect/ResultCodeEnum",
                    "aspect/ExceptionUnifiedHandler","aop/AopAspect","aop/ValidationAspect","aop/AopLogAspect"
                    ,"aop/LogAspect","aop/WebLogAspect","aop/RedisCacheAspect"
    };
}