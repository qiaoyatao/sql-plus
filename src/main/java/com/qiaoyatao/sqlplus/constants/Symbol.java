package com.qiaoyatao.sqlplus.constants;

/**
 * 符号常数
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public class Symbol {

    //1、以下为sql-plus常量
    public static final String COMMA = ",";

    public static final char COLON = '_';

    public static final String LEFTB = "(";

    public static final String RIGHTB = ")";

    public static final String leftA = "'";

    public static final String TO = "'%s'";

    public static final String TO2 = "'%s',";

    public static final String FH = ";";

    public static final String EQ = " = ";

    public static final String NOTEQ = " != ";

    public static final String NOT = " not ";

    public static final String le = " <= ";

    public static final String ge = " >= ";

    public static final String BETWEEN = " between ";

    public static final String IN = " in ( ";

    public static final String ALARM = "#";

    public static final String FROM = "FROM";

    public static final String SELECT = "SELECT";

    public static final String DELETE = "DELETE";

    public static final String UPDATE = "UPDATE";

    public static final String INTO = "INSERT";

    public static final String GT = " > ";

    public static final String LT = " < ";

    public static final String AS = " as ";

    public static final String ASNAME = "%s as %s,";

    public static final String AND = " and ";

    public static final String OR = " or ";

    public static final String LIKE = " like ";

    public static final String DESC = " desc ";

    public static final String ID = " id";

    public static final String SERIALIZABLE = "serialVersionUID";

    public static final String CREATE = "CREATE TABLE  IF NOT EXISTS ";

    public static final String PRIMARY_KEY = "PRIMARY KEY (`%s`))";

    public static final String AUTO_INCREMENT = "NOT NULL AUTO_INCREMENT ";

    public static final String ENGINE = ") ENGINE=InnoDB DEFAULT CHARSET=utf8";

    public static final String COMMENT = " COMMENT ";

    public static final String VARCHAR = " varchar(100) ";

    public static final String INT = " int(11) ";

    public static final String SMALLINT = " smallint(20) ";

    public static final String BIGINT = " bigint(20) ";

    public static final String FLOAT = " float(20) ";

    public static final String DECIMAL = " decimal(20) ";

    public static final String CHAR = " char(2) ";

    public static final String DOUBLE = " double ";

    public static final String BIT = " bit(1) ";

    public static final String DATETIME = " datetime ";

    public static final String SHOW = " SHOW TABLES LIKE '%s'";

    public static final String TRUNCATE = "truncate table %s";

    public static final String SHOW_CREATE = "show create table %s";

    public static final String DROP_TABLE = "drop table %s";

    public static final String run_time = "  RunTime: %sms";

    public static final String sqlSession = "  Session: ";

    public static final String newInsert = "newInsert";

    public static final String insertParam = "insertParam";

    public static final String insertMap = "insertMap";

    public static final String removeMap = "removeMap";

    public static final String modifyMap = "modifyMap";

    public static final String removeById = "removeById";

    public static final String removeId = "removeId";

    public static final String removeByIds = "removeByIds";

    public static final String remove = "remove";

    public static final String removeQueryModel = "removeQueryModel";

    public static final String modifyById = "modifyById";

    public static final String modifyId = "modifyId";

    public static final String modify = "modify";

    public static final String modifyQueryModel = "modifyQueryModel";

    public static final String queryById = "queryById";

    public static final String queryModelList = "queryModelList";

    public static final String queryModelLimit = "queryModelLimit";

    public static final String queryModelCount = "queryModelCount";

    public static final String queryModel = "queryModel";

    public static final String queryMapOne = "queryMapOne";

    public static final String queryLimit = "queryLimit";

    public static final String queryCount = "queryCount";

    public static final String queryList = "queryList";

    public static final String queryByModel = "queryByModel";

    public static final String queryByParam = "queryByParam";

    public static final String queryByModelList = "queryByModelList";

    public static final String queryByParamList = "queryByParamList";

    public static final String queryByParamCount = "queryByParamCount";

    public static final String queryByParamLimit = "queryByParamLimit";

    public static final String queryMapLimit = "queryMapLimit";

    public static final String queryMapCount = "queryMapCount";

    public static final String queryMapList = "queryMapList";

    //2、以下为校验和转对象常量
    public static final String MUST = "必填!";

    public static final String PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static final String TIMEZONE = "GMT+8";

    public static final String NULL = "必须为NULL";

    public static final String KEY = "key";

    public static final String PREPARING = "Preparing: %s";

    public static final String COLUMNS = "  Columns:%s";

    public static final String TOTAL = "    Total: %s";

    public static final String UPDATES = "  Updates: %s";

    public static final String NAMES = " %s,";
}
