package com.qiaoyatao.sqlplus.constants;
/**
 * 错误提示
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public class SqlSymbol {

    public static final String REPEAT ="数据重复！";

    public static final String NULL ="数据不存在！";

    public static final String DELETE ="@Delete不存在！";

    public static final String STATUS ="@Status不存在！";

    public static final String UPDATE ="添加或修改不可无去重条件！";

    public static final String UPDATEMSG ="条件必填！";
}
