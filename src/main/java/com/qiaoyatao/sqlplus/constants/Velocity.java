package com.qiaoyatao.sqlplus.constants;

/**
 * 符号常数
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public class Velocity {

    /**
     * 父类
     */
    public static final String BASE_MODEL = "baseModel";
    /**
     * 字段列表
     */
    public static final String FIELDS = "fields";
    /**
     * 类文件名
     */
    public static final String MODEL = "model";

    /**
     * 首字符小写
     */
    public static final String CAMELCASE = "camelcase";
    /**
     * 字段列表
     */
    public static final String GLOBAL = "global";
    /**
     * 日期
     */
    public static final String DATE = "date";
    /**
     * 表
     */
    public static final String TABLE = "table";
    /**
     * 备注
     */
    public static final String REMARK = "remark";
    /**
     * 是否为dto
     */
    public static final String DTO = "dto";

    /**
     * service
     */
    public static final String SERVICE = "service";

    /**
     * lists
     */
    public static final String LISTS = "lists";
    /**
     * column
     */
    public static final String COLUMN = "column";

    /**
     * 主键
     */
    public static final String PRIMARYKEY = "primarykey";
    /**
     * 实体类模板
     */
    public static final String model = "model.java.vm";
    /**
     * jpa实体类模板
     */
    public static final String entity = "entity.java.vm";
    /**
     * jpa持久层模板
     */
    public static final String dao = "dao.java.vm";
    /**
     * dto模板
     */
    public static final String dto = "dto.java.vm";
    /**
     * Controller模板
     */
    public static final String controller = "controller.java.vm";
    /**
     * 业务层模板
     */
    public static final String service = "service.java.vm";
    /**
     * 业务层实现类模板
     */
    public static final String achieve = "achieve.java.vm";
    /**
     * 持久层类模板
     */
    public static final String mapper = "mapper.java.vm";
    /**
     * 持久层xml模板
     */
    public static final String xml = "mapper-xml.vm";

    /**
     * api模板
     */
    public static final String api = "api.json.vm";

    /**
     * 配置文件
     */
    public static final String java = ".java.vm";

    /**
     * test模板
     */
    public static final String test = "test.java.vm";

}
