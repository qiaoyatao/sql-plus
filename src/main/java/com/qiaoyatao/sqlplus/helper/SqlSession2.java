package com.qiaoyatao.sqlplus.helper;

import com.qiaoyatao.sqlplus.constants.Symbol;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * sql执行器
 * author： qiaoyatao
 * date: 2019年11月16日18:39:34
 */
@Slf4j
@Component
@NoArgsConstructor
public class SqlSession2 {

    @Autowired
    private JdbcTemplate jdbc;

    private static JdbcTemplate sqlH;

    //静态调用
    @PostConstruct
    public void initialize() {
        sqlH = this.jdbc;
        sqlH.setFetchSize(10000);//提高查询效率
    }

    //注入外部数据源
    public SqlSession2(JdbcTemplate jdbc) {
        sqlH = jdbc;
    }

    public static int insert(String sql) {
        return update(sql);
    }

    public static int delete(String sql) {
        return update(sql);
    }

    public static int update(String sql) {
        log.info(sql);
        long startTime = System.currentTimeMillis();
        int update = SqlPlus.isNull(sql) ? 0 : sqlH.update(sql);
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        return update;
    }

    public static <T> T selectOne(String sql, Class<T> clazz) {
        log.info(sql);
        long startTime = System.currentTimeMillis();
        List<T> list = SqlPlus.isNull(sql) ? null : sqlH.query(sql, new BeanPropertyRowMapper(clazz));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        return list.size() != 0 ? list.get(0) : null;
    }

    public static Long count(String sql) {
        log.info(sql);
        long startTime = System.currentTimeMillis();
        Long count = SqlPlus.isNull(sql) ? 0L : sqlH.queryForObject(sql, Long.class);
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        return count;
    }

    public static <T> List<T> selectList(String sql, Class<T> clazz) {
        log.info(sql);
        long startTime = System.currentTimeMillis();
        List list = SqlPlus.isNull(sql) ? null : sqlH.query(sql, new BeanPropertyRowMapper(clazz));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        return list;
    }

    public static int batchInsert(String[] sql) {
        log.info(Arrays.toString(sql));
        long startTime = System.currentTimeMillis();
        int batchInsert = (sql == null || sql.length == 0 ? 0 : sqlH.batchUpdate(sql).length);
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        return batchInsert;
    }

    public static boolean create(String show, String create) {
        log.info(show);
        long startTime = System.currentTimeMillis();
        List<String> list = sqlH.queryForList(show, String.class);
        if (list.size() == 0) {
            sqlH.execute(create);
            log.info(create);
            long endTime = System.currentTimeMillis();
            log.info(String.format(Symbol.run_time, (endTime - startTime)));
            return true;
        }
        return false;
    }

    public static Map<String, Object> selectOne(String sql) {
        try {
            log.info(sql);
            long startTime = System.currentTimeMillis();
            Map<String, Object> result = SqlPlus.isNull(sql) ? null : sqlH.queryForMap(sql);
            long endTime = System.currentTimeMillis();
            log.info(String.format(Symbol.run_time, (endTime - startTime)));
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Map<String, Object>> selectList(String sql) {
        log.info(sql);
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> list = SqlPlus.isNull(sql) ? null : sqlH.queryForList(sql);
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        return list;
    }
}
