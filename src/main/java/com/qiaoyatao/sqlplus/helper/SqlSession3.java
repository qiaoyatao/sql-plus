package com.qiaoyatao.sqlplus.helper;

import com.qiaoyatao.sqlplus.constants.Symbol;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

/**
 * MyBatis执行sql工具
 * author： qiaoyatao
 * date: 2020年5月27日08:55:04
 */
@Slf4j
@Component
@NoArgsConstructor
public class SqlSession3 {

    @Autowired
    private SqlSessionFactory sessionFactory;

    private static SqlSessionFactory sqlH;

    //静态调用
    @PostConstruct
    public void initialize() {
        sqlH = this.sessionFactory;
    }

    //注入外部数据源
    public SqlSession3(SqlSessionFactory sessionFactory) {
        sqlH = sessionFactory;
    }

    public static int insert(String sql) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        int insert = SqlPlus.isNull(sql) ? 0
                : sqlSession.insert(new SqlPlus(sqlSession).insert(sql));
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,insert,null);
        return insert;
    }

    public static int insert(String sql, Object param) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        int insert = SqlPlus.isNull(sql) ? 0
                : sqlSession.insert(new SqlPlus(sqlSession).insertDynamic(sql,
                (param != null ? param.getClass() : null)), param);
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,insert,null);
        return insert;
    }

    public static int delete(String sql) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        int delete = SqlPlus.isNull(sql) ? 0
                : sqlSession.delete(new SqlPlus(sqlSession).delete(sql));
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,delete,null);
        return delete;
    }

    public static int delete(String sql, Object param) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        int delete = SqlPlus.isNull(sql) ? 0
                : sqlSession.delete(new SqlPlus(sqlSession).deleteDynamic(sql,
                (param != null ? param.getClass() : null)), param);
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,delete,null);
        return delete;
    }

    public static int update(String sql) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        int update = SqlPlus.isNull(sql) ? 0
                : sqlSession.update(new SqlPlus(sqlSession).update(sql));
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,update,null);
        return update;
    }

    public static int update(String sql, Object param) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        int update = SqlPlus.isNull(sql) ? 0
                : sqlSession.update(new SqlPlus(sqlSession).updateDynamic(sql,
                (param != null ? param.getClass() : null)), param);
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,update,null);
        return update;
    }

    public static <R> R selectOne(String sql, Class result) {
        List<R> list = SqlPlus.isNull(sql) ? null
                : selectList(sql, result);
        return list.isEmpty() ? null : list.get(0);
    }

    public static <R> R selectOne(String sql, Object param, Class result) {
        List<R> list = SqlPlus.isNull(sql)
                ? null : selectList(sql, param, result);
        return list.isEmpty() ? null : list.get(0);
    }

    public static Long count(String sql) {
        return SqlPlus.isNull(sql) ? 0L : selectOne(sql, Long.class);
    }

    public static <R> List<R> selectList(String sql, Class<R> result) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        List<R> list = SqlPlus.isNull(sql) ?
                null : sqlSession.selectList(new SqlPlus(sqlSession).select(sql, result));
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,list.size(),result);
        return list;
    }

    public static <R> List<R> selectList(String sql, Object param, Class<R> result) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        List<R> list = SqlPlus.isNull(sql)
                ? null : sqlSession.selectList(new SqlPlus(sqlSession).selectDynamic(sql,
                (param != null ? param.getClass() : null), result), param);
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,list.size(),result);
        return list;
    }

    public static Map<String, Object> selectOne(String sql) {
        List<Map<String, Object>> list = SqlPlus.isNull(sql)
                ? null : selectList(sql);
        return list.isEmpty() ? null : list.get(0);
    }

    public static Map<String, Object> selectOne(String sql, Object param) {
        List<Map<String, Object>> list = SqlPlus.isNull(sql) ?
                null : selectList(sql, param);
        return list.isEmpty() ? null : list.get(0);
    }

    public static List<Map<String, Object>> selectList(String sql) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> result = SqlPlus.isNull(sql) ? null :
                sqlSession.selectList(new SqlPlus(sqlSession).select(sql));
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,result.size(),Map.class);
        return result;
    }

    public static List<Map<String, Object>> selectList(String sql, Object param) {
        log.info(String.format(Symbol.PREPARING, sql));
        SqlSession sqlSession = sqlH.openSession();
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> result = SqlPlus.isNull(sql) ? null :
                sqlSession.selectList(new SqlPlus(sqlSession).selectDynamic(sql, param != null ?
                        param.getClass() : null), param);
        long endTime = System.currentTimeMillis();
        SqlUtil.outLog((endTime - startTime),sqlSession,result.size(),Map.class);
        return result;
    }
}
