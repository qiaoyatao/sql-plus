package com.qiaoyatao.sqlplus.helper;

import com.qiaoyatao.sqlplus.constants.Symbol;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * sql执行器
 * author： qiaoyatao
 * date: 2019年11月30日20:46:00
 */
@Slf4j
@Component
public class SqlHelper {

    @Autowired
    private SqlSessionFactory sessionFactory;

    private static Connection sqlH;

    //静态调用
    @PostConstruct
    public void initialize() {
        sqlH = this.sessionFactory.openSession().getConnection();
    }

    //注入外部数据源
    public SqlHelper(SqlSessionFactory sessionFactory) {
        sqlH = sessionFactory.openSession().getConnection();
    }

    private static QueryRunner r = new QueryRunner();

    public static int insert(String sql) {
        return sqlUpdate(sql);
    }

    public static int delete(String sql) {
        return sqlUpdate(sql);
    }

    public static int update(String sql) {
        return sqlUpdate(sql);
    }

    //执行更新
    private static int sqlUpdate(String sql) {
        try {
            log.info(sql);
            long startTime = System.currentTimeMillis();
            int update = (sql == null || sql.length() == 0 ? 0 : r.update(sqlH, sql));
            long endTime = System.currentTimeMillis();
            log.info(String.format(Symbol.run_time, (endTime - startTime)));
            return update;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    //查询单条数据
    public static <T> T selectOne(String sql, Class<T> clazz) {
        try {
            log.info(sql);
            long startTime = System.currentTimeMillis();
            T model = (sql == null || sql.length() == 0 ? null : r.query(sqlH, sql, new BeanHandler<>(clazz)));
            long endTime = System.currentTimeMillis();
            log.info(String.format(Symbol.run_time, (endTime - startTime)));
            return model;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //条件查询数据
    public static <T> List<T> selectList(String sql, Class<T> clazz) {
        try {
            log.info(sql);
            long startTime = System.currentTimeMillis();
            List<T> list = (sql == null || sql.length() == 0 ? null : r.query(sqlH, sql, new BeanListHandler<>(clazz)));
            long endTime = System.currentTimeMillis();
            log.info(String.format(Symbol.run_time, (endTime - startTime)));
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
