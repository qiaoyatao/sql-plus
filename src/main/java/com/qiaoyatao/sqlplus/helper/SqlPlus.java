package com.qiaoyatao.sqlplus.helper;

import org.apache.ibatis.builder.StaticSqlSource;
import org.apache.ibatis.mapping.*;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import java.util.ArrayList;
import java.util.Map;

/**
 * SqlPlus
 * author： qiaoyatao
 * date: 2020年5月27日11:20:12
 */
public class SqlPlus {

    private Configuration config;

    private LanguageDriver languageDriver;

    public SqlPlus(SqlSession sqlSession) {
        this.config = sqlSession.getConfiguration();
        languageDriver = config.getDefaultScriptingLanguageInstance();
    }

    public String insert(String sql) {
        String msId = newMsId(sql, SqlCommandType.INSERT);
        if (isHash(msId)) {
            return msId;
        }
        updateMapped(msId, new StaticSqlSource(config, sql), SqlCommandType.INSERT);
        return msId;
    }

    public String insertDynamic(String sql, Class paramType) {
        String msId = newMsId(sql + paramType, SqlCommandType.INSERT);
        if (isHash(msId)) {
            return msId;
        }
        updateMapped(msId, languageDriver.createSqlSource(config, sql, paramType)
                , SqlCommandType.INSERT);
        return msId;
    }

    public String delete(String sql) {
        String msId = newMsId(sql, SqlCommandType.DELETE);
        if (isHash(msId)) {
            return msId;
        }
        updateMapped(msId, new StaticSqlSource(config, sql), SqlCommandType.DELETE);
        return msId;
    }

    public String deleteDynamic(String sql, Class paramType) {
        String msId = newMsId(sql + paramType, SqlCommandType.DELETE);
        if (isHash(msId)) {
            return msId;
        }
        updateMapped(msId, languageDriver.createSqlSource(config, sql, paramType), SqlCommandType.DELETE);
        return msId;
    }

    public String update(String sql) {
        String msId = newMsId(sql, SqlCommandType.UPDATE);
        if (isHash(msId)) {
            return msId;
        }
        updateMapped(msId, new StaticSqlSource(config, sql), SqlCommandType.UPDATE);
        return msId;
    }

    public String updateDynamic(String sql, Class paramType) {
        String msId = newMsId(sql + paramType, SqlCommandType.UPDATE);
        if (isHash(msId)) {
            return msId;
        }
        updateMapped(msId, languageDriver.createSqlSource(config, sql, paramType), SqlCommandType.UPDATE);
        return msId;
    }

    public String select(String sql) {
        String msId = newMsId(sql, SqlCommandType.SELECT);
        if (isHash(msId)) {
            return msId;
        }
        selectMapped(msId, new StaticSqlSource(config, sql), Map.class);
        return msId;
    }

    public String selectDynamic(String sql, Class paramType) {
        String msId = newMsId(sql + paramType,
                SqlCommandType.SELECT);
        if (isHash(msId)) {
            return msId;
        }
        selectMapped(msId, languageDriver.createSqlSource(config, sql, paramType), Map.class);
        return msId;
    }

    public String select(String sql, Class result) {
        String msId = newMsId(result + sql,
                SqlCommandType.SELECT);
        if (isHash(msId)) {
            return msId;
        }
        selectMapped(msId, new StaticSqlSource(config, sql), result);
        return msId;
    }

    public String selectDynamic(String sql, Class paramType, Class result) {
        String msId = newMsId(result + sql + paramType,
                SqlCommandType.SELECT);
        if (isHash(msId)) {
            return msId;
        }
        selectMapped(msId, languageDriver.createSqlSource(config, sql, paramType), result);
        return msId;
    }

    /**
     * 创建MSID
     *
     * @param sql 执行的sql
     * @param sql 执行的sqlCommandType
     */
    private String newMsId(String sql, SqlCommandType sqlType) {
        return new StringBuilder(sqlType.toString())
                .append(".")
                .append(sql.hashCode())
                .toString();
    }

    /**
     * 是否已经存在该ID
     *
     * @param msId
     */
    private boolean isHash(String msId) {
        return config.hasStatement(msId, false);
    }

    /**
     * 创建一个查询的MS
     *
     * @param msId
     * @param sqlSource 执行的sqlSource
     * @param result    返回的结果类型
     */
    private void selectMapped(String msId, SqlSource sqlSource, final Class result) {
        MappedStatement ms = new MappedStatement.Builder(config, msId, sqlSource, SqlCommandType.SELECT)
                .resultMaps(new ArrayList<ResultMap>() {{
                    add(new ResultMap.Builder(config, "defaultResultMap", result,
                            new ArrayList<ResultMapping>(0)).build());
                }}).build();
        //加入缓存
        config.addMappedStatement(ms);
    }

    /**
     * 创建一个简单的MS
     *
     * @param msId
     * @param sqlSource 执行的sqlSource
     * @param sqlType   执行的sqlType
     */
    private void updateMapped(String msId, SqlSource sqlSource, SqlCommandType sqlType) {
        MappedStatement ms = new MappedStatement.Builder(config, msId, sqlSource, sqlType)
                .resultMaps(new ArrayList<ResultMap>() {{
                    add(new ResultMap.Builder(config, "defaultResultMap", int.class,
                            new ArrayList<ResultMapping>(0)).build());
                }}).build();
        //加入缓存
        config.addMappedStatement(ms);
    }

    public static boolean isNull(String sql) {
        return (sql == null || sql.length() == 0) ? true : false;
    }
}
