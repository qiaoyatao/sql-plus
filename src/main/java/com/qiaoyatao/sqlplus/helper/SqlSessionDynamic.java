package com.qiaoyatao.sqlplus.helper;

import com.qiaoyatao.sqlplus.constants.Symbol;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;

/**
 * MyBatis执行sql工具
 * author： qiaoyatao
 * date: 2020年5月27日08:55:04
 */
@Slf4j
@Component
@NoArgsConstructor
public class SqlSessionDynamic {

    private SqlSessionFactory sqlH;

    //注入外部数据源
    public SqlSessionDynamic(SqlSessionFactory sessionFactory) {
        sqlH = sessionFactory;
    }

    /**
     * 插入数据
     *
     * @param sql 执行的sql
     */
    public int insert(String sql) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        int insert = (sql == null || sql.length() == 0
                ? 0 : sqlSession.insert(sqlPlus.insert(sql)));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return insert;
    }

    /**
     * 插入数据
     *
     * @param sql   执行的sql
     * @param param 参数
     */
    public int insert(String sql, Object param) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        int insert = (sql == null || sql.length() == 0
                ? 0 : sqlSession.insert(sqlPlus.insertDynamic(sql,
                (param != null ? param.getClass() : null)), param));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return insert;
    }

    /**
     * 删除数据
     *
     * @param sql 执行的sql
     */
    public int delete(String sql) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        int delete = (sql == null || sql.length() == 0
                ? 0 : sqlSession.delete(sqlPlus.delete(sql)));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return delete;
    }

    /**
     * 删除数据
     *
     * @param sql   执行的sql
     * @param param 参数
     */
    public int delete(String sql, Object param) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        int delete = (sql == null || sql.length() == 0
                ? 0 : sqlSession.delete(sqlPlus.deleteDynamic(sql,
                (param != null ? param.getClass() : null)), param));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return delete;
    }

    /**
     * 更新数据
     *
     * @param sql 执行的sql
     */
    public int update(String sql) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        int update = (sql == null || sql.length() == 0
                ? 0 : sqlSession.update(sqlPlus.update(sql)));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return update;
    }

    /**
     * 更新数据
     *
     * @param sql   执行的sql
     * @param param 参数
     */
    public int update(String sql, Object param) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        int update = (sql == null || sql.length() == 0
                ? 0 : sqlSession.update(sqlPlus.updateDynamic(sql,
                (param != null ? param.getClass() : null)), param));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return update;
    }

    /**
     * 查询返回一个结果，多个结果时抛出异常
     *
     * @param sql    执行的sql
     * @param result 返回的结果类型
     * @param <R>    泛型类型
     */
    public <R> R selectOne(String sql, Class result) {
        List<R> list = (sql == null || sql.length() == 0)
                ? null : selectList(sql, result);
        return list.size() != 0 ? list.get(0) : null;
    }

    /**
     * 查询返回一个结果
     *
     * @param sql    执行的sql
     * @param param  参数
     * @param result 返回的结果类型
     * @param <R>    泛型类型
     */
    public <R> R selectOne(String sql, Object param, Class result) {
        List<R> list = (sql == null || sql.length() == 0)
                ? null : selectList(sql, param, result);
        return list.size() != 0 ? list.get(0) : null;
    }

    public Long count(String sql) {
        return (sql == null || sql.length() == 0) ? 0L : selectOne(sql, Long.class);
    }

    /**
     * 查询返回指定的结果类型
     *
     * @param sql    执行的sql
     * @param result 返回的结果类型
     * @param <R>    泛型类型
     */
    public <R> List<R> selectList(String sql, Class<R> result) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        List<R> list = (sql == null || sql.length() == 0) ?
                null : sqlSession.selectList(sqlPlus.select(sql, result));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return list;

    }

    /**
     * 查询返回指定的结果类型
     *
     * @param sql    执行的sql
     * @param param  参数
     * @param result 返回的结果类型
     * @param <R>    泛型类型
     */
    public <R> List<R> selectList(String sql, Object param, Class<R> result) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        List<R> list = (sql == null || sql.length() == 0)
                ? null : sqlSession.selectList(sqlPlus.selectDynamic(sql,
                (param != null ? param.getClass() : null), result), param);
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return list;
    }

    /**
     * @param sql 执行的sql
     */
    public Map<String, Object> selectOne(String sql) {
        List<Map<String, Object>> list = (sql == null || sql.length() == 0)
                ? null : selectList(sql);
        return list.size() != 0 ? list.get(0) : null;
    }

    /**
     * @param sql   执行的sql
     * @param param 参数
     */
    public Map<String, Object> selectOne(String sql, Object param) {
        List<Map<String, Object>> list = (sql == null || sql.length() == 0) ?
                null : selectList(sql, param);
        return list.size() != 0 ? list.get(0) : null;
    }


    /**
     * @param sql 执行的sql
     */
    public List<Map<String, Object>> selectList(String sql) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> list = (sql == null || sql.length() == 0) ? null :
                sqlSession.selectList(sqlPlus.select(sql));
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return list;

    }

    /**
     * @param sql   执行的sql
     * @param param 参数
     */
    public List<Map<String, Object>> selectList(String sql, Object param) {
        log.info(sql);
        SqlSession sqlSession = sqlH.openSession();
        SqlPlus sqlPlus = new SqlPlus(sqlSession);
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> list = (sql == null || sql.length() == 0) ? null :
                sqlSession.selectList(sqlPlus.selectDynamic(sql, param != null ?
                        param.getClass() : null), param);
        long endTime = System.currentTimeMillis();
        log.info(String.format(Symbol.run_time, (endTime - startTime)));
        log.info(Symbol.sqlSession + sqlSession.hashCode());
        sqlSession.close();
        return list;
    }

    public boolean create(String show, String create) {
        String result = selectOne(show, String.class);
        if (result == null || result.length() == 0) {
            insert(create);
            return true;
        }
        return false;
    }

}
