package com.qiaoyatao.sqlplus.enums;

/**
 * 主键类型
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public enum IdType {

    AUTO(0, "数据库ID自增"),
    INPUT(1, "用户输入ID"),
    ID_WORKER(2, "全局唯一ID"),
    UUID(3, "全局唯一ID"),
    NONE(4, "该类型为未设置主键类型");

    private final int key;
    private final String desc;

    private IdType(int key, String desc) {
        this.key = key;
        this.desc = desc;
    }
    public int getKey() {
        return this.key;
    }

    public String getDesc() {
        return this.desc;
    }
}
