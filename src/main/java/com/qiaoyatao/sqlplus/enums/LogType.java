package com.qiaoyatao.sqlplus.enums;

/**
 * 主键类型
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
public enum LogType {
    OTHER(0, "其他"),
    INSERT(1, "insert"),
    DELETE(2, "delete"),
    UPDATE(3, "update"),
    SELECT(4, "select"),
    MODIFY(5, "modify");

    private final int key;
    private final String desc;

    private LogType(int key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public int getKey() {
        return this.key;
    }

    public String getDesc() {
        return this.desc;
    }
}
