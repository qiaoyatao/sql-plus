package com.qiaoyatao.sqlplus.base;

import com.qiaoyatao.sqlplus.core.page.*;
import org.springframework.web.bind.annotation.*;
import java.io.Serializable;

public interface BaseController<T> {

    @PostMapping("/add")
    void add(@RequestBody T model);

    @GetMapping("/delete/{id}")
    void delete(@PathVariable Serializable id);

    @PostMapping("/update")
    void update(@RequestBody T model);

    @GetMapping("/queryById/{id}")
    T queryById(@PathVariable Serializable id);

    @PostMapping("/selectPage")
    PageInfo<T> selectPage(@RequestBody T model);

}
