package com.qiaoyatao.sqlplus.base;

import com.qiaoyatao.sqlplus.core.page.PageInfo;
import java.io.Serializable;

public interface BaseService<T> {

    void add(T model);

    void deleteById(Serializable id);

    void updateById(T model);

    T selectById(Serializable id);

    PageInfo<T> selectPage(T model);

}
