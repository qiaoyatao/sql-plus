package com.qiaoyatao.sqlplus.annotation.log;

import com.qiaoyatao.sqlplus.enums.LogType;
import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SysLog {

    String value() default "";

    String path() default "";

    LogType type() default LogType.OTHER; //默认其他
}
