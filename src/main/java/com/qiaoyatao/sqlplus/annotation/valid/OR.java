package com.qiaoyatao.sqlplus.annotation.valid;

import java.lang.annotation.*;

/**
 * 或者注解
 * author： qiaoyatao
 * date: 2019年12月5日18:37:30
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface OR {

    String value();//类型多个逗号分隔

    String message() default "类型错误!";

    Class<?>[] groups() default { };
}
