package com.qiaoyatao.sqlplus.annotation.valid;

import java.lang.annotation.*;

/**
 * 权限注解
 * author：qiaoyatao
 * date: 2019年12月4日10:33:55
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface PreAuthorize {

    String value();
}