package com.qiaoyatao.sqlplus.annotation.valid;
import java.lang.annotation.*;
/**
 * 字段分组(方法注解)
 * author： qiaoyatao
 * date: 2019年12月5日18:37:30
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Group {

    Class<?> value();
}

