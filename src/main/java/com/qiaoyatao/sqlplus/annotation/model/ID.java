package com.qiaoyatao.sqlplus.annotation.model;

import com.qiaoyatao.sqlplus.enums.IdType;
import java.lang.annotation.*;

/**
 * 主键注解
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ID {

    String value() default "";//key

    String comment() default "";//备注

    IdType type() default IdType.AUTO;

}
