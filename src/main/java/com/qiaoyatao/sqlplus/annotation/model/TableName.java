package com.qiaoyatao.sqlplus.annotation.model;

import java.lang.annotation.*;

/**
 * 表名段注解
 * author： qiaoyatao
 * date: 2019年11月9日16:05:14
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface TableName {

    String value();

    String comment() default "";//备注
}