package com.qiaoyatao.sqlplus.annotation.model;
import java.lang.annotation.*;

/**
 * 表字段注解
 * author： qiaoyatao
 * date: 2019年11月9日15:46:06
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface TableField {

    String value() default "";

    String comment() default "";//备注

    boolean like() default false; //是否为like查询

    boolean exist() default true; //true 操作数据库，false 不操作数据库
}

