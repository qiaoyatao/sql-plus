package com.qiaoyatao.sqlplus.annotation.bean;

import java.lang.annotation.*;
/**
 * 状态标志注解
 * author： qiaoyatao
 * date: 2019年12月5日18:37:30
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Status {}