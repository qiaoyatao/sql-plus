package com.qiaoyatao.gen.constants;

/**
 * SP
 * author： qiaoyatao
 * date: 2020年9月3日17:18:37
 */
public class Gen {

    public static final String URL = "jdbc:mysql://%s:3306/%s?useUnicode=true&characterEncoding=utf8&serverTimezone=CTT";

    public static final String DRIVERNAME = "com.mysql.cj.jdbc.Driver";

    public static final String JSONPARAM = "@RequestBody %sDTO dto";

    public static final String PARAM = "%sDTO dto";

    public static final String Param = "@Param(\"%s\") ";

    public static final String PARAMMODEL = "@Param(\"model\") %sVO model, @Param(\"param\") %sDTO param";

    public static final String FORM = "form";

    public static final String SELECT = "select";

    public static final String INSERT = "insert";

    public static final String UPDATE = "update";

    public static final String DELETE = "delete";

    public static final String AS = "%s as %s";

    public static final String IVIEW = "templates/gen/view.java.vm";

    public static final String ISERVICE = "templates/gen/service.java.vm";

    public static final String IACHIEVE = "templates/gen/achieve.java.vm";

    public static final String IMAPPER = "templates/gen/mapper.java.vm";

    public static final String IXML = "templates/gen/xml.vm";

    public static final String IVO = "templates/gen/vo.java.vm";

    public static final String VIEW = "view/%sController.java";

    public static final String SERVICE = "view/%sService.java";

    public static final String ACHIEVE = "view/%sServiceImpl.java";

    public static final String MAPPER = "view/%sMapper.java";

    public static final String XML = "view/%sMapper.xml";

    public static final String SVO = "view/%sVO.java";

    public static final String SDTO = "view/%sDTO.java";

    public static final String PACKAGE = "package";

    public static final String TABLE = "table";

    public static final String NAME = "name";

    public static final String UPNAME = "upname";

    public static final String ASKNAME = "@%sMapping";

    public static final String CODE = "codeList";

    public static final String LOADER = "file.resource.loader.class";

    public static final String VELOCITY = "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader";

    public static final String VOID = "void";

    public static final String INT = "int";

    public static final String VO = "%sVO";

    public static final String TYPE = "type";

    public static final String PATH = "view";

    public static final String LIST = "List<%s>";

    public static final String PAGEINFO = "PageInfo<%s>";

    public static final String DTO = "dto";

    public static final String J_DTO = "%sDTO.java";

    public static final String J_VO = "%sVO.java";

    public static final String X = "_ _";

    //genTable常量
    public static final String CREATE = "CREATE TABLE IF NOT EXISTS `%s` ( " +
            "%s" +
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='%s';";

    public static final String FIELD =" `%s` %s DEFAULT NULL COMMENT '%s',";

    public static final String FIELD_NAME ="字段名";

    public static final String SHEET_NAME = "数据库设计";

    public static final String[] TITLES ={"字段名", "字段类型", "描述"};;
}