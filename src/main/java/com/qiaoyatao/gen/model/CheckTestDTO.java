package com.qiaoyatao.gen.model;

import lombok.Data;

/**
 * @author: 乔小乔
 * @date: 2020-09-03 17:01:40
 * 描述: sp测试数据库连接
 */
@Data
public class CheckTestDTO {

    //地址
    private String url;

    //数据库
    private String database;

    //用户名
    private String username;

    //密码
    private String password;
    //表
    private String tableName;
}
