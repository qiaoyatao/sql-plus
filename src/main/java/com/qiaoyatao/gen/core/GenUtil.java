package com.qiaoyatao.gen.core;

import com.qiaoyatao.sqlplus.system.bean.*;
import com.qiaoyatao.gen.constants.Gen;
import com.qiaoyatao.gen.dto.CodeDTO;
import com.qiaoyatao.gen.dto.CreateCodeDTO;
import com.qiaoyatao.gen.dto.CreateDTO;
import com.qiaoyatao.gen.vo.CreateCodeVO;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.gen.builder.SpUtil;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import java.io.*;
import java.util.*;

public class GenUtil {

    private static boolean create;

    public static CreateCodeVO createCode(CreateDTO dto,boolean createCode) {
        create=createCode;
        delFile(Gen.PATH);
        List<String> list = createService(dto);
        return CreateCodeVO.builder()
                .view(createView(dto))
                .service(list.get(0))
                .achieve(list.get(1))
                .mapper(createMapper(dto))
                .xml(createxml(dto))
                .asList(createModel(dto))
                .build();
    }


    private static String createView(CreateDTO dto) {
        String name = SpUtil.toUpperCase(StrUtil.toCamelCase(dto.getTableName()));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Gen.PACKAGE, dto.getBasePackage());
        ctx.put(Gen.TABLE, dto.getTableName());
        ctx.put(Gen.NAME, name);
        ctx.put(Gen.UPNAME, StrUtil.toCamelCase(dto.getTableName()));
        List<CodeDTO> list = new ArrayList<>();
        dto.getCodeList().forEach(item->{
            list.add(CodeDTO.builder()
                    .method(item.getName())
                    .askName(String.format(Gen.ASKNAME,item.getRequestType()))
                    .methodParam(createParam(item,true,false))
                    .param(createR(item))
                    .result(item.getResult()==null?"0":"1")
                    .build());
        });
        ctx.put(Gen.CODE, list);
        String code = getCode(Gen.IVIEW, ctx);
        if (create){
            create(code,String.format(Gen.VIEW,name));
        }
        return code;
    }

    private static List<String> createService(CreateDTO dto) {
        String name = SpUtil.toUpperCase(StrUtil.toCamelCase(dto.getTableName()));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Gen.PACKAGE, dto.getBasePackage());
        ctx.put(Gen.NAME, name);
        ctx.put(Gen.UPNAME, StrUtil.toCamelCase(dto.getTableName()));
        List<CodeDTO> list = new ArrayList<>();
        dto.getCodeList().forEach(item->{
            list.add(CodeDTO.builder()
                    .method(item.getName())
                    .askName(modifyName(item.getName()))
                    .methodParam(createParam(item,false,false))
                    .param(createR(item))
                    .result(resultVo(item,false))
                    .build());
        });
        ctx.put(Gen.CODE, list);
        List<String> asList = Arrays.asList(getCode(Gen.ISERVICE, ctx), getCode(Gen.IACHIEVE, ctx));
        if (create){
            create(asList.get(0),String.format(Gen.SERVICE,name));
            create(asList.get(1),String.format(Gen.ACHIEVE,name));
        }
        return asList;
    }

    private static String createMapper(CreateDTO dto) {
        String name = SpUtil.toUpperCase(StrUtil.toCamelCase(dto.getTableName()));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Gen.PACKAGE, dto.getBasePackage());
        ctx.put(Gen.NAME, name);
        List<CodeDTO> list = new ArrayList<>();
        dto.getCodeList().forEach(item->{
            list.add(CodeDTO.builder()
                    .method(modifyName(item.getName()))
                    .methodParam(createParam(item,false,true))
                    .param(createR(item))
                    .result(resultVo(item,true))
                    .build());
        });
        ctx.put(Gen.CODE, list);
        String code = getCode(Gen.IMAPPER, ctx);
        if (create){
            create(code,String.format(Gen.MAPPER, name));
        }
        return code;
    }


    private static String createxml(CreateDTO dto) {
        String name = SpUtil.toUpperCase(StrUtil.toCamelCase(dto.getTableName()));
        VelocityContext ctx = new VelocityContext();
        ctx.put(Gen.PACKAGE, dto.getBasePackage());
        ctx.put(Gen.NAME, name);
        ctx.put(Gen.TABLE, dto.getTableName());
        List<CodeDTO> list = new ArrayList<>();
        dto.getCodeList().forEach(item->{
            list.add(CodeDTO.builder()
                    .method(modifyName(item.getName()))
                    .askName(SpUtil.toUpperCase(item.getName()))
                    .model(item)
                    .result(asName(item))
                    .build());
        });
        ctx.put(Gen.CODE, list);
        String code = getCode(Gen.IXML, ctx);
        if (create){
            create(code,String.format(Gen.XML, name));
        }
        return code;
    }

    private static List<String> createModel(CreateDTO dto) {
        List<String> list = new ArrayList<>();
        VelocityContext ctx = new VelocityContext();
        ctx.put(Gen.PACKAGE, dto.getBasePackage());
        dto.getCodeList().forEach(item->{
            list.addAll(create(ctx,dto,item));
        });
        return list;
    }

    private static String createParam(CreateCodeDTO param,boolean view,boolean mapper) {
        StringBuffer result = new StringBuffer();
        if (mapper && param.getOperating().equals(Gen.UPDATE)){
           return String.format(Gen.PARAMMODEL, SpUtil.toUpperCase(param.getName()), SpUtil.toUpperCase(param.getName()));
        }
        if (mapper){
           return String.format(Gen.PARAM, SpUtil.toUpperCase(param.getName()));
        }
        if (param.getInterfaceType().equals(Gen.FORM)) {
            param.getParamData().forEach(item -> {
                result.append(item.getColumnClass() + item.getVariable() + ",");
            });
            return SqlUtil.substr(result);
        }
        return result.append(String.format(view? Gen.JSONPARAM: Gen.PARAM, SpUtil.toUpperCase(param.getName()))).toString();
    }
    private static String createR(CreateCodeDTO param) {
        StringBuffer result = new StringBuffer();
        if (param.getInterfaceType().equals(Gen.FORM)) {
            param.getParamData().forEach(item -> {
                result.append(item.getVariable() + ",");
            });
            return SqlUtil.substr(result);
        }
        return result.append(Gen.DTO).toString();
    }

    private static String asName(CreateCodeDTO param) {
        StringBuffer result = new StringBuffer();
        if (!param.getOperating().equals(Gen.SELECT)) {
            return null;
        }
        param.getRespVo().forEach(item -> {
            result.append(!item.getVariable().equals(item.getField())?
                    String.format(Gen.AS,item.getField(),item.getVariable())
                    :item.getField());
            result.append(",");
        });
        return SqlUtil.substr(result);
    }

    private static String resultVo(CreateCodeDTO param,boolean mapper) {
        if (param.getResult()==null){
            return mapper? Gen.INT: Gen.VOID;
        }
        if (param.getResult()==1){
            return String.format(Gen.VO, SpUtil.toUpperCase(param.getName()));
        }
        if (param.getResult()==2 || (mapper && param.getResult()==3)){
            return  String.format(Gen.LIST,String.format(Gen.VO, SpUtil.toUpperCase(param.getName())));
        }
        if (param.getResult()==3){
            return String.format(Gen.PAGEINFO,String.format(Gen.VO, SpUtil.toUpperCase(param.getName())));
        }
        return mapper? Gen.INT: Gen.VOID;
    }

    private static List<String> create(VelocityContext ctx,CreateDTO dto,CreateCodeDTO param) {
        List<String> list = new ArrayList<>();
        if (Gen.SELECT.equals(param.getOperating()) || Gen.UPDATE.equals(param.getOperating())){
            list.add(createVo(ctx,dto,param));
            list.add(createDto(ctx,dto,param));
        }
        if (Gen.INSERT.equals(param.getOperating())|| Gen.DELETE.equals(param.getOperating())){
            list.add(createDto(ctx,dto,param));
        }
        return list;
    }

    private static String createVo(VelocityContext ctx,CreateDTO dto,CreateCodeDTO param) {
        if (param.getRespVo()==null){
            return null;
        }
        String name = SpUtil.toUpperCase(param.getName());
        ctx.put(Gen.CODE, param.getRespVo());
        ctx.put(Gen.TYPE, false);
        ctx.put(Gen.UPNAME, name);
        String code = getCode(Gen.IVO, ctx);
        if (create){
            create(code,String.format(Gen.SVO, name));
        }
        return result(code,String.format(Gen.J_VO, name));
    }
    private static String createDto(VelocityContext ctx,CreateDTO dto,CreateCodeDTO param) {
        if (param.getParamData()==null){
            return null;
        }
        String name = SpUtil.toUpperCase(param.getName());
        ctx.put(Gen.CODE, param.getParamData());
        ctx.put(Gen.TYPE, true);
        ctx.put(Gen.UPNAME, name);
        String code = getCode(Gen.IVO, ctx);
        if (create){
            create(code,String.format(Gen.SDTO, name));
        }
        return result(code,String.format(Gen.J_DTO, name));
    }


    public static String generatorCode(String filePath) {
        File file = new File(filePath);
        try {
            StringBuffer sbf = new StringBuffer();
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String tempStr;
            while ((tempStr = reader.readLine()) != null) {
                sbf.append(tempStr);
            }
            reader.close();
            return sbf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getCode(String template, VelocityContext ctx) {
        StringWriter sw = new StringWriter();
        Properties prop = new Properties();
        prop.put(Gen.LOADER, Gen.VELOCITY);
        Velocity.init(prop);
        Velocity.getTemplate(template, "UTF-8").merge(ctx, sw);
        return sw.toString();
    }

    public static void create(String view,String name){
        PrintWriter print = getPrin(name);
        print.println(view);
        print.close();
    }

    private static PrintWriter getPrin(String fileName) {
        try {
            return new PrintWriter(fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    private static void delFile(String path) {
        File file = new File(path);
        for (String name : file.list()) {
            new File(Gen.PATH+"/"+name).delete();
        }
    }

    private static String result(String code,String name) {
        return StrUtil.append(code,Gen.X,name);
    }

    private static String modifyName(String name) {
        name=name.replaceFirst("query",Gen.SELECT);
        name=name.replaceFirst("add",Gen.INSERT);
        return name;
    }
}
