package com.qiaoyatao.gen.core;

import com.qiaoyatao.gen.constants.Gen;
import com.qiaoyatao.gen.dto.GenTablePO;
import com.qiaoyatao.gen.dto.GenTableDetailPO;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class GenTableUtil {
    /**
     * 获取sheet列表数据
     *
     * @param sheetAt sheet页
     * @param file    excel文件
     * @return
     */
    public static List<XSSFRow> getRow(int sheetAt, MultipartFile file) {
        try {
            if (file == null) {
                return null;
            }
            List<XSSFRow> list = new ArrayList<>();
            BufferedInputStream bf = new BufferedInputStream(file.getInputStream());
            XSSFWorkbook workbook = new XSSFWorkbook(bf);
            XSSFSheet sheet = workbook.getSheetAt(sheetAt);
            for (int i = 0; i <= getExcelRealRow(sheet); i++) {
                XSSFRow row = sheet.getRow(i);
                if (row == null) {
                    continue;
                }
                list.add(row);
            }
            bf.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取sheet列表数据
     *
     * @param file excel文件
     * @return
     */
    public static List<XSSFRow> getRowList(MultipartFile file) {
        try {
            if (file == null) {
                return null;
            }
            List<XSSFRow> list = new ArrayList<>();
            BufferedInputStream bf = new BufferedInputStream(file.getInputStream());
            XSSFWorkbook workbook = new XSSFWorkbook(bf);
            for (int sheetAt = 0; sheetAt < workbook.getNumberOfSheets(); sheetAt++) {
                XSSFSheet sheet = workbook.getSheetAt(sheetAt);
                for (int i = 0; i <= getExcelRealRow(sheet); i++) {
                    XSSFRow row = sheet.getRow(i);
                    if (row == null) {
                        continue;
                    }
                    list.add(row);
                }
            }
            bf.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getValue(XSSFCell cell) {
        if (cell == null) {
            return null;
        }
        cell.setCellType(CellType.STRING);
        return cell.getStringCellValue();
    }


    //查询
    public static boolean search(String[] array, String searchValue) {
        if (array == null) {
            return false;
        }
        for (String key : Arrays.asList(array)) {
            if (key.toLowerCase().equals(searchValue.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public static String formatDate(Object date) {
        return null == date ? null : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }


    // 获取Excel表的真实行数
    private static int getExcelRealRow(XSSFSheet sheet) {
        boolean flag = false;
        for (int i = 1; i <= sheet.getLastRowNum(); ) {
            Row r = sheet.getRow(i);
            if (r == null) {
                // 如果是空行（即没有任何数据、格式），直接把它以下的数据往上移动
                sheet.shiftRows(i + 1, sheet.getLastRowNum(), -1);
                continue;
            }
            flag = false;
            for (Cell c : r) {
                if (c.getCellType() != CellType.BLANK) {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                i++;
                continue;
            } else {
                // 如果是空白行（即可能没有数据，但是有一定格式）
                if (i == sheet.getLastRowNum()) {
                    // 如果到了最后一行，直接将那一行remove掉
                    sheet.removeRow(r);
                } else {
                    //如果还没到最后一行，则数据往上移一行
                    sheet.shiftRows(i + 1, sheet.getLastRowNum(), -1);
                }
            }
        }
        return sheet.getLastRowNum();
    }


    /**
     * 获取每行数据
     *
     * @return
     */
    public static List<GenTablePO> getLevelRow(List<XSSFRow> rows) {
        if (rows.isEmpty()) {
            return null;
        }
        GenTablePO result = new GenTablePO();
        List<GenTablePO> resultList = new ArrayList<>();
        List<GenTableDetailPO> indexList = new ArrayList<>();
        int indx = 0;
        for (XSSFRow row : rows) {
            String value = getName(row);
            if (value != null) {
                if (indx == 0) {
                    if (!indexList.isEmpty()) {
                        result.setAppend(indexList);
                        resultList.add(result);
                        result = new GenTablePO();
                        indexList = new ArrayList<>();
                    }
                    result.setTableCn(value);
                    indx++;
                    continue;
                }
                result.setTableEn(value);
                indx = 0;//重置为0
                continue;
            }
            GenTableDetailPO dto = new GenTableDetailPO();
            int statr = 0;
            for (int i = 0; i < Integer.valueOf(row.getLastCellNum()); i++) {
                if (row.getCell(i) == null) {
                    continue;
                }
                value = GenTableUtil.getValue(row.getCell(i));
                if (value == null || value.length() == 0) {
                    continue;
                }
                //排除标题
                if (Gen.FIELD_NAME.contains(value)) {
                    break;
                }
                if (statr == 0) {
                    dto.setName(value);
                    statr++;
                    continue;
                }
                if (statr == 1) {
                    dto.setType(value);
                    statr++;
                    continue;
                }
                if (statr == 2) {
                    statr = 0;
                    dto.setDesc(value);
                    indexList.add(dto);
                    break;
                }
            }
        }
        return resultList;
    }

    //获取表名与备注
    private static String getName(XSSFRow row) {
        int index = 0;
        String value = null;
        for (int i = 0; i < Integer.valueOf(row.getLastCellNum()); i++) {
            String val = GenTableUtil.getValue(row.getCell(i));
            if (val != null && val.length() != 0) {
                index++;
                value = val;
            }
        }
        return index == 1 ? value : null;
    }

    //获取sql
    public static List<String> getSql(List<GenTablePO> paramList) {
        if (paramList.isEmpty()) {
            return null;
        }
        List<String> sqlList = new ArrayList<>();
        for (GenTablePO param : paramList) {
            sqlList.add(fromatSql(param));
        }
        return sqlList;
    }

    //格式化成sql语句
    private static String fromatSql(GenTablePO param) {
        StringBuffer sb = new StringBuffer();
        for (GenTableDetailPO field : param.getAppend()) {
            sb.append(String.format(Gen.FIELD, StrUtil.toSymbolCase(field.getName()).toLowerCase(), field.getType(), field.getDesc()));
        }
        return String.format(Gen.CREATE, StrUtil.toSymbolCase(param.getTableEn()), SqlUtil.substr(sb), param.getTableCn());
    }

    public static void dowcodeExcel(List<GenTablePO> tablePOList, HttpServletResponse resp) throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(Gen.SHEET_NAME);
        int start = 2;
        setTitle(tablePOList, workbook, sheet, start);
        resp.setHeader("content-type", "application/octet-stream");
        resp.setContentType("application/octet-stream");
        resp.setHeader("Content-Disposition", "attachment;filename=" +
                URLEncoder.encode(Gen.SHEET_NAME + ".xlsx", "UTF-8"));
        OutputStream os = resp.getOutputStream();
        workbook.write(os);
        os.close();
    }

    private static void setTitle(List<GenTablePO> poList, XSSFWorkbook workbook, XSSFSheet sheet, int start) {
        for (GenTablePO po : poList) {
            start++;
            XSSFRow rowCn = sheet.createRow(start);
            rowCn.setHeightInPoints(20);
            XSSFCell cellCn = rowCn.createCell(0);
            cellCn.setCellValue(po.getTableCn());
            sheet.addMergedRegion(new CellRangeAddress(start, start, 0, 2));
            start++;
            XSSFRow rowEn = sheet.createRow(start);
            rowEn.setHeightInPoints(20);
            XSSFCell cellEn = rowEn.createCell(0);
            cellEn.setCellValue(po.getTableEn());
            sheet.addMergedRegion(new CellRangeAddress(start, start, 0, 2));
            XSSFCellStyle style = workbook.createCellStyle();
            style.setAlignment(HorizontalAlignment.CENTER);//水平居中
            style.setVerticalAlignment(VerticalAlignment.CENTER);//垂直居中
            style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            style.setWrapText(true);//自动换行
            cellCn.setCellStyle(style);
            cellEn.setCellStyle(style);
            start++;
            XSSFRow row = sheet.createRow(start);
            for (int i = 0; i < Gen.TITLES.length; i++) {
                sheet.setColumnWidth((short) i, (short) (row.createCell(i)
                        .getStringCellValue().getBytes().length * 2560 + 6000));
                row.createCell(i).setCellValue(Array.get(Gen.TITLES, i).toString());
            }
            row.setHeightInPoints(20); // 设置行的高度
            start++;
            for (int i = 0; i < po.getAppend().size(); i++) {
                XSSFRow row3 = sheet.createRow(start);
                GenTableDetailPO detailPO = po.getAppend().get(i);
                row3.createCell(0).setCellValue(detailPO.getName());
                row3.createCell(1).setCellValue(detailPO.getType());
                row3.createCell(2).setCellValue(detailPO.getDesc());
                start++;
            }
            start++;
        }
    }
}
