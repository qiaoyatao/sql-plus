package com.qiaoyatao.gen.controller;

import com.qiaoyatao.sqlplus.core.format.R;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import com.qiaoyatao.gen.service.*;
import com.qiaoyatao.gen.model.*;
import com.qiaoyatao.gen.dto.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: 乔小乔
 * @date: 2020-09-03 17:01:40
 * 描述: 线上代码生成器
 */
@Slf4j
@RestController
@RequestMapping("/api/gen")
public class GenController {

    @Autowired
    private GenService spService;

    @PostMapping("/checkTest")
    public R checkTest(@RequestBody CheckTestDTO dto) {
        return R.ok(spService.checkTest(dto));
    }

    @PostMapping("/queryByField")
    public R queryByField(@RequestBody CheckTestDTO dto) {
        return R.ok(spService.queryByField(dto));
    }

    @PostMapping("/createCode")
    public R createCode(@RequestBody CreateDTO dto) {
        return R.ok(spService.createCode(dto));
    }

    @PostMapping("/dowcode")
    public void dowcode(@RequestBody CreateDTO dto,HttpServletResponse resp) throws IOException {
        spService.dowcode(dto,resp);
    }

    @GetMapping("/dowcode")
    public void dowcode(HttpServletResponse resp) throws IOException {
        spService.dowcode(resp);
    }
}




