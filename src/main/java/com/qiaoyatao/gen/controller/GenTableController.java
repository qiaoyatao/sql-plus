package com.qiaoyatao.gen.controller;

import com.qiaoyatao.gen.dto.GenTableDTO;
import com.qiaoyatao.gen.model.CheckTestDTO;
import com.qiaoyatao.gen.service.GenTableService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;

/**
 * @author: 乔小乔
 * @date: 2021年3月4日14:03:17
 * 描述: 生成数据库表
 */
@Slf4j
@RestController
@RequestMapping("/api/genTable")
public class GenTableController {

    @Autowired
    private GenTableService genTableService;

    @PostMapping("/")
    public void create(GenTableDTO dto) {
        genTableService.create(dto);
    }

    @GetMapping("/dowcode")
    public void dowcode(CheckTestDTO dto, HttpServletResponse resp)  throws Exception{
        genTableService.dowcode(dto,resp);
    }
}
