package com.qiaoyatao.gen.service;

import com.qiaoyatao.gen.vo.CreateCodeVO;
import com.qiaoyatao.gen.model.*;
import com.qiaoyatao.gen.dto.*;
import com.qiaoyatao.sqlplus.gen.builder.SpField;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author: 乔小乔
 * @date: 2020-09-03 17:01:40
 * 描述: 线上代码生成
 */
public interface GenService {

    List<String> checkTest(CheckTestDTO dto);

    List<SpField> queryByField(CheckTestDTO dto);

    CreateCodeVO createCode(CreateDTO dto);

    void dowcode(HttpServletResponse resp) throws IOException;

    void dowcode(CreateDTO dto,HttpServletResponse resp) throws IOException;
}

