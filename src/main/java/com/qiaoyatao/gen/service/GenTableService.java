package com.qiaoyatao.gen.service;

import com.qiaoyatao.gen.dto.GenTableDTO;
import com.qiaoyatao.gen.model.CheckTestDTO;

import javax.servlet.http.HttpServletResponse;

public interface GenTableService {

    void create(GenTableDTO dto);

    void dowcode(CheckTestDTO dto, HttpServletResponse resp) throws Exception;
}
