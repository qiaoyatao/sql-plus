package com.qiaoyatao.gen.service.impl;

import com.qiaoyatao.gen.constants.Gen;
import com.qiaoyatao.gen.core.GenUtil;
import com.qiaoyatao.gen.vo.CreateCodeVO;
import com.qiaoyatao.sqlplus.annotation.bean.AsName;
import com.qiaoyatao.sqlplus.core.format.SqlUtil;
import com.qiaoyatao.sqlplus.gen.GlobalConfig;
import com.qiaoyatao.sqlplus.gen.builder.SpUtil;
import com.qiaoyatao.sqlplus.gen.builder.DataSource;
import com.qiaoyatao.sqlplus.gen.builder.SpField;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.system.bean.StrUtil;
import com.qiaoyatao.sqlplus.system.valid.Assert;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.qiaoyatao.gen.service.*;
import com.qiaoyatao.gen.model.*;
import com.qiaoyatao.gen.dto.*;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author: 乔小乔
 * @date: 2020-09-03 17:01:40
 * 描述: 线上代码生成
 */
@SuppressWarnings("all")
@Transactional
@Service
public class GenServiceImpl implements GenService {

    @Override
    public List<String> checkTest(CheckTestDTO dto) {
        GlobalConfig global = getGlobal(dto);
        Assert.isNull(global.getConnection(),"连接失败！");
        return SpUtil.getTable(global);
    }

    @Override
    public List<SpField> queryByField(CheckTestDTO dto) {
        return SpUtil.getField(dto.getTableName(), getGlobal(dto));
    }


    @Override
    public CreateCodeVO createCode(CreateDTO dto) {
        return GenUtil.createCode(dto,true);
    }

    @Override
    public void dowcode(HttpServletResponse resp) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(out);
        byte[] buffer = new byte[4096];
        File file = new File(Gen.PATH);
        String[] filenames = file.list();
        for (String name : filenames) {
            File newFile = new File(Gen.PATH + "/" + name);
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(newFile));
            zip.putNextEntry(new ZipEntry(newFile.getName()));
            int size = 0;
            while ((size = bis.read(buffer)) > 0) {
                zip.write(buffer, 0, size);
            }
            zip.closeEntry();
            bis.close();
        }
        resp.reset();
        resp.setHeader("Content-Disposition", "attachment; filename=\"code.zip\"");
        resp.addHeader("Content-Length", "" + out.toByteArray().length);
        resp.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(out.toByteArray(), resp.getOutputStream());
    }

    @Override
    public void dowcode(CreateDTO dto,HttpServletResponse resp) throws IOException {
        CreateCodeVO code = GenUtil.createCode(dto,false);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(out);
        Field[] fields = SqlUtil.superField(code.getClass());
        for (Field field : fields) {
            field.setAccessible(true);
            AsName asName = field.getAnnotation(AsName.class);
            if (asName!=null){
                write(zip,String.format(asName.value(), SpUtil.toUpperCase(StrUtil.toCamelCase(dto.getTableName())))
                        ,SqlUtil.get(field, code).toString());
            }
            if (field.getType().equals(List.class)){
                List<String> asList = (List)SqlUtil.get(field, code);
                if (asList==null){return;}
                for (String data : asList) {
                    write(zip,String.format(data.split(Gen.X)[1]),String.format(data.split(Gen.X)[0]));
                }
            }
        }
        resp.reset();
        resp.setHeader("Content-Disposition", "attachment; filename=\"code.zip\"");
        resp.addHeader("Content-Length", "" + out.toByteArray().length);
        resp.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(out.toByteArray(), resp.getOutputStream());
    }


    /**
     * 输出内容
     */
    private void write(ZipOutputStream zip,String name,String data) throws IOException {
        zip.putNextEntry(new ZipEntry(name));
        StringWriter sw = new StringWriter();
        sw.write(data);
        IOUtils.write(sw.toString(), zip, "UTF-8");
        IOUtils.closeQuietly(sw);
        zip.closeEntry();
    }

    private GlobalConfig getGlobal(CheckTestDTO model){
        DataSource source = new DataSource();
        source.setUrl(String.format(Gen.URL,model.getUrl(),model.getDatabase()));
        source.setDriverName(Gen.DRIVERNAME);
        GlobalConfig global = new GlobalConfig();
        BeanUtil.copyAsName(model,source);
        global.setDatabase(model.getDatabase());
        global.setConnection(source.connection());
        return global;
    }
}

