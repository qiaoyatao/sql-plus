package com.qiaoyatao.gen.service.impl;

import com.qiaoyatao.gen.constants.Gen;
import com.qiaoyatao.gen.core.GenTableUtil;
import com.qiaoyatao.gen.dto.GenTableDTO;
import com.qiaoyatao.gen.dto.GenTableDetailPO;
import com.qiaoyatao.gen.dto.GenTablePO;
import com.qiaoyatao.gen.model.CheckTestDTO;
import com.qiaoyatao.gen.service.GenTableService;
import com.qiaoyatao.sqlplus.gen.GlobalConfig;
import com.qiaoyatao.sqlplus.gen.builder.DataSource;
import com.qiaoyatao.sqlplus.gen.builder.SpField;
import com.qiaoyatao.sqlplus.gen.builder.SpUtil;
import com.qiaoyatao.sqlplus.system.bean.BeanUtil;
import com.qiaoyatao.sqlplus.system.valid.Assert;
import org.apache.commons.dbutils.QueryRunner;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class GenTableServiceImpl implements GenTableService {

    private static QueryRunner r = new QueryRunner();

    @Override
    public void create(GenTableDTO param){
        GlobalConfig global = getGlobal(param);
        Assert.isNull(global.getConnection(),"连接失败！");
        List<String> sqlList= GenTableUtil.getSql(GenTableUtil.getLevelRow(
                GenTableUtil.getRowList(param.getFile())));
        sqlList.forEach(sql->{
            try {
                r.update(global.getConnection(), sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void dowcode(CheckTestDTO dto, HttpServletResponse resp) throws Exception {
        GlobalConfig global = getGlobal(dto);
        List<String> tableList = SpUtil.getTable(global);
        List<GenTablePO> fieldList = new ArrayList<>();
        tableList.forEach(tableName->{
             fieldList.add(GenTablePO.builder()
                     .tableCn(SpUtil.remarks(tableName,global))
                     .tableEn(tableName)
                     .append(getGenTableDetailList(SpUtil.getField(tableName, global)))
                     .build());
        });
        GenTableUtil.dowcodeExcel(fieldList,resp);
    }

    private List<GenTableDetailPO> getGenTableDetailList(List<SpField> spFields) {
        if (spFields.isEmpty()){
            return null;
        }
        List<GenTableDetailPO> result = new ArrayList<>();
        spFields.forEach(field->{
            result.add(new GenTableDetailPO(field));
        });
        return result;
    }

    public GlobalConfig getGlobal(CheckTestDTO model){
        DataSource source = new DataSource();
        source.setUrl(String.format(Gen.URL,model.getUrl(),model.getDatabase()));
        source.setDriverName(Gen.DRIVERNAME);
        GlobalConfig global = new GlobalConfig();
        BeanUtil.copyAsName(model,source);
        global.setDatabase(model.getDatabase());
        global.setConnection(source.connection());
        return global;
    }

}
