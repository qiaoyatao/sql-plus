package com.qiaoyatao.gen.vo;

import com.qiaoyatao.sqlplus.annotation.bean.AsName;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.List;

/**
 * @author: 乔小乔
 * @date: 2020-09-03 17:01:41
 * 描述: 返回信息
 */
@Builder
@Data
@Accessors(chain = true)
public class CreateCodeVO {

    //视图层
    @AsName(value = "%sController.java")
    private String view;

    //业务层
    @AsName(value = "%sService.java")
    private String service;

    //业务实现层
    @AsName(value = "%sServiceImpl.java")
    private String achieve;

    //持久层
    @AsName(value = "%sMapper.java")
    private String mapper;

    //持久层xml
    @AsName(value = "%sMapper.xml")
    private String xml;

    private List<String> asList;

}
