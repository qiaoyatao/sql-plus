package com.qiaoyatao.gen.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CodeDTO {

    private String method;

    private String methodParam;

    private String param;

    private String askName;

    private String result;

    private String resultAs;

    private CreateCodeDTO model;

}


