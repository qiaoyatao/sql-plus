package com.qiaoyatao.gen.dto;

import com.qiaoyatao.sqlplus.gen.builder.SpField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GenTableDetailPO {

    //字段名称
    private String type;
    //类型
    private String name;
    //字段描述
    private String desc;

    public GenTableDetailPO(SpField field) {
        this.type = field.getColumnType();
        this.name=field.getField();
        this.desc=field.getComment();
    }
}
