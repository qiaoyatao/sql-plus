package com.qiaoyatao.gen.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GenTablePO {

    //表名
    private String tableEn;
    //表描述
    private String tableCn;

    private List<GenTableDetailPO> append;

}
