package com.qiaoyatao.gen.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import java.util.List;

@Accessors(chain = true)
@Data
public class CreateDTO {

    private String basePackage;

    private String tableName;

    private List<CreateCodeDTO> codeList;
}
