package com.qiaoyatao.gen.dto;

import com.qiaoyatao.gen.model.CheckTestDTO;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class GenTableDTO extends CheckTestDTO {

    private MultipartFile file;
}
