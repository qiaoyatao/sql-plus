package com.qiaoyatao.gen.dto;

import com.qiaoyatao.sqlplus.gen.builder.SpField;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.List;

/**
 * @author: 乔小乔
 * @date: 2020-09-03 17:01:40
 * 描述:
 */
@Data
@Accessors(chain = true)
public class CreateCodeDTO {

    private String operating;

    private String name;

    private String interfaceType;

    private String requestType;

    private Integer result;

    private List<SpField> paramData;

    private List<SpField> respVo;


}
