# sql_plus
### 1、介绍

    锋利的sql

### 2、软件架构

    原生sql实现增删改查功能,实体类继承Model,或者mappe继承AnnotationMapper,再者也可以通过SqlPuls2类也可实现操作数据库,与mybatis进行无缝整合,两种代码生成器
    可根据生成代码规则,节约%80开发成本，开发只需关心书写业务逻辑，避免浪费精力书写与业务无关代码。节约你的开发周期，提高
    效率。 
  

### 3、安装教程

一、引入maven jar包 

```
<dependency>
    <groupId>com.qiaoyatao</groupId>
    <artifactId>sql-plus</artifactId>
    <version>2.0.0-SNAPSHOT</version>
 </dependency>
```

  创建配置类，引入以下配置

    @Configuration
    public class SqlSessionCF {
    
        @Autowired
        private JdbcTemplate jdbc;
    
        @Autowired
        private SqlSessionFactory sessionFactory;
    
        @Bean
        public SqlSession2 sqlSession2(){
            return 	new SqlSession2(jdbc);
        }
        
        @Bean
        public SqlSession3 sqlSession3(){
            return 	new SqlSession3(sessionFactory);
        }
    
        @Bean
        public SqlHelper sqlHelper(){
            return 	new SqlHelper(sessionFactory);
        }
    
    }

### 4、 使用说明

1. 通过实体类对象创建sql表结构。

    1、实体类对象、集成Model

   ```
   @Data
   public class Employee extends Model<Employee> {
   
       private  Integer id;
   
       private  String name;
   
       private  String age;
   
       private  String gender;
       
       private  Long deptId;
   
       private  String address;
   
       private Date createTime;
   }
   ```

自动生成实体文件

```
	DataSource source = new DataSource();
        source.setUrl("jdbc:mysql://localhost:3306/tmc_info?useUnicode=true&characterEncoding=utf8&serverTimezone=CTT");
        source.setUsername("root");
        source.setPassword("root");
        source.setDriverName("com.mysql.cj.jdbc.Driver");
        GlobalConfig global = new GlobalConfig();
        global.setConnection(source.get());
        global.setAuthor("乔小乔");
        global.setOutputDir("./src/test/java/");//文件路径
        global.setDatabase("tmc_info");//数据库
        //global.setExclude(new String[]{"sys_airport","sys_airport_city"}); 排除表
        //需要生成表(默认所有表)
        //global.setInclude(new String[]{"sys_airport","sys_airport_city",
                //"sys_tmc_office","sys_user_role"});
        PackageConfig pc = new PackageConfig();
        AutoGenerator generator = new AutoGenerator();
        generator.setGlobal(global);
        generator.setPc(pc);
        generator.execute();
        source.close();
```

2、执行创建表

```
Employee employee = new Employee();
employee.create();
```

3、运行结果发现数据库中创建了一个为employee的表。

```
CREATE TABLE `employee` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `age` varchar(100) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `dept_id` bigint(11) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

2. 插入数据

   ```
   Employee employee = new Employee();
   employee.setId(1);
   employee.setName("乔小乔");
   employee.setAddress("北京市昌平区生命科学园");
   employee.setAge("10");
   employee.setDeptId(1l);
   employee.setCreateTime(new Date());
   //1、单个新增
   employee.insert(); 
   ```

3. 批量插入

   ```
   List<Employee> list = new ArrayList<>();
   for (int i = 1; i <=10 ; i++) {
       Employee employee = new Employee();
       employee.setId(i);
       employee.setName("乔小乔");
       employee.setAddress("北京市昌平区生命科学园");
       employee.setAge("10");
       employee.setDeptId(1l);
       employee.setCreateTime(new Date());
       list.add(employee);
   }
   Employee.insertList(list);//2、批量新增
   ```

4. 多表插入

   ```
   SysAccount account = new SysAccount();
   account.setPhone("15510304125");
   account.setUserName("乔小乔");
   Employee employee = new Employee();
   employee.setId(1);
   employee.setName("乔小乔");
   employee.setAddress("北京市昌平区生命科学园");
   employee.setAge("27");
   employee.setDeptId(1l);
   employee.setCreateTime(new Date());
   Employee.insertList(account,employee);//不同类型插入
   ```

5. 参数id查询

   ```
   //1、参数id
   Employee one = new Employee().selectByid(1);
   ```

6. 实体字段id查询

   ```
   //2、实体字段id查询
   Employee employee = new Employee();
   employee.setId(2);
   Employee newEmployee = employee.selectByid();
   ```

7. 参数id删除

   ```
   //参数id删除
   int i = new Employee().deleteById(1);
   ```

8. 实体字段id删除

   ```
   //实体字段id删除
   Employee employee = new Employee();
   employee.setId(2);
   employee.deleteById();
   ```

9. 参数id更新

   ```
   //1、参数id更新
   Employee employee = new Employee();
   employee.setCreateTime(new Date());
   employee.setName("王梦妍");
   employee.setAge("20");
   employee.setGender("女");
   employee.setCreateTime(new Date());
   employee.setAddress("哈尔滨市");
   employee.updateById(1);
   ```

10. 字段id更新

    ```
    //实体字段id更新
    Employee employee = new Employee();
    employee.setId(1);
    employee.setName("乔小乔");
    employee.setAddress("北京市昌平区生命科学园");
    employee.updateById();
    ```

11. 参数list查询

    ```
    Employee employee = new Employee();
    employee.setName("乔小乔");
    employee.setAddress("北京市昌平区生命科学园");
    List<Employee> list = employee.selectList();
    ```

12. 参数count查询

    ```
    Employee employee1 = new Employee();
    employee1.setName("乔小乔");
    employee1.setAddress("北京市昌平区生命科学园");
    Long i = employee1.count();
    ```

    

13. 分页查询

    ```
    Employee employee = new Employee();
    employee.setId(1);
    employee.setName("乔小乔");
    PageInfo<Employee> info = employee.selectPage(new Page(1, 10));
    ```

14. 关键词删除（and）

    ```
    Employee employee = new Employee();
    employee.setId(1);
    employee.setName("乔小乔");
    employee.delete();
    ```

15. QueryModel删除 and 、or

    ```
    QueryModel QueryModel = new QueryModel();
    QueryModel.eq("name","乔小乔");
    QueryModel.or("id",1);
    int delete = new Employee().delete(QueryModel);
    ```

16. QueryModel map删除 

```
QueryModel QueryModel = new QueryModel();
Map<String, Object> params = new HashMap<>();
params.put("id",1);
params.put("name","乔小乔");
QueryModel.allEq(params);
int delete = new Employee().delete(QueryModel);
```

17. QueryModel查询 and 、or

    ```
    QueryModel QueryModel = new QueryModel();
    ```

    18. QueryModel list 查询

        ```
        QueryModel QueryModel = new QueryModel();
        QueryModel.eq("name","乔小乔");
        QueryModel.or("id",1);
        List<Employee> list = new Employee().selectList(QueryModel);
        ```

    19. QueryModel 更新

    ```
    QueryModel QueryModel = new QueryModel();
    QueryModel.eq("name","乔小乔");
    QueryModel.or("id",1);
    int update = new Employee().update(QueryModel);
    ```

    20. QueryModel map更新

    ```
    QueryModel QueryModel = new QueryModel();
    Map<String, Object> params = new HashMap<>();
    params.put("id",1);
    params.put("name","乔小乔");
    QueryModel.allEq(params);
    int update = new Employee().update(QueryModel)
    ```

    21. QueryModel 片段sql

    ```
    QueryModel QueryModel = new QueryModel();
    Object [] params={1,"乔小乔"};
    String sql=" `id` =# AND `name` =#";
    //1、数组类型
    QueryModel.judge(sql,params);
    //2、可变类型
    QueryModel.judge(sql,1,"乔小乔");
    //3、对象类型
    Employee e = new Employee();
    e.setId(1);
    e.setName("老王");
    QueryModel.judge(sql,e);
    //1、查询一条
    Employee employee = new Employee().selectOne(QueryModel);
    //2、查询列表
    List<Employee> list = new Employee().selectList(QueryModel);
    //3、条件更新
    int update = new Employee().update(QueryModel);
    //4、条件删除
    int delete = new Employee().delete(QueryModel);
    ```
    1. 22、QueryModel 全量sql

17. ```
    QueryModel QueryModel = new QueryModel(); //*号默认会替换成字段，可以查询具体字段，切记不可as
    String sql="SELECT * FROM `employee` WHERE `id` = # AND `name` =#";
    //1、数组类型
    Object [] params={1,"乔小乔"};
    QueryModel.qL(sql,params);
    //2、可变类型
    QueryModel.qL(sql,1,"乔小乔");
    //3、对象类型
    Employee e = new Employee();
    e.setId(1);
    e.setName("老王");
    QueryModel QueryModel1 = new QueryModel();
    QueryModel1.qL(sql,e);
    //1、查询一条
    Employee employee = new Employee().selectOne(QueryModel1);
    //2、查询列表
    List<Employee> list = new Employee().selectList(QueryModel);
    //3、条件更新
    int update = new Employee().update(QueryModel);
    //4、条件删除
    int delete = new Employee().delete(QueryModel);
    ```

5、 参与贡献

​    Author：乔小乔（ 乔小乔)

​    Date:  2019年11月

​    描述： 作为一名开发如果你的业务比较复杂，表字段过多那么可能书写sql语句是你做为浪费时间与精力的，大量重复sql语句会滞缓你的开发效率，因此借助插件或者工具是提供代码书写效率的最好途径，也是你代码简洁性，健壮性不可缺少的关键，作为一名程序员书写代码不是为了实现功能而去书写代码，有时候需要考虑很多，比如代码健壮性，可读性，简洁性，可复用性，好的程序员写出的代码就想一件艺术品，让人爱不释手。 编写此插件的目的为了提高开发效率，用最少的代码实现最复杂的功能，进而提高开发效率。